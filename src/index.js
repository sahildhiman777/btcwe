import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Index from './containers';
import { Provider } from 'react-redux'
import * as serviceWorker from './serviceWorker';
import { BrowserRouter as Router, Switch } from "react-router-dom";
import history from './history';
import store from './store';

ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
            <Switch>
                <Index />
            </Switch>
        </Router>
    </Provider>,
    document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
