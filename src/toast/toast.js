import Swal from 'sweetalert2';

const ToastTopEnd = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000000
});

const ToastTopEndSuccessFire = (title) => {
    ToastTopEnd.fire({
        type: 'success',
        title: title
    });
}

const ToastTopEndWarningFire = (title) => {
    ToastTopEnd.fire({
        type: 'warning',
        title: title
    });
}

const ToastTopEndErrorFire = (title) => {
    ToastTopEnd.fire({
        type: 'error',
        title: title
    });
}

export {
    ToastTopEndSuccessFire,
    ToastTopEndWarningFire,
    ToastTopEndErrorFire
}
