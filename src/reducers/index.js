/**
 * Root Reducer
 */

// Import Reducers
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import AuthReducer from './authReducer';

const rootReducer = combineReducers({
    AuthReducer,
    routing: routerReducer
});

export default rootReducer;