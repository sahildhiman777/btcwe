import {
    LOGIN_SUCCESS,
    USER_LOGGEDOUT,
    LOADING,
    SET_USER,
    IS_REDIRECT,
    PROFILE,
    IS_TAB,
    BTC_RATE,
    IS_SIGN,
    IS_DATA,
    CONTACT_US,
    WITHDRAW_REQUEST
} from '../actions/authAction';

if (typeof localStorage === 'undefined') {
    global.localStorage = {}
}

// Initial State
const initialState = {
    isAuthenticated: (localStorage.accessToken) ? true : false,
    accessToken: (localStorage.accessToken) ? localStorage.accessToken : '',
    currentUser: (localStorage.currentUserId) ? localStorage.currentUserId : '',
    name : (localStorage.name) ? localStorage.name : '',
    balance : (localStorage.balance) ? localStorage.balance : '',
    // name : (localStorage.name) ? localStorage.name : '',
    isLoading: false,
    isRedirect:'',
    isTab: 'first',
    isbtcRate:[],
    IS_SIGN:'',
    IS_DATA:'',
    CONTACT_US:'',
    WITHDRAW_REQUEST:''
};

const AuthReducer = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
            state.isAuthenticated = action.isAuthenticated;
            state.accessToken = action.accessToken;
            // state.currentEmail = action.payload
            state.currentUser = action.payload;
            state.name = state.currentUser.full_name;
            state.isLoading = false;
            return { ...state };

        case LOADING:
            return { ...state, isLoading: action.isLoading }

        case SET_USER:
            console.log(action, 'payload SET_USER')
            return { ...state, currentUser: action.payload }
        case IS_DATA:
            return { ...state, IS_DATA: action.payload }
        
        case PROFILE:
        return { ...state, isLoading: action.isLoading, currentUser: action.payload }

        case IS_REDIRECT:
        return { ...state, isRedirect: action.payload }

        case IS_TAB:
        return { ...state, isTab: action.payload }

        case BTC_RATE:
        return { ...state,isbtcRate: action.payload }

        case IS_SIGN:
        return { ...state,IS_SIGN: action.IS_SIGN}

        case CONTACT_US:
        return { ...state,CONTACT_US: action.payload}

        case WITHDRAW_REQUEST:
        return { ...state,WITHDRAW_REQUEST: action.payload}

        case USER_LOGGEDOUT:
            state.isAuthenticated = false;
            state.accessToken = '';
            state.isLoading = false;
            state.currentUser = [];
            return state;

        default:
            return state;
    }
};

// Export Reducer
export default AuthReducer;
