import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import { Button, FormGroup, FormControl, FormLabel, Card } from "react-bootstrap";
import '../withdraw/withdraw.css';
import ReactLoading from "react-loading";
import { withdrawRequest } from '../../actions/authAction';
import $ from 'jquery';


import {
  Redirect
} from "react-router-dom";
import * as Scroll from 'react-scroll';
let scroll = Scroll.animateScroll;
class Withdraw extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loader: false,
      amount: '',
      method: 'Withdrawal crypto',
      comments: '',

    }
  }
  componentDidMount() {
    scroll.scrollToTop({ smooth: "linear", duration: 100 });
  }
  handleChange = event => {
    this.setState({ amount: event.target.value });
  }

  handlemethod = event => {
    if (event.target.value === 'Withdrawal crypto') {
      this.setState({ method: event.target.value })
    }
    if (event.target.value === 'Withdrawal wire') {
      this.setState({ method: event.target.value })
    }
    if (event.target.value === 'Withdrawal CC') {
      this.setState({ method: event.target.value })
    }
  }

  handletext = event => {
    this.setState({ comments: event.target.value })
  }

  withdraw = (e) => {
    e.preventDefault();
    this.setState({ loader: true, })
    let data = {
      email: this.props.currentUser,
      amount: (this.state.amount),
      method: (this.state.method),
      comments: (this.state.comments),
    }

    withdrawRequest(data).then(res => {
      this.setState({ amount: '', method: 'Withdrawal crypto', comments: '', loader: false })
    });
  }
  componentWillMount() {
    $(document).ready(function () {
      $(".mobile_menu_icon").removeClass("hide_mobile");
    });
  }
  render() {


    let { isAuthenticated, WITHDRAW_REQUEST } = this.props;
    if (!isAuthenticated) {
      return (<Redirect to='/login' />);
    }
    return (
      <div>
        <Helmet>
          <style>{'body { background-color: #eceff54d; } .container{ padding:0px;} .navbar{padding-right: 0px; padding-left:0px;}'}</style>
        </Helmet>
        <div className="container">
          <div className="row margin-top-50 withdraw-request-section padding-top-10 text-center">
            <div className="col-md-6 offset-md-3">
              <h2 className="text-center padding-top-20 padding-bottom-20">Withdraw Request</h2>
              <Card >
                <Card.Body>
                  {(this.state.loader) ?
                    <div className="row margin-top-20 dashboard-section padding-top-10 text-center">
                      <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px" }}>
                        <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                      </div>
                    </div>
                    :
                    <form className="text-left margin-bottom-20" onSubmit={(e) => { this.withdraw(e) }}>
                      <div className=" col-md-12">
                        <FormLabel>Select method</FormLabel>
                        <select className="method" name="method" value={this.state.method} onChange={this.handlemethod}>
                          <option value="Withdrawal crypto">Withdrawal crypto</option>
                          <option value="Withdrawal wire">Withdrawal wire</option>
                          <option value="Withdrawal CC">Withdrawal CC</option>
                        </select>
                        <div className="withdrawal_div">
                          <FormGroup controlId="amount">
                            <FormLabel>Amount</FormLabel>
                            <FormControl
                              autoFocus
                              name="amount"
                              type="number"
                              required="required"
                              value={this.state.amount}
                              onChange={this.handleChange}
                            />
                            <p>€</p>
                          </FormGroup>
                        </div>
                        <FormLabel>Comment</FormLabel>
                        <textarea className="form-control comment" rows="5" name="text" value={this.state.comments}
                          onChange={this.handletext}></textarea>
                        <Button className="btn-signup-custom" type="submit">Withdraw
                        </Button>
                      </div>

                    </form>
                  }
                </Card.Body>
              </Card>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.AuthReducer.isLoading,
    accessToken: state.AuthReducer.accessToken,
    isAuthenticated: state.AuthReducer.isAuthenticated,
    currentUser: state.AuthReducer.currentUser,
    isbtcRate: state.AuthReducer.isbtcRate,
    isTab: state.AuthReducer.isTab,
    balance: state.AuthReducer.balance,
    WITHDRAW_REQUEST: state.AuthReducer.WITHDRAW_REQUEST
  }
}


export default connect(mapStateToProps, { withdrawRequest }
)(Withdraw)
