import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, FormGroup, FormControl, FormLabel, Card, Form } from "react-bootstrap";
import { getuserdetails, getComplianceImages, complianceRequest, complianceUploadFile } from '../../actions/authAction';
import country from "../../assets/countrylist"
import "../compliance/compliance.css";
import * as Scroll from 'react-scroll';
import ReactLoading from "react-loading";
import $ from 'jquery';
let scroll = Scroll.animateScroll;
class Compliance extends Component {
  constructor(props) {
    super(props);
    this.options = country;
    this.state = {
      options: this.options,
      firstname: "",
      lastname: "",
      country: "",
      value: "",
      countryValue: "",
      phonenumber: "",
      phonenumberopt: "",
      address: "",
      city: "",
      states: "",
      zip: "",
      front_photo_name: "",
      back_photo_name: "",
      utility_bill_name: "",
      front_credit_card_name: "",
      back_credit_card_name: "",
      front_photo_type_error: "",
      back_photo_type_error: "",
      utility_bill_type_error: '',
      front_credit_type_error: '',
      back_credit_type_error: '',
      email: "",
      photoId: '',
      formData: '',
      images: [],
      frontid: false,
      backid: false,
      utility_bill: false,
      ccfront: false,
      ccback: false,
      front_photo_loader: false,
      back_photo_loader: false,
      utility_bill_loader: false,
      front_credit_card_loader: false,
      back_credit_card_loader: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.nameChangeHandler = this.nameChangeHandler.bind(this);
  }

  componentDidMount() {
    $(document).ready(function () {
      $(".mobile_menu_icon").removeClass("hide_mobile");
    });
    scroll.scrollToTop({ smooth: "linear", duration: 100 });
  }

  componentWillMount() {
    $(document).ready(function () {
      $(".mobile_menu_icon").removeClass("hide_mobile");
    });
    let data = {
      email: localStorage.getItem('currentUserId')
    }
    getuserdetails(data).then(res => {
      let firstName = res.full_name.split(' ').slice(0, -1).join(' ');
      let lastName = res.full_name.split(' ').slice(-1).join(' ');
      var phoneComponents = {
        IDDCC: res.mobile.substring(0, res.mobile.length - 10),
        NN: res.mobile.substring(res.mobile.length - 10, res.mobile.length)
      };
      this.setState({ firstname: firstName, lastname: lastName, value: phoneComponents.IDDCC, phonenumber: phoneComponents.NN, phonenumberopt: res.phone2, address: res.address, city: res.city, states: res.state, zip: res.postal });

      this.state.options.filter((opt) => {
        if (opt.label === res.country) {
          this.setState({ value: opt.code, country: opt.label })
        }
      })
    });

    let compliancedata = {
      email: localStorage.getItem('currentUserId')
    }
    getComplianceImages(compliancedata).then(res => {
      this.setState({ images: res, frontid: true, backid: true, utility_bill: true, ccfront: true, ccback: true })
    });

  }
  validateForm = () => {
    return this.state.firstname.length > 0 && this.state.lastname.length > 0 && this.state.phonenumber.length > 0 && this.state.address.length > 0 && this.state.city.length > 0 && this.state.states.length > 0 && this.state.zip.length > 0;
  }

  handleSubmit = (e) => {
    e.preventDefault();
    let data = {
      email: localStorage.getItem('currentUserId'),
      first_name: (this.state.firstname !== '') && this.state.firstname,
      last_name: (this.state.lastname !== '') && this.state.lastname,
      phoneCode: (this.state.value !== '') && this.state.value,
      mobile: (this.state.phonenumber !== '') && this.state.phonenumber,
      phone2: (this.state.phonenumberopt !== '') && this.state.phonenumberopt,
      address: (this.state.address !== '') && this.state.address,
      city: (this.state.city !== '') && this.state.city,
      state: (this.state.states !== '') && this.state.states,
      postal: (this.state.zip !== '') && this.state.zip,
      country: (this.state.country !== '') && this.state.country,
    }
    this.props.complianceRequest(data);
  }



  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
    if (event.target.name === 'phonenumber') {
    }
  }

  changeHandler = event => {
    if (event.target.name === 'value') {
      this.state.options.filter((opt) => {
        if (opt.code === event.target.value) {

          this.setState({ country: opt.label, value: opt.code })
        }
      })

    }
    else if (event.target.name === 'country') {
      this.state.options.filter((opt) => {
        if (opt.label === event.target.value) {
          this.setState({ country: opt.label, value: opt.code })
        }
      })
    }
  }

  nameChangeHandler = (event) => {
    event.preventDefault();
    if (event.target.name === 'front_photo_id') {
      this.setState({ front_photo_loader: true })
      let file = event.target.files[0];
      let t = file.type.split('/').pop().toLowerCase(); console.log('t', t)
      if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        this.setState({ front_photo_type_error: 'select only image file', frontid: false })
      }
      else {
        let name = event.target.files[0].name;
        this.setState({ front_photo_name: name, frontid: false, front_photo_type_error: '' })
        const formData = new FormData();
        formData.append('file', file, file.name);
        formData.append('id', localStorage.getItem('currentUserId'));
        formData.append('type', 'id');
        this.props.complianceUploadFile(formData).then(res => {
          this.setState({ front_photo_loader: false })
        });
      }

    }

    if (event.target.name === 'back_photo_id') {
      this.setState({ back_photo_loader: true })
      let file = event.target.files[0];
      let t = file.type.split('/').pop().toLowerCase(); console.log('t', t)
      if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        this.setState({ back_photo_type_error: 'select only image file', backid: false })
      }
      else {
        let name = event.target.files[0].name;
        this.setState({ back_photo_name: name, backid: false, back_photo_type_error: '' });
        const formData = new FormData();
        formData.append('file', file, file.name);
        formData.append('id', localStorage.getItem('currentUserId'));
        formData.append('type', 'id back');
        this.props.complianceUploadFile(formData).then(res => {
          this.setState({ back_photo_loader: false })
        });
      }
    }
    if (event.target.name === 'utility_bill') {
      this.setState({ utility_bill_loader: true })
      let file = event.target.files[0];
      let t = file.type.split('/').pop().toLowerCase(); console.log('t', t)
      if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        this.setState({ utility_bill_type_error: 'select only image file', utility_bill: false })
      }
      else {
        let name = event.target.files[0].name;
        this.setState({ utility_bill_name: name, utility_bill: false, utility_bill_type_error: '' })
        const formData = new FormData();
        formData.append('file', file, file.name);
        formData.append('id', localStorage.getItem('currentUserId'));
        formData.append('type', 'utility bill');
        this.props.complianceUploadFile(formData).then(res => {
          this.setState({ utility_bill_loader: false })
        });
      }
    }
    if (event.target.name === 'front_credit_card') {
      this.setState({ front_credit_card_loader: true })
      let file = event.target.files[0];
      let t = file.type.split('/').pop().toLowerCase(); console.log('t', t)
      if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        this.setState({ front_credit_type_error: 'select only image file', ccfront: false })
      }
      else {
        let name = event.target.files[0].name;
        this.setState({ front_credit_card_name: name, ccfront: false, front_credit_type_error: '' });
        const formData = new FormData();
        formData.append('file', file, file.name);
        formData.append('id', localStorage.getItem('currentUserId'));
        formData.append('type', 'cc front');
        this.props.complianceUploadFile(formData).then(res => {
          this.setState({ front_credit_card_loader: false })
        });
      }
    }
    if (event.target.name === 'back_credit_card') {
      this.setState({ back_credit_card_loader: true })
      let file = event.target.files[0];
      let t = file.type.split('/').pop().toLowerCase(); console.log('t', t)
      if (t != "jpeg" && t != "jpg" && t != "png" && t != "bmp" && t != "gif") {
        this.setState({ back_credit_type_error: 'select only image file', ccback: false })
      }
      else {
        let name = event.target.files[0].name;
        this.setState({ back_credit_card_name: name, ccback: false, back_credit_type_error: '' });
        const formData = new FormData();
        formData.append('file', file, file.name);
        formData.append('id', localStorage.getItem('currentUserId'));
        formData.append('type', 'cc back');
        this.props.complianceUploadFile(formData).then(res => {
          this.setState({ back_credit_card_loader: false })
        });
      }
    }
  }

  render() {
    let frontid = '';
    let backid = '';
    let utility_bill = '';
    let ccfront = '';
    let ccback = '';
    this.state.images.map((item) => {
      if (item.type === 'id') {
        frontid = item.item
      }
      if (item.type === 'id back') {
        backid = item.item
      }
      if (item.type === 'utility bill') {
        utility_bill = item.item
      }
      if (item.type === 'cc front') {
        ccfront = item.item
      }
      if (item.type === 'cc back') {
        ccback = item.item
      }
    });

    return (
      <div id="compliance_page">
        <div className="container">
          <div className="form_container">
            <form onSubmit={(e) => { this.handleSubmit(e) }} encType="multipart/form-data">
              <div className="form_colum">
                <div className="heading">
                  <h2>BTCWE.IO</h2>
                  <div className="back"></div>
                </div>
                <FormGroup controlId="firstname" className="field_width">
                  <FormLabel>First Name</FormLabel>
                  <FormControl
                    name="firstname"
                    type="text"
                    placeholder="Your first name"
                    value={this.state.firstname}
                    onChange={this.handleChange}
                  />
                </FormGroup>

                <FormGroup controlId="lastname" className="field_width1">
                  <FormLabel>Last Name</FormLabel>
                  <FormControl
                    name="lastname"
                    type="text"
                    placeholder="Your last name"
                    value={this.state.lastname}
                    onChange={this.handleChange}
                  />
                </FormGroup>

                <FormGroup controlId="phonenumber" className="phn_country">
                  <FormLabel>Phone number</FormLabel>
                  <div className="country_div">
                    <select className="country_code" name="value" value={this.state.value} onChange={this.changeHandler}>
                      {this.state.options.map((options) => <option key={options.code} value={options.code}>{options.code}</option>)}
                    </select>
                  </div>
                  <FormControl
                    name="phonenumber"
                    type="text"
                    value={this.state.phonenumber}
                    onChange={this.handleChange}
                    minlength="10"
                    maxlength="10"
                  />
                </FormGroup>

                <FormGroup controlId="phonenumberopt" className="phn_country">
                  <FormLabel>Phone number (opt.)</FormLabel>
                  <div className="country_div">
                    <select className="country_code" name="value" value={this.state.value} onChange={this.changeHandler}>
                      {this.state.options.map((options) => <option key={options.code} value={options.code}>{options.code}</option>)}
                    </select>
                  </div>
                  <FormControl
                    name="phonenumberopt"
                    type="text"
                    value={this.state.phonenumberopt}
                    onChange={this.handleChange}
                  />
                </FormGroup>

                <FormGroup controlId="address">
                  <FormLabel>Address</FormLabel>
                  <FormControl
                    name="address"
                    type="text"
                    placeholder="Your address"
                    value={this.state.address}
                    onChange={this.handleChange}
                  />
                </FormGroup>

                <FormGroup controlId="city" className="field_width">
                  <FormControl
                    name="city"
                    type="text"
                    placeholder="City"
                    value={this.state.city}
                    onChange={this.handleChange}
                  />
                </FormGroup>

                <FormGroup controlId="state" className="field_width1">
                  <FormControl
                    name="states"
                    type="text"
                    placeholder="state"
                    value={this.state.states}
                    onChange={this.handleChange}
                  />
                </FormGroup>

                <FormGroup controlId="zip" className="field_width">
                  <FormControl
                    name="zip"
                    type="text"
                    placeholder="Zip"
                    value={this.state.zip}
                    onChange={this.handleChange}
                  />
                </FormGroup>

                <FormGroup controlId="counrty" className="field_width1 counrty_field">
                  <div className="country_div_field">
                    <select className="country_code_tag" name="country" value={this.state.country} onChange={this.changeHandler}>
                      {this.state.options.map((options) => <option key={options.label} value={options.label}>{options.label}</option>)}
                    </select>
                  </div>

                </FormGroup>
              </div>

              <div className="form_colum1">
                <div className="form_card">
                  <h4>Attach front of photo ID</h4>
                  <div class="custom-file">
                    {this.state.front_photo_loader &&
                      <>
                        <div className="loader_image">
                          <div>
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                              <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                          </div>
                        </div>
                      </>
                    }
                    <input id="front_photo_id" accept="image/png, image/jpeg" name="front_photo_id" type="file" class="custom-file-input" onChange={this.nameChangeHandler} />
                    {this.state.front_photo_name ?
                      <div class="custom-file-div"><img src={require('../../images/check.svg')} alt="#" />{this.state.front_photo_name}</div>
                      :
                      <label for="front_photo_id" class="custom-file-label">No file chosen</label>
                    }
                    {this.state.frontid ?
                      <p className="images">{frontid}<img src={require('../../images/check.svg')} alt="#" /></p>
                      :
                      null
                    }
                    {this.state.front_photo_type_error &&
                      <p className="images img_type_err">{this.state.front_photo_type_error}
                      </p>
                    }
                  </div>

                  <h4>Attach back of photo ID</h4>
                  <div class="custom-file">
                    {this.state.back_photo_loader &&
                      <>
                        <div className="loader_image">
                          <div>
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                              <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                          </div>
                        </div>
                      </>
                    }
                    <input id="back_photo_id" accept="image/png, image/jpeg" name="back_photo_id" type="file" class="custom-file-input" onChange={this.nameChangeHandler} />
                    {this.state.back_photo_name ?
                      <div class="custom-file-div"><img src={require('../../images/check.svg')} alt="#" />{this.state.back_photo_name}</div>
                      :
                      <label for="back_photo_id" class="custom-file-label">No file chosen</label>
                    }
                    {this.state.backid ?
                      <p className="images">{backid}<img src={require('../../images/check.svg')} alt="#" /></p>
                      :
                      null
                    }
                    {this.state.back_photo_type_error &&
                      <p className="images img_type_err">{this.state.back_photo_type_error}
                      </p>
                    }
                  </div>

                  <h4>Attach utility bill</h4>
                  <div class="custom-file">
                    {this.state.utility_bill_loader &&
                      <>
                        <div className="loader_image">
                          <div>
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                              <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                          </div>
                        </div>
                      </>
                    }
                    <input id="utility_bill" accept="image/png, image/jpeg" name="utility_bill" onChange={this.nameChangeHandler} type="file" class="custom-file-input" />
                    {this.state.utility_bill_name ?
                      <div class="custom-file-div"><img src={require('../../images/check.svg')} alt="#" />{this.state.utility_bill_name}</div>
                      :
                      <label for="utility_bill" class="custom-file-label">No file chosen</label>
                    }
                    {this.state.utility_bill ?
                      <p className="images">{utility_bill}<img src={require('../../images/check.svg')} alt="#" /></p>
                      :
                      null
                    }
                    {this.state.utility_bill_type_error &&
                      <p className="images img_type_err">{this.state.utility_bill_type_error}
                      </p>
                    }
                  </div>

                </div>

                <div className="form_card2 custom-height">
                  <p>* If you are paying with a credit card please attach below</p>
                  <h4>Attach front of credit card</h4>
                  <div class="custom-file">
                    {this.state.front_credit_card_loader &&
                      <>
                        <div className="loader_image">
                          <div>
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                              <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                          </div>
                        </div>
                      </>
                    }
                    <input id="front_credit_card" accept="image/png, image/jpeg" name="front_credit_card" type="file" class="custom-file-input" onChange={this.nameChangeHandler} />
                    {this.state.front_credit_card_name ?
                      <div class="custom-file-div"><img src={require('../../images/check.svg')} alt="#" />{this.state.front_credit_card_name}</div>
                      :
                      <label for="front_credit_card" class="custom-file-label">No file chosen</label>
                    }
                    {this.state.ccfront ?
                      <p className="images">{ccfront}<img src={require('../../images/check.svg')} alt="#" /></p>
                      :
                      null
                    }
                    {this.state.front_credit_type_error &&
                      <p className="images img_type_err">{this.state.front_credit_type_error}
                      </p>
                    }
                  </div>

                  <h4>Attach back of credit card</h4>
                  <div class="custom-file" id="margin_bottom_last">
                    {this.state.back_credit_card_loader &&
                      <>
                        <div className="loader_image">
                          <div>
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                              <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                          </div>
                        </div>
                      </>
                    }
                    <input id="back_credit_card" accept="image/png, image/jpeg" name="back_credit_card" type="file" class="custom-file-input" onChange={this.nameChangeHandler} />
                    {this.state.back_credit_card_name ?
                      <div class="custom-file-div"><img src={require('../../images/check.svg')} alt="#" />{this.state.back_credit_card_name}</div>
                      :
                      <label for="back_credit_card" class="custom-file-label">No file chosen</label>
                    }
                    {this.state.ccback ?
                      <p className="images">{ccback}<img src={require('../../images/check.svg')} alt="#" /></p>
                      :
                      null
                    }
                    {this.state.back_credit_type_error &&
                      <p className="images img_type_err">{this.state.back_credit_type_error}
                      </p>
                    }
                  </div>

                </div>

              </div>
              <div className="clearfix"></div>
              <div className="submit_button">
                <Button className="button_submit"
                  type="submit"
                  disabled={!this.validateForm()}
                >
                  Save
          </Button>
              </div>
            </form>
          </div>
          <div className="background_div">
            <div className="dots_img">
              <img alt="#" src={require("../../images/dots.png")} />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.AuthReducer.isAuthenticated,
    accessToken: state.AuthReducer.accessToken,
  };
};

const mapDispatchToProps = (dispatch) => ({
  complianceRequest: (data) => dispatch(complianceRequest(data)),
  complianceUploadFile: (data) => dispatch(complianceUploadFile(data))

})

export default connect(mapStateToProps, mapDispatchToProps)(Compliance)
