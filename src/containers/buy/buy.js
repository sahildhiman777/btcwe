import React from 'react';
import { connect } from 'react-redux';
import CustomTabs from "../../shared/CustomTabs/CustomTabs";

class Buy extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectState: false,
        }
    }

    render() {
        return (
            <div className="row " id="project-tabs">
                <div className="col-md-10 offset-md-1">
                    <div className="card margin-top-50 container_box_shadow">
                        <div className="card-body">
                            <CustomTabs />
                        </div>
                    </div>
                </div>
            </div>


        )
    }
}
const mapStateToProps = (state) => {
    return {
        isLoading: state.AuthReducer.isLoading,
        accessToken: state.AuthReducer.accessToken,
        isAuthenticated: state.AuthReducer.isAuthenticated,
        currentUser: state.AuthReducer.currentUser,
    }
}
export default connect(mapStateToProps)(Buy)