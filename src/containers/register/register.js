import React, { Component } from 'react'
import { connect } from 'react-redux'
import { registerRequest, getCountry } from '../../actions/authAction';
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
import country from "../../assets/countrylist"
import { Button, FormGroup, FormControl, FormLabel, Card, InputGroup, Form } from "react-bootstrap";
import Select from 'react-select';
import * as Scroll from 'react-scroll';
import "../register/register.css";
import $ from 'jquery';
import ReactLoading from "react-loading";


let scroll = Scroll.animateScroll;
class Register extends Component {
    constructor(props) {
        super(props);
        this.options = country;
        this.state = {
            redirectState: false,
            email: "",
            password: "",
            repeat_password: "",
            lastName: "",
            firstName: "",
            phone: "",
            options: this.options,
            value: "",
            country: "",
            agree: false,
            passworderror: "",
            type: 'password',
            type2: 'password',
            urlparams: [],
            dial: '',
            countryvalue: '',
            blockloader:true,
        }

    }

    componentWillMount() {
        $(document).ready(function () {
            $(".mobile_menu_icon").addClass("hide_mobile");
        });
        getCountry().then(res => {
            if(res.props != undefined){
            if(res.props.to){
                this.props.history.push("/countryblocked");
                this.setState({blockloader:false});
            }
        }else{
            this.setState({blockloader:false});
          }
            if(res.message){
                if (res.message.length == 0) {
                    this.setState({ country: 'United Kingdom', value: '+44', countryvalue: 'GB' })
                } else {
                    this.setState({ country: res.message[0].official_name, value: '+'+res.message[0].Dial, countryvalue: res.message[0]['ISO3166-1-Alpha-2']})
                }
            }
        })
        const search = window.location.search.substring(1);
        if (search) {
            let vars = search.split("&");
            for (var i = 0; i < vars.length; i++) {
                let pair = vars[i].split("=");
                this.state.urlparams.push({ key: pair[0], value: pair[1] })
            }
        }
    }
    componentDidMount() {
        $(document).ready(function () {
            $(".mobile_menu_icon").addClass("hide_mobile");
        });
        scroll.scrollToTop({ smooth: "linear", duration: 100 });
        this.nameInput.focus();
    }

    changeHandler = event => {
        if (event.target.name === 'value') {
            this.state.options.filter((opt) => {
                if (opt.code === event.target.value) {
                    // this.setState({ country: opt.label, value: opt.code })
                    this.setState({ value: opt.code })
                }
            })

        }
        else if (event.target.name === 'country') {
            this.state.options.filter((opt) => {
                if (opt.label === event.target.value) {
                    this.setState({ country: opt.label, value: opt.code, countryvalue: opt.value })
                }
            })
        }
    }
    validateForm = () => {
        if (this.state.email.length > 0 && this.state.password.length > 0 && this.state.repeat_password.length > 0 && this.state.lastName.length > 0 && this.state.firstName.length > 0 && this.state.phone.length > 0 && this.state.country.length > 0 && this.state.agree.length > 0) {
            if (this.state.password === this.state.repeat_password) {
                return true;
            } else {
                return false;
            }
        }
    }
    componentWillReceiveProps(nextProps) {
    }

    handleClick = () => this.setState(({ type }) => ({
        type: type === 'text' ? 'password' : 'text'
    }));
    handleClick2 = () => this.setState(({ type2 }) => ({
        type2: type2 === 'text' ? 'password' : 'text'
    }));

    handleChange = event => {
        const value = event.target.type === 'checkbox' ? "1" : event.target.value;
        const name = event.target.name
        this.setState({
            [name]: value
        });
    }

    registerFun = (e) => {
        e.preventDefault();
        let country_name = this.state.countryvalue;
        var code = this.state.value;
        var res = code.substr(1);

        let data = {
            email: (this.state.email !== '') && this.state.email,
            password: (this.state.password !== '') && this.state.password,
            last_name: (this.state.lastName !== '') && this.state.lastName,
            first_name: (this.state.firstName !== '') && this.state.firstName,
            country_name: (country_name !== '') && country_name,
            contact: (this.state.phone !== '') && this.state.phone,
            code: res
        }

        if (this.state.urlparams.length !== 0) {
            this.state.urlparams.map(function (v) {
                data[v.key] = v.value;

            })
        } else {
            data['token'] = 'fhygj$345hghj2#@42!@~":L@#@ghjef';
        }
        this.props.registerRequest(data);
    }

    render() {
        if (this.props.isRedirect !== '') {
            this.props.history.push(this.props.isRedirect)
        }

        return (
            <div className=" register-section-cointainer">
                {(this.state.blockloader) &&
          <>
            <div className="blockloader">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
          </>}
                <div className="container padding-top-bottom-50">
                    <Helmet>
                        <style>
                            {".header2 .container{ max-width:unset; margin:unset;padding-left:6%;} #root{overflow:hidden;}.register-section-cointainer .container { transform: skew(0deg, 2deg);}.register-section-cointainer { transform: skew(0deg, -2deg); background:#f9fafe; background-repeat: no-repeat; border-radius: 0px 104px 0px 104px;} ul.ml-auto{display:none !important;}  .footer{ display:none !important;}"}</style>
                    </Helmet>
                    <div className="row margin-top-50">
                        <div className="col-md-12 col-lg-6 reg-message-section hide_mobile">
                            <div className="col-md-12 col-lg-12 reg-message-section-2">
                                <div className="text-rotate">
                                    <p className="signup-text">Already Signed Up?</p>
                                    <Link to="/login" className="btn bck-white-text" role="button">Log In</Link>
                                </div>
                            </div>
                        </div>

                        <div className=" col-md-12 col-lg-6 register-section">
                            <div className="col-md-12">
                                <h1 className="register-header-text">Sign up for an account</h1>
                                <Card>
                                    <Card.Body>
                                        <form onSubmit={(e) => { this.registerFun(e) }}>
                                            <div className="row margin-top-50">
                                                <FormGroup className="col-md-12 alreadysign">
                                                    <FormLabel className="text-center">Already Signed Up? <Link to="/login" className="dark-signup">Log In </Link></FormLabel>
                                                </FormGroup>
                                                <FormGroup controlId="firstName" className="col-md-12">
                                                    <FormLabel>First name</FormLabel>
                                                    <FormControl
                                                        autoFocus
                                                        name="firstName"
                                                        type="text"
                                                        placeholder="Enter first name"
                                                        value={this.state.firstName}
                                                        onChange={this.handleChange}
                                                        ref={(input) => { this.nameInput = input; }}
                                                    />
                                                </FormGroup>
                                                <FormGroup controlId="lastName" className="col-md-12">
                                                    <FormLabel>Last name</FormLabel>
                                                    <FormControl
                                                        autoFocus
                                                        name="lastName"
                                                        type="text"
                                                        placeholder="Enter last name"
                                                        value={this.state.lastName}
                                                        onChange={this.handleChange}
                                                    />
                                                </FormGroup>
                                            </div>
                                            <FormGroup controlId="country">
                                                <FormLabel>Country</FormLabel>
                                                <select name="country" className="country" value={this.state.country} onChange={this.changeHandler}>
                                                    {this.state.options.map((options) => <option key={options.label} value={options.label}>{options.label}</option>)}
                                                </select>
                                            </FormGroup>
                                            <FormGroup controlId="phone_number">
                                                <FormLabel>Phone number</FormLabel>
                                                <InputGroup className="col-md-12 padding-0 phone_code_div">
                                                    <div className="">
                                                        <select className="country_code" name="value" value={this.state.value} onChange={this.changeHandler}>
                                                            {this.state.options.map((options) => <option key={options.code} value={options.code}>{options.code}</option>)}
                                                        </select>
                                                    </div>
                                                    <FormControl
                                                        className="country_input"
                                                        name="phone"
                                                        value={this.state.phone}
                                                        onChange={this.handleChange}
                                                        type="text"
                                                        minlength="10"
                                                        maxlength="10"
                                                    />
                                                </InputGroup>
                                            </FormGroup>
                                            <FormGroup controlId="email">
                                                <FormLabel>Email</FormLabel>
                                                <FormControl
                                                    autoFocus
                                                    name="email"
                                                    type="email"
                                                    placeholder="Enter email"
                                                    value={this.state.email}
                                                    onChange={this.handleChange}
                                                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                                                />
                                            </FormGroup>
                                            <div className="row">
                                                <FormGroup controlId="password" className="col-md-12">
                                                    <FormLabel>Password</FormLabel>

                                                    <InputGroup className="col-md-12 padding-0">
                                                        <FormControl
                                                            className="border-right-unset"
                                                            name="password"
                                                            value={this.state.password}
                                                            onChange={this.handleChange}
                                                            type={this.state.type}
                                                        />
                                                        <InputGroup.Append>
                                                            <InputGroup.Text onClick={this.handleClick}> <img src={require('../../images/icons/visibility.svg')} alt="#" /> </InputGroup.Text>
                                                        </InputGroup.Append>
                                                    </InputGroup>
                                                </FormGroup>
                                                <FormGroup controlId="password" className="col-md-12">
                                                    <FormLabel>Repeat Password</FormLabel>
                                                    <InputGroup className="col-md-12 padding-0">
                                                        <FormControl
                                                            className="border-right-unset"
                                                            name="repeat_password"
                                                            value={this.state.repeat_password}
                                                            onChange={this.handleChange}
                                                            type={this.state.type2}
                                                        />
                                                        <InputGroup.Append>
                                                            <InputGroup.Text onClick={this.handleClick2}> <img src={require('../../images/icons/visibility.svg')} alt="#" /> </InputGroup.Text>
                                                        </InputGroup.Append>
                                                    </InputGroup>
                                                </FormGroup>
                                                {this.state.passworderror &&
                                                    <>
                                                        <p>{this.state.passworderror}</p>
                                                    </>}
                                            </div>
                                            {/* <FormGroup controlId="country">
                                                <FormLabel>Country</FormLabel>
                                                <select name="country" className="country" value={this.state.country} onChange={this.changeHandler}>
                                                    {this.state.options.map((options) => <option key={options.label} value={options.label}>{options.label}</option>)}
                                                </select>
                                            </FormGroup> */}
                                            <FormGroup controlId="formBasicCheckbox" className="margin-top-30 margin-bottom-30 agree_div">
                                                <span>
                                                    <Form.Check
                                                        name="agree"
                                                        onChange={this.handleChange}
                                                        value={this.state.agree}
                                                        type="checkbox"
                                                    />
                                                </span>
                                                <span className="agree_text">I agree to <Link to="/terms-and-conditions">Terms & Conditions</Link></span>
                                            </FormGroup>
                                            <div className="signup_button">
                                                <Button className="btn-signup-custom" disabled={!this.validateForm()} type="submit">Sign Up
                                        </Button>
                                            </div>

                                        </form>

                                    </Card.Body>
                                </Card>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.AuthReducer.isAuthenticated,
        accessToken: state.AuthReducer.accessToken,
        isRedirect: state.AuthReducer.isRedirect
    };
};

const mapDispatchToProps = (dispatch) => ({
    registerRequest: (data) => dispatch(registerRequest(data))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Register)