import React, { Component } from 'react'
import './tradinghistory.css';
import { close_trades_by_username } from '../../../actions/authAction';
import ReactLoading from "react-loading";
import DataTable from 'react-data-table-component';
import $ from 'jquery';

class TradingHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tableCloseContent: [],
      loader: true,
      alltrade: false,
      losttrade: false,
      wontrade: false,
      tietrade: false,
      active: '',
    }
    this.getallTrade = this.getallTrade.bind(this);
    this.getlostTrade = this.getlostTrade.bind(this);
    this.getwonTrade = this.getwonTrade.bind(this);
    this.gettietTrade = this.gettietTrade.bind(this);
  }
  getallTrade() {
    this.setState({ alltrade: true, losttrade: false, wontrade: false, tietrade: false, active: 'all' })
  }
  getlostTrade() {
    this.setState({ alltrade: false, losttrade: true, wontrade: false, tietrade: false, active: 'lost' })
  }
  getwonTrade() {
    this.setState({ alltrade: false, losttrade: false, wontrade: true, tietrade: false, active: 'won' })
  }
  gettietTrade() {
    this.setState({ alltrade: false, losttrade: false, wontrade: false, tietrade: true, active: 'tie' })
  }

  componentWillMount() {
    $(document).ready(function () {
      $(".mobile_menu_icon").removeClass("hide_mobile");
    });
    let data = { 'email': localStorage.getItem('currentUserId') };
    close_trades_by_username(data).then(res => {
      if (Array.isArray(res) && res.length !== 0) {
        this.setState({ tableCloseContent: res }, () => {
          this.setState({ loader: false, active: 'all' })
        });
      }
    });
  }
  render() {
    let table_close_item = this.state.tableCloseContent;
    let clone_table_close_item = this.state.tableCloseContent;
    let close_no_record = [];
    let final = 0;

    if (table_close_item === undefined || table_close_item.length == 0) {
      close_no_record = [];
    } else {
      close_no_record = table_close_item;
    }


    if (this.state.alltrade == true) {
      close_no_record = [];
      if (table_close_item === undefined || table_close_item.length == 0) {
        close_no_record = [];
      } else {
        close_no_record = table_close_item;
      }
    }

    if (this.state.losttrade == true) {
      close_no_record = [];
      let lost_data = clone_table_close_item.filter(function (daa) {
        return daa.final_pl < final;
      });

      if (lost_data === undefined || lost_data.length == 0) {
        close_no_record = [];
      } else {
        close_no_record = lost_data;
      }
    }

    if (this.state.wontrade == true) {
      close_no_record = [];
      let won_data = clone_table_close_item.filter(function (daa) {
        return daa.final_pl > final;
      });

      if (won_data === undefined || won_data.length == 0) {
        close_no_record = [];
      } else {
        close_no_record = won_data;
      }

    }

    if (this.state.tietrade == true) {
      close_no_record = [];
      let tie_data = clone_table_close_item.filter(function (daa) {
        return daa.final_pl == final;
      });

      if (tie_data === undefined || tie_data.length == 0) {
        close_no_record = [];
      } else {
        close_no_record = tie_data;
      }

    }

    const columns = [
      {
        name: 'TradeId',
        selector: 'trade_id',
        cell: row => <>{(row.final_pl < 0) ? <div className="trading_table_red">{row.trade_id}</div> : (row.final_pl == 0 ? <div className="trading_table">{row.trade_id}</div> : <div className="trading_table_green">{row.trade_id}</div>)}</>
      },
      {
        name: 'Date',
        selector: 'date1',
        cell: row => <>{(row.final_pl < 0) ? <div className="trading_table_red">{row.date1}</div> : (row.final_pl == 0 ? <div className="trading_table">{row.date1}</div> : <div className="trading_table_green">{row.date1}</div>)}</>
      },
      {
        name: 'Type',
        selector: 'type',
        cell: row => <>{(row.final_pl < 0) ? <div className="trading_table_red">{row.type}</div> : (row.final_pl == 0 ? <div className="trading_table">{row.type}</div> : <div className="trading_table_green">{row.type}</div>)}</>
      },
      {
        name: 'Asset Name',
        selector: 'asset',
        cell: row => <>{(row.final_pl < 0) ? <div className="trading_table_red">{row.asset}</div> : (row.final_pl == 0 ? <div className="trading_table">{row.asset}</div> : <div className="trading_table_green">{row.asset}</div>)}</>
      },
      {
        name: 'Amount',
        selector: 'investment',
        cell: row => <>{(row.final_pl < 0) ? <div className="trading_table_red">{row.investment} €</div> : (row.final_pl == 0 ? <div className="trading_table">{row.investment} €</div> : <div className="trading_table_green">{row.investment} €</div>)}</>
      },
      {
        name: 'Leverage',
        selector: 'leverage',
        cell: row => <>{(row.final_pl < 0) ? <div className="trading_table_red">{row.leverage}</div> : (row.final_pl == 0 ? <div className="trading_table">{row.leverage}</div> : <div className="trading_table_green">{row.leverage}</div>)}</>
      },
      {
        name: 'Entry Rate',
        selector: 'entry_rate',
        cell: row => <>{(row.final_pl < 0) ? <div className="trading_table_red">{row.entry_rate}</div> : (row.final_pl == 0 ? <div className="trading_table">{row.entry_rate}</div> : <div className="trading_table_green">{row.entry_rate}</div>)}</>
      },
      {
        name: 'Take Profit',
        selector: 'take_profit',
        cell: row => <>{(row.final_pl < 0) ? <div className="trading_table_red">{row.take_profit}</div> : (row.final_pl == 0 ? <div className="trading_table">{row.take_profit}</div> : <div className="trading_table_green">{row.take_profit}</div>)}</>
      },
      {
        name: 'Stop Loss',
        selector: 'stop_loss',
        cell: row => <>{(row.final_pl < 0) ? <div className="trading_table_red">{row.stop_loss}</div> : (row.final_pl == 0 ? <div className="trading_table">{row.stop_loss}</div> : <div className="trading_table_green">{row.stop_loss}</div>)}</>
      },
      {
        name: 'Current Rate',
        selector: 'final_rate',
        cell: row => <>{(row.final_pl < 0) ? <div className="trading_table_red">{row.final_rate}</div> : (row.final_pl == 0 ? <div className="trading_table">{row.final_rate}</div> : <div className="trading_table_green">{row.final_rate}</div>)}</>
      },
      {
        name: 'P/L',
        selector: 'final_pl',
        cell: row => <>{(row.final_pl < 0) ? <div className="trading_table_red">{row.final_pl} €</div> : (row.final_pl == 0 ? <div className="trading_table">{row.final_pl} €</div> : <div className="trading_table_green">{row.final_pl} €</div>)}</>
      },

    ];


    return (
      <div>

        <div className="container">
          <div className="theme-title margin-top-70">
            <h2 className="text-center"><img alt="#" src={require("../../../images/line.png")} /> Trading History</h2>
            <button type="button" className={"btn all_trades_button " + ((this.state.active === 'all') ? 'active' : '')} onClick={this.getallTrade} >All</button>
            <button type="button" className={"btn all_trades_button " + ((this.state.active === 'won') ? 'active' : '')} onClick={this.getwonTrade} >Won</button>
            <button type="button" className={"btn all_trades_button " + ((this.state.active === 'lost') ? 'active' : '')} onClick={this.getlostTrade} >Lost</button>
            <button type="button" className={"btn all_trades_button " + ((this.state.active === 'tie') ? 'active' : '')} onClick={this.gettietTrade} >Tie</button>
          </div>
          {(this.state.loader) ?
            <div className="row margin-top-20 dashboard-section padding-top-10 text-center">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px" }}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
            :
            <DataTable className="scrollbar_all_trades trading-hestory" columns={columns} data={close_no_record} pagination />
          }
        </div>
      </div>
    )
  }
}


export default (TradingHistory)
