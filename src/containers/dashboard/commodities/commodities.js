import React, { Component } from 'react'
import { connect } from 'react-redux';
import {Helmet} from "react-helmet";
import {OverlayTrigger,Popover} from "react-bootstrap";
import { getAssets, getComAssets, createMarket, getRemoteBtcRate, add_fav_assets, open_trades_by_username, oneTrade, close_trades, get_buy_sell } from '../../../actions/authAction';
import ReactLoading from "react-loading";
import {  ToastTopEndErrorFire } from '../../../toast/toast';
// import { getbtcrate ,getpending , getcompleted, getfailed, getlogs} from '../../actions/authAction';
// import { logoutFun } from '../../actions/authAction';
import { authorizationView } from '../../authorization';
import CandlestickChart from '../../../shared/CandlestickChart/candlestickChart';
import DataTable, { createTheme } from 'react-data-table-component';
import {
  // Modal, 
  Button
} from 'react-bootstrap';
import {
  Redirect
} from "react-router-dom";
import Slider from "react-slick";
import * as Scroll from 'react-scroll';
let scroll     = Scroll.animateScroll;

var convertPrice = function(num){
  return num ? Number(num) :0;
}
var convertSpread = function(spread){
  return spread ? spread.toFixed(9) : 1 ;
}

class Commodities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buyprice: 0,
      sellprice: 0,
      show: 'show',
      div_id:'',
      assets:'',
      asset_id:'',
      assetName:'Crude Oil WTI/BTC',
      assetIcon:'btc.png',
      isLoading:true,
      isLoadingBuySell : true,
      active:'Crude Oil WTI/BTC',
      buyPercentage:'',
      sellPercentage:'',
      buyMarketPrice:'0',
      sellMarketPrice:'0',
      buy:0,
      sell:0,
      buyqty1:'',
      add1:'',
      minus1:'',
      sellqty1:'',
      buy_tp_qty1:'',
      sell_tp_qty1:'',
      message:'',
      fiatassets:'',
      buyleverage:'1',
      sellleverage:'1',
      buystop_take: false,
      sellstop_take: false,
      tableContent: [],
      closedata:[],
      alltrade: false,
      buy_value:'',
      sell_value:'',
    }
 this.BuyIncrementItem = this.BuyIncrementItem.bind(this);
 this.BuyDecreaseItem = this.BuyDecreaseItem.bind(this);
 this.clickState = this.clickState.bind(this);
 this.getallTrade = this.getallTrade.bind(this);
}

componentDidMount(){
  scroll.scrollToTop({smooth: "linear",duration: 100});
}
  componentWillMount() {
    let { currentUser } = this.props;
    let currentUserEmail = localStorage.getItem('currentUserId');
    let data = { 'email': currentUserEmail };
    getRemoteBtcRate().then(res=>{});

    let asset_data = { 'email': currentUserEmail, 'asset_type': 'crypto2commodity' }

    // getComAssets().then(res => {
      getAssets(asset_data).then(res => {
      // let assetId = 1;
      let result= res.rate[0];
      localStorage.setItem('asset_id', result.id);
      // assetId = result.id;   

      this.setState({ buyprice: convertPrice(result.buy_rate) });
      this.setState({ buy_value: convertPrice(0.5*(result.buy_rate/100)) });
      this.setState({ sellprice: convertPrice(result.sell_rate) });
      this.setState({ sell_value: convertPrice(0.5*(result.sell_rate/100)) });
        this.setState({ buyMarketPrice: convertPrice(result.buy_rate) });
        this.setState({ sellMarketPrice: convertPrice(result.sell_rate), isLoadingBuySell: false });

      this.setState({ assets: res.rate, assetIcon: result.base_icon ,div_id:'div-id-'+result.id,asset_id:result.id,isLoading:false});
      let active_asset_page = localStorage.getItem('commodities-asset-page');
      if(active_asset_page) {      
        active_asset_page = JSON.parse(active_asset_page);
        this.setState({ active: active_asset_page.asset_name, assetName: active_asset_page.asset_name, buyMarketPrice:convertPrice(active_asset_page.buy_rate), sellMarketPrice:convertPrice(active_asset_page.sell_rate), buyprice: active_asset_page.buy_rate, sellprice: active_asset_page.sell_rate, assetIcon: active_asset_page.asseticon, div_id: 'div-id-' + active_asset_page.id, asset_id: active_asset_page.id});
      }
      // let gbsdata = {'email': data.email, 'id':assetId}
      // get_buy_sell(gbsdata).then(gbs =>{
      //   this.setState({ buyprice: convertPrice(gbs.buy_rate) });
      //   this.setState({ sellprice: convertPrice(gbs.sell_rate) });
      //   this.setState({ buyMarketPrice: convertPrice(gbs.buy_rate) });
      //   this.setState({ sellMarketPrice: convertPrice(gbs.sell_rate), isLoadingBuySell: false });
      // })
  });

open_trades_by_username(data).then(res => {
  if (Array.isArray(res) && res.length !== 0) {
    this.setState({ tableContent: res }, () => {
    });
  }
});

}
clickState(id,asset_name,buy_rate, sell_rate, asseticon) {
  let div_id = 'div-id-' + id;
  this.setState({ buyprice: convertPrice(buy_rate),buy_value: convertPrice(0.5*(buy_rate/100)), sellprice: convertPrice(sell_rate),sell_value: convertPrice(0.5*(sell_rate/100)), div_id: div_id, assetName: asset_name, assetIcon: asseticon, asset_id: id, buyMarketPrice: convertPrice(buy_rate), sellMarketPrice: convertPrice(sell_rate), active: asset_name });

        localStorage.setItem('commodities-asset-page', JSON.stringify({ id: id, asset_name: asset_name, buy_rate: convertPrice(buy_rate), sell_rate: convertPrice(sell_rate), asseticon: asseticon }));
        this.setState({ buy_tp_qty1: '', buyqty1: '', sell_tp_qty1: '', sellqty1: '', isLoadingBuySell : false});

        localStorage.setItem('asset_id', id)

  // let currentUserEmail = localStorage.getItem('currentUserId');
  // let data = { 'email': currentUserEmail };
  // this.setState({isLoadingBuySell : true})
  // let gbsdata = {'email': data.email, 'id':id}
  //   get_buy_sell(gbsdata).then(gbs =>{
  //     let div_id = 'div-id-' + id;
  //       this.setState({ buyprice: convertPrice(gbs.buy_rate), sellprice: convertPrice(gbs.sell_rate), div_id: div_id, assetName: asset_name, assetIcon: asseticon, asset_id: id, buyMarketPrice: convertPrice(gbs.buy_rate), sellMarketPrice: convertPrice(gbs.sell_rate), active: asset_name });

  //       localStorage.setItem('commodities-asset-page', JSON.stringify({ id: id, asset_name: asset_name, buy_rate: convertPrice(gbs.buy_rate), sell_rate: convertPrice(gbs.sell_rate), asseticon: asseticon }));
  //       this.setState({ buy_tp_qty1: '', buyqty1: '', sell_tp_qty1: '', sellqty1: '', isLoadingBuySell : false})
  //   });
}
  addFavourite(){
    const { currentUser } = this.props;
    const asset_id = this.state.asset_id;
    let data = {
      'email': currentUser,
      'asset_id': asset_id
    }
    this.props.add_fav_assets(data);
  }

  // buyPercentage(percentage){
  //   // let current = this.state.assetName.split('/');
  //   // let current_asset = current[1]+'/EUR';
  //   // let fiatbuyprice;
  //   // this.state.fiatassets.filter(function (da) {
  //   //   if(da.asset === current_asset){
  //   //     fiatbuyprice = da.buy_rate;
  //   //   }
  //   // });
  //   let price = localStorage.getItem('balance');
  //   // let markit_rate = this.state.buyMarketPrice;
  //   let percent = price*convertPrice(percentage)/100;
  //   // let amount = (1/fiatbuyprice)*percent;
  //   // let total = amount/markit_rate;
  //   this.setState({buy: percent, buyPercentage:'buy-per-'+convertPrice(percentage)}); 
  // }

  // sellPercentage(percentage){
  //   // let current = this.state.assetName.split('/');
  //   // let current_asset = current[1]+'/EUR';
  //   // let sellbuyprice;
  //   // this.state.fiatassets.filter(function (da) {
  //   //   if(da.asset === current_asset){
  //   //     sellbuyprice = da.sell_rate;
  //   //   }
  //   // });
  //   let price = localStorage.getItem('balance');
  //   // let markit_rate = this.state.sellMarketPrice;
  //   let percent = price*convertPrice(percentage)/100;
  //   // let amount = (1/sellbuyprice)*percent;
  //   // let total = amount/markit_rate;
  //   this.setState({sell:percent, sellPercentage:'sell-per-'+convertPrice(percentage)});
  // }

  handleBuyInput(e) {
    e.stopPropagation();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ buyprice: convertPrice(value) });
    // this.setState({ [name]: value },
    //     () => { this.validateBankPaymentField(name, value) });

  }

  handleBuyAmountInput(e) {
    e.stopPropagation();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ buy: Number(value) });
    // this.setState({ [name]: value },
    //     () => { this.validateBankPaymentField(name, value) });

  }
  handleSellAmountInput(e){
    e.stopPropagation();
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ sell: Number(value) });
  }
  handleleverage(e){
    e.preventDefault();
    if(e.target.name === 'buy'){
      this.setState({buyleverage:e.target.value})
    }else{
      this.setState({sellleverage:e.target.value})
    }
  }

  handleSellInput(e) { 
    e.stopPropagation();   
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ sellprice: convertPrice(value) });
  }

  buyMarketOrder(trade_type,e) { 
    e.preventDefault();
    e.stopPropagation();
    if(this.state.buy !== 0 && this.state.buy !== ''){
    const {currentUser} = this.props;  
    let email = localStorage.getItem('currentUserId');
    let data = {
      'email': email,
      'amount':this.state.buy,
      'asset_id':this.state.asset_id, 
      'trade_type':trade_type, 
      'leverage': this.state.buyleverage, 
      'rate':this.state.buyMarketPrice,
      'profit':this.state.buy_tp_qty1,
      'stop_loss':this.state.buyqty1
    }
  
    this.props.createMarket(data).then(res => {
      // if (res.status == "success"){
        open_trades_by_username(data).then(res => {
          if (Array.isArray(res) && res.length !== 0) {
            this.setState({ tableContent: res }, () => {
              this.setState({
                buy: '',
                buyleverage: 1,
                buy_tp_qty1: '',
                buyqty1: ''
              })
            });
          }
        });
      // }
      
   });
  }else{
    ToastTopEndErrorFire('Please Enter Amount !!');
  }
}

  sellMarketOrder(trade_type,e) { 
    e.preventDefault();
    e.stopPropagation();
    if(this.state.sell !== 0 && this.state.sell !== ''){
    const {currentUser} = this.props; 
    let email = localStorage.getItem('currentUserId');
    let data = {
      'email': email,
      'amount':this.state.sell,
      'asset_id':this.state.asset_id, 
      'trade_type':trade_type, 
      'leverage': this.state.sellleverage, 
      'rate':this.state.sellMarketPrice,
      'profit':this.state.sell_tp_qty1,
      'stop_loss':this.state.sellqty1
    }
  
    this.props.createMarket(data).then(res => {
      // if (res.status == "success"){
        open_trades_by_username(data).then(res => {
          if (Array.isArray(res) && res.length !== 0) {
            this.setState({ tableContent: res }, () => {
              this.setState({
                sell: '',
                sellleverage: 1,
                sell_tp_qty1: '',
                sellqty1: ''
              })
            });
          }
        });
      // }
   });
  }else{
    ToastTopEndErrorFire('Please Enter Amount !!');
  }}

  handlestoploss(e, trade_type) {
    const name = e.target.name;
    const value = e.target.value;
    if (trade_type === 'buy') {
      if(value < 0){
        this.setState({ buyqty1: 0 });
      }else{
        this.setState({ buyqty1: convertPrice(value) });
      }
      if(value > this.state.buyprice){
        this.setState({ buyqty1: this.state.buyprice });
      }else{
        this.setState({ buyqty1: convertPrice(value) });
      }
      
    } else {
      if(value < 0){
        this.setState({ sellqty1: this.state.sellprice });
      }else{
        this.setState({ sellqty1: convertPrice(value) });
      }
      if(value > this.state.sellprice){
        this.setState({ sellqty1: convertPrice(value) });
      }
    }
  }

  handletakeprofit(e, trade_type) {
    const name = e.target.name;
    const value = e.target.value;
    if (trade_type === 'buy') {
      if(value < 0){
        this.setState({ buy_tp_qty1: this.state.buyprice });
      }else{
        this.setState({ buy_tp_qty1: convertPrice(value) });
      }
      if(value > this.state.buyprice){
        this.setState({ buy_tp_qty1: convertPrice(value) });
      }
    } else {
      if(value < 0){
        this.setState({ sell_tp_qty1: 0 });
      }else{
        this.setState({ sell_tp_qty1: convertPrice(value) });
      }
      if(value > this.state.sellprice){
        this.setState({ sell_tp_qty1: this.state.sellprice });
      }
    }
  }

  // sl_minus(trade_type) {
  //   if(trade_type == 'sell'){
  //     var sell_qty = this.state.sellqty1;
  //     var float = parseFloat(sell_qty)
  //     let final = float - 0.1
  //     this.setState({sellqty1:final.toFixed(1)})
  //   }else if(trade_type === 'buy'){
  //     var buy_qty = this.state.buyqty1;
  //     var float = parseFloat(buy_qty)
  //     let final = float - 0.1
  //     this.setState({buyqty1:final.toFixed(1)})
      
  //   }
  // }
  
  // sl_plus(trade_type) {
  //   if(trade_type == 'sell'){
  //     var sell_qty = this.state.sellqty1;
  //     var float = parseFloat(sell_qty)
  //     let final = float + 0.1
  //     this.setState({sellqty1:final.toFixed(1)})
  // }else if(trade_type === 'buy'){
  //     var buy_qty = this.state.buyqty1;
  //     var float = parseFloat(buy_qty)
  //     let final = float + 0.1
  //     this.setState({buyqty1:final.toFixed(1)})
  // }
  // }
  
  // tp_plus(trade_type) {
  //   if(trade_type == 'sell'){
  //     var sell_qty = this.state.sell_tp_qty1;
  //     var float = parseFloat(sell_qty)
  //     let final = float + 0.1
  //     this.setState({sell_tp_qty1:final.toFixed(1)})
  // }else if(trade_type === 'buy'){
  //     var buy_qty = this.state.buy_tp_qty1;
  //     var float = parseFloat(buy_qty)
  //     let final = float + 0.1
  //     this.setState({buy_tp_qty1:final.toFixed(1)})
  // }
  // }
  
  // tp_minus(trade_type) {
  //   if(trade_type == 'sell'){
  //     var sell_qty = this.state.sell_tp_qty1;
  //     var float = parseFloat(sell_qty)
  //     let final = float - 0.1
  //     this.setState({sell_tp_qty1:final.toFixed(1)})
  // }else if(trade_type === 'buy'){
  //     var buy_qty = this.state.buy_tp_qty1;
  //     var float = parseFloat(buy_qty)
  //     let final = float - 0.1
  //     this.setState({buy_tp_qty1:final.toFixed(1)})
  // }  
  // }

  sl_minus(trade_type) {
    if (trade_type == 'sell') {
      if (this.state.sellqty1 === '') {
        var cur_val = this.state.sellprice;
        var cur_val_float = parseFloat(cur_val);
        var new_val = cur_val_float - parseFloat(this.state.sell_value);
        

        if (new_val < this.state.sellprice) {
          this.setState({ sellqty1: this.state.sellprice });
        }
        else {
          let sell_qty = cur_val_float - parseFloat(this.state.sell_value); 
          this.setState({ sellqty1: convertPrice(sell_qty) });
        }
      } else {
        var cur_val = this.state.sellqty1;
        var cur_val_float = parseFloat(cur_val);
        var new_val = cur_val_float - parseFloat(this.state.sell_value);
        

        if (new_val < this.state.sellprice) {
          this.setState({ sellqty1: this.state.sellprice });
        }
        else {
          let sell_qty = cur_val_float - parseFloat(this.state.sell_value);
          this.setState({ sellqty1: convertPrice(sell_qty) });
        }
      }
    } else if (trade_type === 'buy') {
      if (this.state.buyqty1 === '') {
        var cur_val = this.state.buyprice;
        var cur_val_float = parseFloat(cur_val);
        var new_val = cur_val_float - parseFloat(this.state.buy_value);
        if (new_val > this.state.buyprice) {
          this.setState({ buyqty1: this.state.buyprice });
        }
        else { 
          this.setState({ buyqty1: convertPrice(new_val) });
        }
        if(new_val < 0){
          this.setState({ buyqty1: 0 }); 
        }
      } else{
        var cur_val = this.state.buyqty1;
        var cur_val_float = parseFloat(cur_val);
        var new_val = cur_val_float - parseFloat(this.state.buy_value);

        if (new_val > this.state.buyprice) {
          this.setState({ buyqty1: this.state.buyprice });
        }
        else {
          let buy_qty = cur_val_float - parseFloat(this.state.buy_value);
          this.setState({ buyqty1: convertPrice(buy_qty) });
        }
        if(new_val < 0){
          this.setState({ buyqty1: 0 }); 
        }
      }

    }
  }
  sl_plus(trade_type) {
    if (trade_type === 'sell') {
      if (this.state.sellqty1 === '') {
        var cur_val = this.state.sellprice;
        var cur_val_float = parseFloat(cur_val);
        let sell_qty = cur_val_float + parseFloat(this.state.sell_value);
        // if (sell_qty > this.state.sellprice) {
        //   this.setState({ sellqty1: this.state.sellprice });
        // }
        // else {
          this.setState({ sellqty1: convertPrice(sell_qty) });
        // }
      } else {
        var cur_val = this.state.sellqty1;
        var cur_val_float = parseFloat(cur_val);
        let sell_qty = cur_val_float + parseFloat(this.state.sell_value);
        // if (sell_qty > this.state.sellprice) {
        //   this.setState({ sellqty1: this.state.sellprice });
        // }
        // else {
          this.setState({ sellqty1: convertPrice(sell_qty) });
        // }
      }
    } else if (trade_type === 'buy') {
      if (this.state.buyqty1 === '') {
        var cur_val = this.state.buyprice;
        var cur_val_float = parseFloat(cur_val);
        var new_val = cur_val_float + parseFloat(this.state.buy_value);
        if (new_val > this.state.buyprice) {
          this.setState({ buyqty1: this.state.buyprice });
        }
        else {
          this.setState({ buyqty1: convertPrice(new_val) });
        }
      }
      else {
        var cur_val = this.state.buyqty1;
        var cur_val_float = parseFloat(cur_val);
        var new_val = cur_val_float + parseFloat(this.state.buy_value);

        if (new_val > this.state.buyprice) {
          this.setState({ buyqty1: this.state.buyprice });
        }
        else {
          // let buy_qty = cur_val_float + 0.1;
          this.setState({ buyqty1: convertPrice(new_val) });
        }
      }
    }
  }
  tp_plus(trade_type) {

    if (trade_type == 'sell') {
      if (this.state.sell_tp_qty1 === '') {
        var cur_val = this.state.sellprice;
        var cur_val_float = parseFloat(cur_val);
        var new_val = cur_val_float + parseFloat(this.state.sell_value);
        if(new_val > this.state.sellprice){
          this.setState({ sell_tp_qty1: this.state.sellprice });
        }
        else{
          this.setState({ sell_tp_qty1: convertPrice(new_val) });
        }
      } else {
        var cur_val = this.state.sell_tp_qty1;
        var cur_val_float = parseFloat(cur_val);
        var new_val = cur_val_float + parseFloat(this.state.sell_value);
        if(new_val > this.state.sellprice){
          this.setState({ sell_tp_qty1: this.state.sellprice });
        }
        else{
          this.setState({ sell_tp_qty1: convertPrice(new_val) });
        }
      }
    } else if (trade_type == 'buy') {
      if (this.state.buy_tp_qty1 === '') {
        var cur_val = this.state.buyprice;
        var cur_val_float = parseFloat(cur_val);
        let buy_qty = cur_val_float + parseFloat(this.state.buy_value);
        this.setState({ buy_tp_qty1: convertPrice(buy_qty) });
      }
      else {
        var cur_val = this.state.buy_tp_qty1;
        var cur_val_float = parseFloat(cur_val);
        let buy_qty = cur_val_float + parseFloat(this.state.buy_value);
        this.setState({ buy_tp_qty1: convertPrice(buy_qty) });
      }
    }
  }

  tp_minus(trade_type) {
    if (trade_type == 'sell') {
      if (this.state.sell_tp_qty1 === '') {
        var cur_val = this.state.sellprice;
        var cur_val_float = parseFloat(cur_val);
        var new_val = cur_val_float - parseFloat(this.state.sell_value);

        if (new_val < 0 ) {
          this.setState({ sell_tp_qty1: 0 });
        }
        else {
          // let sell_qty = cur_val_float - 0.1;
          this.setState({ sell_tp_qty1: convertPrice(new_val) });
        }
      } else {
        var cur_val = this.state.sell_tp_qty1;
        var cur_val_float = parseFloat(cur_val);
        var new_val = cur_val_float - parseFloat(this.state.sell_value);

        if (new_val < 0) {
          this.setState({ sell_tp_qty1: 0 });
        }
        else {
          // let sell_qty = cur_val_float - 0.1;
          this.setState({ sell_tp_qty1: convertPrice(new_val) });
        }
      }


    } else if (trade_type == 'buy') {
      if (this.state.buy_tp_qty1 === '') {
        var cur_val = this.state.buyprice;
        var cur_val_float = parseFloat(cur_val);
        var new_val = cur_val_float - parseFloat(this.state.buy_value);

        if (new_val < this.state.buyprice) {
          this.setState({ buy_tp_qty1: this.state.buyprice });
        }
        else {
          // let buy_qty = cur_val_float - 0.1;
          this.setState({ buy_tp_qty1: convertPrice(new_val) });
        }
      } else {
        var cur_val = this.state.buy_tp_qty1;
        var cur_val_float = parseFloat(cur_val);
        var new_val = cur_val_float - parseFloat(this.state.buy_value);

        if (new_val < this.state.buyprice) {
          this.setState({ buy_tp_qty1: this.state.buyprice });
        }
        else {
          // let buy_qty = cur_val_float - 0.1;
          this.setState({ buy_tp_qty1: convertPrice(new_val) });
        }
      }
    }
  }

  BuyIncrementItem = (e) => {
    e.preventDefault();
    this.state.buyprice =convertPrice(this.state.buyprice) + 0.000001;
    let price=this.state.buyprice;

    this.setState({ buyprice: convertPrice(price) });
  }
  BuyDecreaseItem = () => {
    if(this.state.buyprice != 0.000000 && this.state.buyprice > 0.000001){
      this.setState({ buyprice: convertPrice(this.state.buyprice) - 0.000001 });
    }
  }

  SellIncrementItem = () => {
    this.state.sellprice = convertPrice(this.state.sellprice) + 0.000001;
    let price=this.state.sellprice;

    this.setState({ sellprice: convertPrice(price) });
  }
  SellDecreaseItem = () => {
    if(this.state.sellprice != 0.000000 && this.state.sellprice > 0.000001){
      this.setState({ sellprice: convertPrice(this.state.sellprice) - 0.000001 });
    }
  }


  authorizationCheck = (pathname, viewRight) => {
    let { currentUser } = this.props;
    let listCheck = authorizationView(currentUser, pathname, viewRight);
    return listCheck;
}

handleBuystop_take(e){
  e.preventDefault();
  if(e.target.value === 'take'){
    // this.setState({buystop_take:true,buyqty1:0});
    this.setState({buystop_take:true});
  }else{
    this.setState({buystop_take:false});
  }
}
handleSellstop_take(e){
  e.preventDefault();
  if(e.target.value === 'take'){
    // this.setState({sellstop_take:true, sellqty1:0});
    this.setState({sellstop_take:true});
  }else{
    this.setState({sellstop_take:false});
  }
}
closeOneTrade = () => {
  let data = this.state.closedata;
  let trade ={
    username: (data.username),
    trade_id: (data.trade_id)
  }
 oneTrade(trade).then(res => {
  let { currentUser } = this.props;
  let user = { 'email': currentUser }
 open_trades_by_username(user).then(res => {
    if (Array.isArray(res) && res.length !== 0) {
      this.setState({ tableContent: res }, () => {
      });
    }
  });
   
 });
 }
 closePopup = (row) => {
  this.setState({closedata: row})
}

CloseTrades = () => {

  const { currentUser } = this.props;
  let data = {
    'email': localStorage.getItem('currentUserId')
  }
  close_trades(data).then(res => {
    if(res != ''){
      let data = { 'email': localStorage.getItem('currentUserId') }
      open_trades_by_username(data).then(res => {
        if (Array.isArray(res) && res.length !== 0) {
          this.setState({ tableContent: res }, () => {
          });
        }else{
          this.setState({ tableContent: res }, () => {
          });
        }
      });
    }
  });
}

getallTrade() {
  this.setState({alltrade: !this.state.alltrade})
}

  render() {
    let nameassest = this.state.assetName.split('/');
    let assest_id = this.state.asset_id;
    let table_item = this.state.alltrade ? this.state.tableContent : this.state.tableContent.filter(function(daa){
      return daa.asset_id === assest_id;
    });
    let no_record;
    if (table_item === undefined || table_item.length == 0) {
      no_record = [];
    } else {
      no_record = table_item;
    }

    const columns = [
      {
        name: 'Date',
        selector:'date1'
      },
      {
        name: 'Type',
        selector:'type'
      },
      {
        name: 'Asset Name',
        selector:'asset'
      },
      {
        name: 'Investment',
        selector:'investment'
      },
      {
        name: 'Leverage',
        selector:'leverage'
      },
      {
        name: 'Entry Rate',
        selector:'entry_rate'
      },
      {
        name: 'Take Profit',
        selector:'take_profit'
      },
      {
        name: 'Stop Loss',
        selector:'stop_loss'
      },
      {
        name: 'Current Rate',
        selector:'current_rate'
      },
      {
        name: 'P/L',
        selector:'p_l'
      },
      {
        name: 'Profit Percentage',
        selector:'profit_percent'
      },
      {
        name: 'Required Margin',
        selector:'required_margin'
      },
      {
        name: 'Free Margin',
        selector:'free_margin'
      },
      {
        name: 'Action',
        selector:'',
        cell: row => <button type="button" className="btn btn-trade" data-toggle="modal" data-target="#myModal" onClick={() => { this.closePopup(row) }}>Close</button>,
      },

    ];


    var settingss = {
      dots: false,
      infinite: true,
      speed: 500,slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settingss: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 768,
          settingss: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settingss: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }]
    };

    let new_item=this.state.assets;

    let show_div= this.state.show;
    let { isAuthenticated} = this.props;
    if (!isAuthenticated) {
      return (<Redirect to='/login' />);
    }
    return (      
      <div className="dashboard_login">
        <Helmet>
              <style>{'body { background-color: #eceff54d; } .container{ padding:0px;} .navbar{padding-right: 0px; padding-left:0px;}'}</style>
          </Helmet>
        {/* <div className="container">
          <div className="row margin-top-50">
            <div className="col-md-12 text-center">
              <button type="button" class="btn btn-signup-custom float-right" onClick={(e)=> this.addFavourite(e)}>Add asset to favourites</button>
            </div>            
          </div>
        </div> */}
        <div className="container">        
        {(this.state.isLoading) &&
        <>
            <div className="row margin-top-20 dashboard-section padding-top-10 text-center">              
              <div className="col-md-2 offset-md-5"  style={{"marginLeft": "45%","marginBottom":"10px"}}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />                
              </div>
            </div>
            </>}
            <div className="hide_on_mobile">
            {(() => {
                const options = [];

                for (let i = 0; i < new_item.length; i++) {
                  // let max='';
                  //             let min='';
                  //             let percent = '';
                  //            if(new_item[i].max && new_item[i].min){
                  //             this.max = new_item[i].max;
                  //             this.min = new_item[i].min;
                  //             let sum = ((new_item[i].max + new_item[i].min)/2);
                  //             let sum1 = new_item[i].max - new_item[i].min ;
                  //             let percents = (sum1/sum)*100;
                  //             this.percent = percents.toFixed(2);
                  //            }else{
                  //             this.max = 0;
                  //             this.min = 0;
                  //             this.percent = 0;
                  //            }
                  options.push(
                  <div className={"row margin-top-20 dashboard-section padding-top-10 text-center " + ((this.state.active === new_item[i].asset) ? 'show': 'hide')}  id={'div-id-'+new_item[i].id} key={new_item[i].id}>
                      <div className="width-215 text-left">                
                        <div className="assets-heading">
                        <div className="curncy_image">
                            <img alt="#" src={require(`../../../images/icons/${new_item[i].base_icon}`)}/>
                            </div>
                            <div className="asset-name-class"> {new_item[i].asset} </div>
                        </div>
                      </div>
                      <div className="width-1">
                        <p className="lg-change-text">Fair Value</p>
                      </div>
                      <div className="width-10 text-center">
                        <div className="point-value">
                          {new_item[i].spread <0 ?
                          <b className="low">{convertSpread(new_item[i].spread)} <img alt="#" src={require("../../../images/Path.svg")} />
                          <span className="tooltiptext">{convertSpread(new_item[i].spread)}</span>
                          </b>
                          :
                          <b className="heigh">{convertSpread(new_item[i].spread)} <img alt="#" src={require("../../../images/Path-1.svg")} />
                          <span className="tooltiptext">{convertSpread(new_item[i].spread)}</span>
                          </b>
                          }
                        </div>
                      </div>
                      <div className="width-1">
                        <p className="lg-change-text">24h Change</p>
                      </div>
                      <div className="width-9 text-center">
                        <div className="status-balance">
                          {new_item[i]['24h_change']<0 ?
                          <b className="low">{new_item[i]['24h_change']}%</b>
                          :
                          <b className="heigh">{new_item[i]['24h_change']}%</b>
                          }
                        </div>
                      </div>
                      <div className="width-1">
                        <p className="lg-change-text">24h High</p>
                      </div>
                      <div className="width-11 text-center">
                        <div className="status">
                            <b>{new_item[i].max}</b>
                        </div>
                      </div>
                      <div className="width-1">
                        <p className="lg-change-text">24h Low</p>
                      </div>
                      <div className="width-9 text-center">
                        <div className="status">
                            <b>{new_item[i].min} </b>
                        </div>
                      </div>
                      <div className="width-12 text-center">
                        <div className="asset-info">
                        <OverlayTrigger trigger="click" placement="bottom" rootClose={true} overlay={
                        <Popover id={"popover-basic"+ new_item[i].id}>
                            <Popover.Title as="h3">Assets Info</Popover.Title>
                            <Popover.Content>
                              {new_item[i].base_info}
                            </Popover.Content>
                          </Popover>
                        }>
                          <b><img alt="#" src={require("../../../images/Vector.svg")}/> <span>Asset Info</span></b>
                        </OverlayTrigger>                            
                        </div>
                      </div>
                  </div>);
                }

                return options;
              })()}
            </div>
            <div className="row margin-top-30">              
                <div className="table-assets">
                  <div className="dashboard-table">
                  <div className="add_assest">
                    <button type="button" className="btn add_assest_button float-right" onClick={(e) => this.addFavourite(e)}>Add asset to favourites</button>
                  </div>
                    <ul className="table" type="none"  style={{'padding':'0px'}}>
                        <li className="VOLUMEhour">
                          <div className="heading-table"><div>24H VOLUME | 24H CHANGE</div></div>
                        </li>
                          {/* {items} */}
                          {(() => {
                            const options = [];

                            for (let i = 0; i < new_item.length; i++) {
                            //   let max='';
                            //   let min='';
                            //   let percent = '';
                            //  if(new_item[i].max && new_item[i].min){
                            //   this.max = new_item[i].max;
                            //   this.min = new_item[i].min;
                            //   let sum = ((new_item[i].max + new_item[i].min)/2);
                            //   let sum1 = new_item[i].max - new_item[i].min ;
                            //   let percents = (sum1/sum)*100;
                            //   this.percent = percents.toFixed(2);
                            //  }else{
                            //   this.max = 0;
                            //   this.min = 0;
                            //   this.percent = 0;
                            //  }
                              options.push(<li className={ ((this.state.active === new_item[i].asset) ? 'active': '')} id={'li'+new_item[i].id} key={new_item[i].id} onClick={ () => this.clickState(new_item[i].id,new_item[i].asset,new_item[i].buy_rate, new_item[i].sell_rate, new_item[i].base_icon) }>
                                <div className="row"  style={{'padding':'15px'}}>
                                  <div className="col-2">
                                  <div className="curncy_image">
                                    <img alt="#" src={require(`../../../images/icons/${new_item[i].base_icon}`)}/>
                                    </div>
                                    </div>
                                  <div className=" col-5"><p className="heading-assets">{new_item[i].asset}</p><p className="heading-value">{convertPrice(new_item[i].buy_rate)}</p></div>
                                  <div className="col-5"><p className="change-text">24h Change</p>
                                  {new_item[i]['24h_change']<0 ?
                                  <p className="positive-text low">{new_item[i]['24h_change']}%</p>
                                  :
                                  <p className="positive-text heigh">{new_item[i]['24h_change']}%</p>
                                  }
                                  </div>
                                </div>
                            </li>);
                            }

                            return options;
                          })()}
                    </ul>
                    <div className="mobile_slider">
                          <h2 className="volume_heading">24H VOLUME | 24H CHANGE</h2>
                    <Slider {...settingss}>
                          {(() => {
                            const options = [];

                            for (let i = 0; i < new_item.length; i++) {
                              let max='';
                              let min='';
                              let percent = '';
                             if(new_item[i].max && new_item[i].min){
                              this.max = new_item[i].max;
                              this.min = new_item[i].min;
                              let sum = ((new_item[i].max + new_item[i].min)/2);
                              let sum1 = new_item[i].max - new_item[i].min ;
                              let percents = (sum1/sum)*100;
                              this.percent = percents.toFixed(2);
                             }else{
                              this.max = 0;
                              this.min = 0;
                              this.percent = 0;
                             }
                              
                              options.push(
                                <div>
                                <div className="tab-body">
                              <div className={ ((this.state.active === new_item[i].asset) ? 'active': '')} id={'li'+new_item[i].id} key={new_item[i].id} onClick={ () => this.clickState(new_item[i].id, new_item[i].asset, new_item[i].buy_rate, new_item[i].sell_rate, new_item[i].base_icon) }>
                                <div className="row"  style={{'padding':'15px'}}>
                                  <div className="col-2">
                                  <div className="curncy_image">
                                    <img alt="#" src={require(`../../../images/icons/${new_item[i].base_icon}`)}/>
                                    </div>
                                    </div>
                                  <div className=" col-5"><p className="heading-assets">{new_item[i].asset}</p><p className="heading-value">{convertPrice(new_item[i].buy_rate)}</p></div>
                              <div className="col-5"><p className="change-text">24h Change</p>
                              {new_item[i]['24h_change']<0 ?
                              <p className="positive-text low">{new_item[i]['24h_change']}%</p>
                            :
                              <p className="positive-text heigh">{new_item[i]['24h_change']}%</p>
                            }
                              </div>
                                </div>
                            </div>
                            </div>
                            </div>
                            );
                            }

                            return options;
                          })()}
                    {/* </ul> */}
                    </Slider>
                    </div>
                  </div>
                </div>
                <div className="dash-asset-section">
                <div className="col-md-12 graph-section margin-top-50">
                  <div className="graph_div">
                    <CandlestickChart assetCoin={this.state.assetName} assetIcon={this.state.assetIcon} asset_id={this.state.asset_id} />
                  </div>
                </div>
                {(this.state.isLoadingBuySell) ?

<div className="row margin-top-20 dashboard-section padding-top-10 text-center">
  <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px" }}>
    <ReactLoading color={'#3445CF'} type="spinningBubbles" />
  </div>
</div>
:
                  <div className="row dashboard-container">                  
                    <div className="buy col-md-6 padding-0 box-shadow margin-right-15">
                      <div className="buy-heading">
                        <h2>Buy <span className="assetname">{nameassest[0]}</span></h2>
                      </div>
                      
                      <form>
                      <div className="row">
                      <div className="col-md-6">
                        <p className="purple-text">Enter Amount</p>
                        {/* <span className={"gray-text "+((this.state.buyPercentage === 'buy-per-25')? this.state.buyPercentage :'')} onClick={()=> this.buyPercentage(25)}>25%</span>
                        <span className={"gray-text "+((this.state.buyPercentage === 'buy-per-50')? this.state.buyPercentage :'')}  onClick={()=> this.buyPercentage(50)}>50%</span>
                        <span className={"gray-text "+((this.state.buyPercentage === 'buy-per-75')? this.state.buyPercentage :'')}  onClick={()=> this.buyPercentage(75)}>75%</span>
                        <span className={"gray-text "+((this.state.buyPercentage === 'buy-per-100')? this.state.buyPercentage :'')}  onClick={()=> this.buyPercentage(100)}>100%</span> */}
                        
                          <div className="form-group">
                            <div className="input-group mb-3">
                              <input className="buy-input form-control" value={this.state.buy?(this.state.buy):""} step="0.00000001" onChange={(e)=>this.handleBuyAmountInput(e)} name="buy" placeholder="0" />
                              <div className="input-group-append">
                                    {/* <span className="input-group-text" style={{ }}> {this.state.assetName}
                                    </span> */}
                                    <span className="input-group-text" style={{ }}> EUR
                                    </span>
                              </div>
                            </div>
                            <p> <img src={require("../../../images/icons/Vector (1).svg")} alt="help" /> Minimum order amount is 26 EUR.</p>
                          </div>                                                   
                        
                      </div>
                      
                      <div className="col-md-6 padding-0">
                      <div className="circular_div">
                      {/* <div className="float-div float-left">
                          <img src={require("../../../images/icons/minus.svg")} role="button" onClick={this.BuyDecreaseItem} alt="help" />
                        </div> */}
                          
                          <div className="circular-buy-div float-left text-center">
                            
                          <img src={require("../../../images/icons/Polygon.svg")} alt="caret" />
                                <p className="enter-text">CURRENT RATE</p>
                                {/* <input className="price amount-text form-control" name="buy_price" step="0.00000001" onChange={(e => this.handleBuyInput(e))} value={this.state.buyprice?this.state.buyprice:"0"}/> */}
                                <div className="price amount-text form-control">{this.state.buyprice?this.state.buyprice:"0"}</div>
                                <p className="buy-text">BUY</p>

                          </div>
                      {/* <div className="float-left float-div">
                        <img src={require("../../../images/icons/plus.svg")} onClick={this.BuyIncrementItem} alt="help" />
                      </div> */}
                        </div>  
                      <div className="clearfix"></div>                        
                      </div>
                      </div>

                      <div className="row">
                      <div className="col-md-6">
                        {this.state.buystop_take ?
                         <div className="form-group">
                         <label className="purple-text">Take Profit</label>
                         <div className="input-group mb-3">
                           <input className="buy-input form-control" type="text" value={(this.state.buy_tp_qty1 !== '') ? this.state.buy_tp_qty1 : '0'} onChange={(e) => this.handletakeprofit(e, 'buy')} step="0.00000001" />
                           <img src={require("../../../images/icons/minus.svg")} onClick={(e) => this.tp_minus('buy', e)} name="minus1" className="minus" width="22" height="22" />
                           <img name="add1" src={require("../../../images/icons/plus.svg")} onClick={(e) => this.tp_plus('buy', e)} className="add" width="22" height="22" />
                         </div>
                       </div>
                        :
                        <div className="form-group">
                          <label className="purple-text">Stop Loss</label>
                          <div className="input-group mb-3">
                            <input className="buy-input form-control" type="text" value={(this.state.buyqty1 !== '') ? this.state.buyqty1 : '0'} onChange={(e) => this.handlestoploss(e, 'buy')} step="0.00000001" />
                            <div className="input-group-append">
                              <img src={require("../../../images/icons/minus.svg")} onClick={(e) => this.sl_minus('buy', e)} id="minus1" className="minus" width="22" height="22" />
                              <img id="add1" src={require("../../../images/icons/plus.svg")} onClick={(e) => this.sl_plus('buy', e)} className="add" width="22" height="22" />
                            </div>
                          </div>
                        </div>

                        }
                      </div>
                      <div className="col-md-6">
                      <select className="stop_take" onChange={(e) => this.handleBuystop_take(e)}>
                          <option value="stop">Stop loss</option>
                          <option value="take">Take Profit</option>
                        </select>
                      </div>
                    </div>

                      
                      {/* <div className="row">
                          <div className="col-md-6 ">
                          <div className="form-group">
                          <label className="purple-text">Stop Loss</label>
                            <div className="input-group mb-3">
                              <input className="buy-input form-control" type="text" value={(this.state.buyqty1 !== '')? this.state.buyqty1 : '0'}  onChange={(e) => this.handlestoploss(e,'buy')} step="0.00000001"  />
                              <div className="input-group-append">
                              <img src={require("../../../images/icons/minus.svg")}  onClick={(e)=> this.sl_minus('buy',e)} id="minus1" className="minus" width="22" height="22" />
                                <img id="add1" src={require("../../../images/icons/plus.svg")} onClick={(e)=> this.sl_plus('buy',e)} className="add" width="22" height="22" />                               
                              </div>
                            </div>
                          </div>
                          </div> 
                          <div className="col-md-6">
                            <div className="form-group">
                            <label className="purple-text">Take Profit</label>
                            <div className="input-group mb-3">
                              <input className="buy-input form-control" type="text" value={(this.state.buy_tp_qty1 !== '')? this.state.buy_tp_qty1 : '0'} onChange={(e) => this.handletakeprofit(e,'buy')} step="0.00000001" />
                              <img src={require("../../../images/icons/minus.svg")}  onClick={(e)=> this.tp_minus('buy',e)} name="minus1" className="minus" width="22" height="22" />
                              <img name="add1" src={require("../../../images/icons/plus.svg")} onClick={(e)=> this.tp_plus('buy',e)} className="add" width="22" height="22" />                              
                            </div>
                          </div>
                          </div>
                          </div> */}
                          <div className="row">
                            <div className="col-md-12">
                            <p class="purple-text">Leverage</p>
                              <select className="leverage_tag" value={(this.state.buyleverage !== '')? this.state.buyleverage : '0'} onChange={(e)=>this.handleleverage(e)} name="buy">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                              </select>
                            </div>
                            <div className="col-md-12">
                            <button className="buy_submit_button" onClick={(e) => this.buyMarketOrder('buy',e)}>Buy</button>
                            </div>
                          </div>
                      </form>
                      
                    </div>
                    <div className="sell col-md-6 padding-0 box-shadow">
                      <div className="sell-heading">
                        <h2>Sell <span className="assetname">{nameassest[0]}</span></h2>
                      </div>
                      <form>
                      <div className="row">
                      <div className="col-md-6">
                        <p className="purple-text">Enter Amount</p>
                        {/* <span className={"gray-text "+((this.state.sellPercentage === 'sell-per-25')? this.state.sellPercentage :'')} onClick={()=> this.sellPercentage(25)}>25%</span>
                        <span className={"gray-text "+((this.state.sellPercentage === 'sell-per-50')? this.state.sellPercentage :'')}  onClick={()=> this.sellPercentage(50)}>50%</span>
                        <span className={"gray-text "+((this.state.sellPercentage === 'sell-per-75')? this.state.sellPercentage :'')}  onClick={()=> this.sellPercentage(75)}>75%</span>
                        <span className={"gray-text "+((this.state.sellPercentage === 'sell-per-100')? this.state.sellPercentage :'')}  onClick={()=> this.sellPercentage(100)}>100%</span> */}
                        
                          <div className="form-group">
                          <div className="input-group mb-3">
                          <input className="sell-input form-control" name="sell" value={this.state.sell?(this.state.sell):""} step="0.00000001" onChange={(e)=>this.handleSellAmountInput(e)} placeholder="0" />
                              <div className="input-group-append">
                                  {/* <span className="input-group-text" style={{ }}> {this.state.assetName}
                                   </span> */}
                                   <span className="input-group-text" style={{ }}> EUR
                                   </span>
                              </div>
                            </div>                          
                          <p> <img src={require("../../../images/icons/Vector (1).svg")} alt="help" /> Minimum order amount is 26 EUR.</p>
                          </div> 
                      </div>
                      <div className="col-md-6 padding-0">
                      <div className="circular_div">
                      {/* <div className="float-div float-left">
                          <img src={require("../../../images/icons/minus.svg")} onClick={this.SellDecreaseItem} alt="help" />
                        </div> */}
                          
                          {/* <div className="circular-sell-div float-left text-center"  onClick={(e) => this.sellMarketOrder('sell',e)}> */}
                          <div className="circular-sell-div float-left text-center">
                            
                          <img src={require("../../../images/icons/redPolygon.svg")} alt="caret" />
                                <p className="enter-text">CURRENT RATE</p>
                                {/* <input className="price amount-text form-control" name="sell_price" onChange={(e => this.handleSellInput(e))} value={this.state.sellprice?this.state.sellprice:"0"}/> */}
                                <div className="price amount-text form-control">{this.state.sellprice?this.state.sellprice:"0"}</div>
                                <p className="sell-text">SELL</p>

                          </div>
                      {/* <div className="float-left float-div">
                        <img src={require("../../../images/icons/plus.svg")} onClick={this.SellIncrementItem} alt="help" />
                      </div> */}
                      </div>
                      </div>
                      </div>

                       <div className="row">
                      <div className="col-md-6">
                        {this.state.sellstop_take ?
                         <div className="form-group">
                         <label className="purple-text">Take Profit</label>
                         <div className="input-group mb-3">
                           <input className="sell-input form-control" type="text" value={(this.state.sell_tp_qty1 !== '') ? this.state.sell_tp_qty1 : '0'} onChange={(e) => this.handletakeprofit(e, 'sell')} step="0.00000001" />
                           <img id="sellminus1" src={require("../../../images/icons/minus.svg")} onClick={(e) => this.tp_minus('sell', e)} className="minus" width="25" height="25" />
                           <img id="selladd1" src={require("../../../images/icons/plus.svg")} onClick={(e) => this.tp_plus('sell', e)} width="25" height="25" />
                         </div>
                       </div>
                        :
                        <div className="form-group">
                          <label className="purple-text">Stop Loss</label>
                          <div className="input-group mb-3">
                            <input className="sell-input form-control" type="text" value={(this.state.sellqty1 !== '') ? this.state.sellqty1 : '0'} onChange={(e) => this.handlestoploss(e, 'sell')} step="0.00000001" />
                            <div className="input-group-append">
                              <img id="sellminus1" src={require("../../../images/icons/minus.svg")} onClick={(e) => this.sl_minus('sell', e)} width="22" height="22" />
                              <img id="selladd1" src={require("../../../images/icons/plus.svg")} onClick={(e) => this.sl_plus('sell', e)} width="22" height="22" />
                            </div>
                          </div>
                        </div>

                        }
                      </div>
                      <div className="col-md-6">
                      <select className="stop_take" onChange={(e) => this.handleSellstop_take(e)}>
                          <option value="stop">Stop loss</option>
                          <option value="take">Take Profit</option>
                        </select>
                      </div>
                    </div>

                      {/* <div className="row">
                          <div className="col-md-6 ">
                          <div className="form-group">
                          <label className="purple-text">Stop Loss</label>                          
                            <div className="input-group mb-3">
                              <input className="sell-input form-control" type="text" value={(this.state.sellqty1 !== '')? this.state.sellqty1 : '0' } onChange={(e) => this.handlestoploss(e,'sell')} step="0.00000001"  />
                              <div className="input-group-append">
                              <img id="sellminus1" src={require("../../../images/icons/minus.svg")}  onClick={(e)=> this.sl_minus('sell',e)} width="22" height="22" />
                                <img id="selladd1" src={require("../../../images/icons/plus.svg")} onClick={(e)=> this.sl_plus('sell',e)}  width="22" height="22" />                      
                              </div>
                            </div>
                          </div>
                          </div> 
                          <div className="col-md-6">
                            <div className="form-group">
                            <label className="purple-text">Take Profit</label>
                            <div className="input-group mb-3">
                              <input className="sell-input form-control" type="text" value={(this.state.sell_tp_qty1 !== '')? this.state.sell_tp_qty1 : '0'} onChange={(e) => this.handletakeprofit(e,'sell')} step="0.00000001" />
                              <img id="sellminus1" src={require("../../../images/icons/minus.svg")}  onClick={(e)=> this.tp_minus('sell',e)} className="minus" width="25" height="25" />
                              <img id="selladd1" src={require("../../../images/icons/plus.svg")} onClick={(e)=> this.tp_plus('sell',e)} width="25" height="25" />
                            </div>
                          </div>
                          </div>
                          </div> */}
                          <div className="row">
                            <div className="col-md-12">
                            <p class="purple-text">Leverage</p>
                              <select className="leverage_tag"value={(this.state.sellleverage !== '')? this.state.sellleverage : '0'} onChange={(e)=>this.handleleverage(e)} name="sell">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="5">5</option>
                                <option value="10">10</option>
                              </select>
                            </div>
                            <div className="col-md-12">
                            <button className="sell_submit_button" onClick={(e) => this.sellMarketOrder('sell',e)}>Sell</button>
                            </div>
                          </div>
                          </form>
                      </div>                     
                  </div>
                  }
          {/* <div className="row margin-top-50 dashboard-section padding-top-10 text-center">
            <div className="col-md-12 margin-bottom-50" >
              <div className="table-responsive">
                <table className="table table-striped ">
                  <tbody>
                    <tr>
                      <th>Date</th>
                      <th> TradeId</th>
                      <th>Type</th>
                      <th>Asset Name</th>
                      <th>AssetId</th>
                      <th>Investment</th>
                      <th>Leverage</th>
                      <th>Entry Rate</th>
                      <th>Take Profit</th>
                      <th>Stop Loss</th>
                      <th>Current Rate</th>
                      <th>P/L</th>
                      <th>Profit Percentage</th>
                      <th>Required Margin</th>
                      <th>Free Margin</th>
                      <th>Action</th>
                    </tr>
                    {(no_record === 'Record Not Found') &&
                      <>
                        <tr><td colSpan="17">{no_record}</td></tr>
                      </>}
                    {(no_record !== 'Record Not Found') &&
                      <>
                        {no_record.map((data) =>
                          <tr>
                            <td><div className="td_div">{data.date1}</div></td>
                            <td>{data.trade_id}</td>
                            <td><div className="td_div">{data.type}</div></td>
                            <td><div className="td_div">{data.asset}</div></td>
                            <td>{data.asset_id}</td>
                            <td><div className="td_div">{data.investment}</div></td>
                            <td><div className="td_div">{data.leverage}</div></td>
                            <td><div className="td_div">{data.entry_rate}</div></td>
                            <td><div className="td_div">{data.take_profit}</div></td>
                            <td><div className="td_div">{data.stop_loss}</div></td>
                            <td><div className="td_div">{data.current_rate}</div></td>
                            <td><div className="td_div">{data.p_l}</div></td>
                            <td><div className="td_div">{data.profit_percent}</div></td>
                            <td><div className="td_div">{data.required_margin}</div></td>
                            <td><div className="td_div">{data.free_margin}</div></td>
                            <td><button type="button" className="btn btn-trade" onClick={() => { this.closeOneTrade(data) }}>Close Trade</button></td>
                            <td><button type="button" className="btn btn-trade" data-toggle="modal" data-target="#myModal" onClick={() => { this.closePopup(data) }}>Close Trade</button></td>
                          </tr>
                        )
                        }
                      </>}
                  </tbody>
                </table>
              </div>
              <div className="text-right margin-top-20 open_trades_button">
                <button type="button" className="btn btn-signup-custom" data-toggle="modal" data-target="#closeall">Close All Trades</button>
              </div>
            </div>
          </div> */}

                
                    {/* Modal  */}
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
     {/* Modal content */}
      <div class="modal-content">
        <div class="modal-body">
          <h4>Are you sure you want to close trade?</h4>
          <button type="button" className="btn btn-trade-yes" data-dismiss="modal" onClick={() => { this.closeOneTrade() }}>Yes</button>
          <button type="button" class="btn btn-trade-no" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>

         {/* Modal  */}
         <div class="modal fade" id="closeall" role="dialog">
    <div class="modal-dialog">
    
     {/* Modal content */}
      <div class="modal-content">
        <div class="modal-body">
          <h4>Are you sure you want to close all trade?</h4>
          <button type="button" className="btn btn-trade-yes" data-dismiss="modal" onClick={this.CloseTrades}>Yes</button>
          <button type="button" class="btn btn-trade-no" data-dismiss="modal">No</button>
        </div>
      </div>
      
    </div>
  </div>
                  
                </div>
              {/* </div> */}
              <div className="table-title">
                <h2 className="text-center"><img alt="#" src={require("../../../images/line.png")}/> Open Trades</h2>
                <button type="button" className="btn btn-signup-custom all_trades" onClick={this.getallTrade} >{this.state.alltrade ? 'Single Trade' :"All Trades"}</button>              
            
              </div>
            <DataTable className="ul_scrollbar scrollbar_all_trades" columns={columns} data={no_record}/>
            <div className="margin-top-20 open_trades_button">
                <button type="button" className="btn btn-signup-custom" data-toggle="modal" data-target="#closeall">Close All Trades</button>
              </div>
            </div>
            
            
          </div>
        </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.AuthReducer.isLoading,
    accessToken: state.AuthReducer.accessToken,
    isAuthenticated: state.AuthReducer.isAuthenticated,
    currentUser: state.AuthReducer.currentUser,
    isbtcRate: state.AuthReducer.isbtcRate,
    isTab: state.AuthReducer.isTab,
    CONTACT_US: state.AuthReducer.CONTACT_US,
    balance: state.AuthReducer.balance
  }
}


// const mapDispatchToProps = (dispatch) => {
//   return {
//     logoutFun: () => dispatch(logoutFun())
//   }
// }

export default connect(mapStateToProps,{ getAssets, getComAssets, createMarket , getRemoteBtcRate, add_fav_assets, open_trades_by_username, oneTrade, close_trades}
  )(Commodities)
