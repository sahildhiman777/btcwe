import { Helmet } from "react-helmet";
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { resetRequest, verifyToken, updateNewPassword } from '../../actions/authAction';
import { ToastTopEndErrorFire } from '../../toast/toast';
import { Button, FormGroup, FormControl, FormLabel, Card, InputGroup } from "react-bootstrap";
import "../ResetPassword/resetPassword.css";
import ReactLoading from "react-loading";
import * as Scroll from 'react-scroll';
import $ from 'jquery';


let scroll = Scroll.animateScroll;
export class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectState: false,
      password: "",
      repeat_password: "",
      old_password: "",
      email: "",
      message: '',
      type: 'password',
      type2: 'password',
      blockloader:true,
    }
  }
  validateForm = () => {
    return this.state.email.length > 0 && (this.state.password === this.state.repeat_password) && this.state.password.length > 0 && this.state.repeat_password.length > 0;
  }
  componentDidMount() {
    $(document).ready(function () {
      $(".mobile_menu_icon").addClass("hide_mobile");
    });
    scroll.scrollToTop({ smooth: "linear", duration: 100 });
  }


  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  reset = (e) => {
    e.preventDefault();
    if (this.state.password !== this.state.repeat_password) {
      return this.setState({ message: 'Password not matched!!' });
    } else {
      let data = {
        email: (this.state.email !== '') ? this.state.email : "",
        password: (this.state.password !== '') ? this.state.password : "",
        old_password: (this.state.old_password !== '') ? this.state.old_password : ""
      }
      this.setState({ message: '' });
      this.props.updateNewPassword(data);
    }
  }
  componentWillMount() {
    $(document).ready(function () {
      $(".mobile_menu_icon").addClass("hide_mobile");
    });
    const query = new URLSearchParams(this.props.location.search);
    const token = query.get('token');
    let data = { 'token': token };
    verifyToken(data).then(res => {
      if(res.props != undefined){
        if(res.props.to){
            this.props.history.push("/countryblocked")
            this.setState({blockloader:false});
        }
    }else{
      this.setState({blockloader:false});
      if (res !== '') {
        if (res.message === 'invalid signature') {
          ToastTopEndErrorFire(res.message);
          this.props.history.push('/forgot-password');
        } else if (res.message === 'jwt expired') {
          ToastTopEndErrorFire(res.message);
          this.props.history.push('/forgot-password');
        } else {
          if (res.message !== '') {
            let response = res.message.data;
            let email = response.email;
            let pass = response.pass;
            this.setState({ old_password: pass, email: email });
          }
        }
      }
    }
    });
  }
  handleClick = () => this.setState(({ type }) => ({
    type: type === 'text' ? 'password' : 'text'
  }));
  handleClick2 = () => this.setState(({ type2 }) => ({
    type2: type2 === 'text' ? 'password' : 'text'
  }));
  componentDidMount() {
  }

  componentWillReceiveProps(nxtProps) {
    if (nxtProps.isAuthenticated) {
      nxtProps.history.push('/dashboard');
    }
  }

  render() {
    if (this.props.isRedirect !== '') {
      this.props.history.push(this.props.isRedirect)
    }
    return (
      <div className=" reset-section-cointainer">
        {(this.state.blockloader) &&
          <>
            <div className="blockloader">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
          </>}
        <div className="container padding-top-50">
          <Helmet>
            <style>
              {".header2 .container{ max-width:unset; margin:unset;padding-left:6%;} #root{overflow:hidden;} .reset-section-cointainer .container { transform: skew(0deg, 2deg);}.reset-section-cointainer { transform: skew(0deg, -2deg); background:#f9fafe; background-repeat: no-repeat; border-radius: 0px 104px 0px 104px;} ul.ml-auto{display:none !important;}  .footer{ display:none !important;}"}</style>
          </Helmet>
          {(this.state.isLoading) &&
            <>
              <div className="row margin-top-20 dashboard-section padding-top-10 text-center">
                <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px" }}>
                  <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                </div>
              </div>
            </>}
          <div className="row margin-top-50">
            <div className=" col-md-12 col-lg-6 reset-section">
              <div className="col-md-9 offset-md-1">
                <h1 className="reset-header-text text-left">Reset password?</h1>
                <p className="text-left margin-bottom-20">Enter new password</p>
                <Card >
                  <Card.Body>
                    <form onSubmit={(e) => { this.reset(e) }}>
                      <FormGroup controlId="password">
                        <FormLabel>New Password</FormLabel>

                        <InputGroup className="mb-3">
                          <FormControl
                            className="border-right-unset"
                            name="password"
                            value={this.state.password}
                            onChange={this.handleChange}
                            type={this.state.type}
                          />
                          <InputGroup.Append>
                            <InputGroup.Text onClick={this.handleClick}> <img src={require('../../images/icons/visibility.svg')} alt="#" /> </InputGroup.Text>
                          </InputGroup.Append>
                        </InputGroup>
                      </FormGroup>
                      {(this.state.message !== '') &&
                        <>
                          <FormLabel className="text-left text-danger">{this.state.message}</FormLabel>
                        </>
                      }
                      <FormGroup controlId="repeat_password" className="margin-bottom-50">
                        <FormLabel>Repeat New Password</FormLabel>

                        <InputGroup className="mb-3">
                          <FormControl
                            className="border-right-unset"
                            name="repeat_password"
                            value={this.state.repeat_password}
                            onChange={this.handleChange}
                            type={this.state.type2}
                          />
                          <InputGroup.Append>
                            <InputGroup.Text onClick={this.handleClick2}> <img src={require('../../images/icons/visibility.svg')} alt="#" /> </InputGroup.Text>
                          </InputGroup.Append>
                        </InputGroup>
                      </FormGroup>
                      <div className="reset_button_div">
                        <Button className="btn-signup-custom" disabled={!this.validateForm()} type="submit" > Reset </Button>
                      </div>
                    </form>
                  </Card.Body>
                </Card>
              </div>
            </div>
            <div className="col-md-12 col-lg-6 message-section">
              <div className="col-md-12 col-lg-12 message-section-2">
                <div className="text-rotate">
                  <div className="margin-bottom-20">
                    <img src={require('../../images/bckground/undraw_authentication_fsn5 1.svg')} className="img-fluid" alt="#" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-3 col-lg-4"></div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.AuthReducer.isAuthenticated,
    accessToken: state.AuthReducer.accessToken,
    isRedirect: state.AuthReducer.isRedirect,
    isLoading: state.AuthReducer.isLoading,
  };
};

const mapDispatchToProps = (dispatch) => ({
  resetRequest: (data) => dispatch(resetRequest(data)),
  verifyToken: (data) => dispatch(verifyToken(data)),
  updateNewPassword: (data) => dispatch(updateNewPassword(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword)