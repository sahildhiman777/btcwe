import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { loginRequest, updateRedirectRequest, getCountry } from '../../actions/authAction';
import { Button, FormGroup, FormControl, FormLabel, Card } from "react-bootstrap";
import "../privacyPolicy/privacyPolicy.css";
import * as Scroll from 'react-scroll';
import $ from 'jquery';
import ReactLoading from "react-loading";

let scroll = Scroll.animateScroll;
class PrivacyPolicy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectState: false,
      email: "test3@gmail.com",
      username: "test",
      blockloader:true,
    };
  }
  componentWillMount(){
    getCountry().then(res => {
      if(res.props != undefined){
      if(res.props.to){
          this.props.history.push("/countryblocked");
          this.setState({blockloader:false});
      }
  }else{
    this.setState({blockloader:false});
  }
  })
  }
  componentDidMount() {
    $(document).ready(function () {
      $(".mobile_menu_icon").removeClass("hide_mobile");
    });
    scroll.scrollToTop({ smooth: "linear", duration: 100 });
  }

  render() {
    return (
      <>
       {(this.state.blockloader) &&
          <>
            <div className="blockloader">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
          </>}
        <div className="container">
          <div className="row margin-top-50 body-color">
            <div className="col-md-12">
              <div class="theme-title">
                <h2 class="text-center">PRIVACY POLICY</h2>
              </div>
              <p className="paragraph-color">This Privacy Policy describes how BTCWE UK, Ltd ("BTCWE") collects, uses, stores, shares, and protects your information whenever you use BTCWE.io, a BTCWE mobile app, any BTCWE or third party applications relying on such an API (the "BTCWE Site") or any BTCWE Services. By using the BTCWE Site and BTCWE Services, you consent to the data practices prescribed in this statement. We may periodically post changes to this Privacy Policy on this page, and it is your responsibility to review this Privacy Policy frequently and we encourage you to visit this page often. When required by law, we will notify you of any changes to this Privacy Policy.</p>
              <p className="paragraph-color dark-txt"><b>Last updated: January 25,2018</b></p>

              <h4 className="question-color"><img alt="#" src={require("../../images/line.png")} /> HOW WE COLLECT INFORMATION ABOUT YOU</h4>
              <p className="paragraph-color">When you use BTCWE Services, we collect information sent to us through your computer, mobile phone, or other access device. This information may include your IP address, device information including, but not limited to, identifier, device name and type, operating system, location, mobile network information, and standard web log information, such as your browser type, traffic to and from our site, and the pages you accessed on our website. BTCWE does not intentionally collect information from or about any individual who is under 18 years old.</p>
              <p className="paragraph-color">If you create an account or use BTCWE Services, we, or our affiliates vendors acting on our behalf may collect the following types of information:</p>
              <ul>
                <li>Contact information - your name, address, phone, email, Skype ID, and other similar information; and</li>
                <li>Financial information - the full bank account and routing numbers and/or credit card numbers that you link to your BTCWE Account or input when you use paid BTCWE Services. If you do not use the BTCWE Conversion Service, you may opt out of providing this information.</li>
              </ul>
              <p className="paragraph-color">If you seek permissions to raise Digital Currency buy and sell limits associated with your BTCWE Account, we may require you to provide additional information which we may use in collaboration with service providers acting on our behalf to verify your identity or address, and/or to manage risk. This information may include your date of birth, taxpayer or government identification number, a copy of your government-issued identification, or other personal information. We may also obtain information about you from third parties such as credit bureaus and identity verification services.</p>
              <p className="paragraph-color">When you use BTCWE Services, we collect information about your transactions and/or your other activities on our website and we may continuously collect information about your computer, mobile device, or other access device for fraud prevention purposes, to monitor for possible breach of your BTCWE Account, and to identify any malicious software or other activity that may harm BTCWE or its users.</p>
              <p className="paragraph-color">You may choose to provide us with access to certain personal information stored by third parties such as social media sites (such as Facebook and Twitter). The information we have access to varies by site and is controlled by your privacy settings on that site and your authorization. By associating an account managed by a third party with your BTCWE account and authorizing BTCWE to have access to this information, you agree that BTCWE may collect, store, and use this information in accordance with this Privacy Policy.</p>
              <p className="paragraph-color">Finally, we may collect additional information you may disclose to our customer support team.</p>

              <h4 className="question-color"><img alt="#" src={require("../../images/line.png")} /> HOW WE USE COOKIES</h4>
              <p className="paragraph-color">When you access our website or content or use our application or BTCWE Services, we or companies we work with may place small data files called cookies or pixel tags on your computer or other device. We use these technologies to:</p>
              <ul>
                <li>Recognize you as a BTCWE customer</li>
                <li>Customize BTCWE Services, content, and advertising</li>
                <li>Measure promotional effectiveness; and</li>
                <li>Collect information about your computer or other access device to mitigate risk, help prevent fraud, and promote trust and safety</li>
              </ul>
              <p className="paragraph-color">We use both session and persistent cookies when you access our website or content. Session cookies expire and no longer have any effect when you log out of your account or close your browser. Persistent cookies remain on your browser until you erase them or they expire. We also use Local Shared Objects, commonly referred to as "Flash cookies," to help ensure that your account security is not compromised, to spot irregularities in behavior to help prevent fraud, and to support our sites and services.</p>
              <p className="paragraph-color">We encode our cookies so that only we can interpret the information stored in them. You are free to decline our cookies if your browser or browser add-on permits, but doing so may interfere with your use of BTCWE Services. The help section of most browsers or browser add-ons provides instructions on blocking, deleting, or disabling cookies.</p>
              <p className="paragraph-color">You may encounter BTCWE cookies or pixel tags on websites that we do not control. For example, if you view a web page created by a third party or use an application developed by a third party, there may be a cookie or pixel tag placed by the web page or application. Likewise, these third parties may place cookies or pixel tags that are not subject to our control and the BTCWE Privacy Policy does not cover their use.</p>

              <h4 className="question-color"><img alt="#" src={require("../../images/line.png")} /> HOW WE PROTECT AND STORE PERSONAL INFORMATION</h4>
              <p className="paragraph-color">Throughout this policy, we use the term "personal information" to describe information that can be associated with a specific person and can be used to identify that person. This Privacy Policy does not apply to personal information that has been anonymized so that it does not and cannot be used to identify a specific user. BTCWE takes reasonable precautions, as described herein, to protect your personal information from loss, misuse, unauthorized access, disclosure, alteration, and destruction.</p>
              <p className="paragraph-color">We store and process your personal and transactional information, including certain payment information, where BTCWE facilities or our service providers are located, including in the Cayman Island, and we protect it by maintaining physical, electronic, and procedural safeguards in compliance with applicable laws. We use computer safeguards such as firewalls and data encryption, we enforce physical access controls to our buildings and files, and we authorize access to personal information only for those employees who require it to fulfill their job responsibilities. Full credit card data is securely transferred and hosted off-site by a payment vendor in compliance with Payment Card Industry Data Security Standards (PCI DSS). This information is not accessible to BTCWE staff.</p>
              <p className="paragraph-color">We store our customers' personal information securely throughout the life of the customer's BTCWE Account. BTCWE will retain your personal information for a minimum of five years or as necessary to comply with our legal obligations or to resolve disputes.</p>

              <h4 className="question-color"><img alt="#" src={require("../../images/line.png")} /> HOW WE USE THE PERSONAL INFORMATION WE COLLECT</h4>
              <p className="paragraph-color">Our primary purpose in collecting personal information is to provide you with a secure, smooth, efficient, and customized experience. We may use your personal information to:</p>
              <ul>
                <li>Provide BTCWE Services and customer support you request</li>
                <li>Process transactions and send notices about your transactions</li>
                <li>Resolve disputes, collect fees, and troubleshoot problems</li>
                <li>Prevent and investigate potentially prohibited or illegal activities, and/or violations of our posted user terms</li>
                <li>Customize, measure, and improve BTCWE Services and the content and layout of our website and applications</li>
                <li>Deliver targeted marketing, service update notices, and promotional offers based on your communication preferences; and</li>
                <li>Verify your identity by comparing your personal information against third-party databases.</li>
              </ul>
              <p className="paragraph-color">We will not use your personal information for purposes other than those purposes we have disclosed to you, without your permission. From time to time we may request your permission to allow us to share your personal information with third parties. You may opt out of having your personal information shared with third parties, or from allowing us to use your personal information for any purpose that is incompatible with the purposes for which we originally collected it or subsequently obtained your authorization. If you choose to so limit the use of your personal information, certain features or BTCWE Services may not be available to you.</p>

              <h4 className="question-color"><img alt="#" src={require("../../images/line.png")} /> HOW YOU CAN ACCESS OR CHANGE YOUR PERSONAL INFORMATION</h4>
              <p className="paragraph-color">You are entitled to review, correct, or amend your personal information, or to delete that information where it is inaccurate, and you may do so at any time by logging in to your account and clicking the Profile or My Account tab. This right shall only be limited where the burden or expense of providing access would be disproportionate to the risks to your privacy in the case in question, or where the rights of persons other than you would be violated. If you close your BTCWE account, we will mark your account in our database as "Closed," but will keep your account information in our database for a period of time described above. This is necessary in order to deter fraud, by ensuring that persons who try to commit fraud will not be able to avoid detection simply by closing their account and opening a new account. However, if you close your account, your personally identifiable information will not be used by us for any further purposes, nor sold or shared with third parties, except as necessary to prevent fraud and assist law enforcement, as required by law, or in accordance with this Privacy Policy.</p>

              <h4 className="question-color"><img alt="#" src={require("../../images/line.png")} /> HOW YOU CAN CONTACT US ABOUT PRIVACY QUESTIONS</h4>
              <p className="paragraph-color">If you have questions or concerns regarding this policy, or if you have a complaint, you can contact us at <a class="anchor-color" href="mailto:info@btcwe.io"><b>info@btcwe.io</b></a></p>

            </div>
          </div>
        </div>
      </>)
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.AuthReducer.isAuthenticated,
    accessToken: state.AuthReducer.accessToken,
  };
};

const mapDispatchToProps = (dispatch) => ({
  loginRequest: (data) => dispatch(loginRequest(data)),
  updateRedirectRequest: () => dispatch(updateRedirectRequest())
})

export default connect(mapStateToProps, mapDispatchToProps)(PrivacyPolicy)