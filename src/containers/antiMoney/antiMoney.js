import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { loginRequest, updateRedirectRequest, getCountry } from '../../actions/authAction';
import { Button, FormGroup, FormControl, FormLabel, Card } from "react-bootstrap";
import "../antiMoney/antiMoney.css";
import * as Scroll from 'react-scroll';
import ReactLoading from "react-loading";

let scroll     = Scroll.animateScroll;
class AntiMoney extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectState: false,
      email: "test3@gmail.com",
      username: "test",
      blockloader:true,
      };
  }
  componentWillMount(){
    getCountry().then(res => {
      if(res.props != undefined){
      if(res.props.to){
          this.props.history.push("/countryblocked");
          this.setState({blockloader:false});
      }
  }else{
    this.setState({blockloader:false});
  }
  })
  }
  componentDidMount(){
    scroll.scrollToTop({smooth: "linear",duration: 100});
  }
  render() {
    return (  
      <>  
      {(this.state.blockloader) &&
          <>
            <div className="blockloader">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
          </>}
       <div className="container">
         <div className="row margin-top-50 body-color">
           <div className="col-md-12">
              <div class="theme-title-antimoney">
                  <h2 class="text-center">BTCWE ANTI MONEY LAUNDERING ("AML") AND COUNTER TERRORIST FINANCING ("CTF") POLICY</h2>
              </div>
                <p className="paragraph-color">Money laundering is defined as the process where the identity of the proceeds of crime are so disguised that it gives the appearance of legitimate income. Criminals specifically target financial services firms through which they attempt to launder criminal proceeds without the firm's knowledge or suspicions.</p>

                <h4 className="question-color">Anti-Money Laundering (AML) Policy:</h4>
                <p className="paragraph-color">The BTCWE Policy is designed to prevent money laundering by meeting the UK AML legislation obligations including the need to have adequate systems and controls in place to mitigate the risk of the firm being used to facilitate financial crime. This AML Policy sets out the minimum standards which must be complied with and includes:</p>
                <ul>
                    <li>The appointment of a Money Laundering Reporting Officer (MLRO) who has sufficient level of seniority and independence and who has responsibility for oversight of compliance with relevant legislation, regulations, rules and industry guidance;</li>
                    <li>Establishing and maintaining a Risk Based Approach (RBA) towards assessing and managing the money laundering and terrorist financing risks to the company;</li>
                    <li>Establishing and maintaining risk-based customer due diligence, identification, verification and know your customer (KYC) procedures, including enhanced due diligence for those customers presenting higher risk, such as Politically Exposed Persons (PEPs);</li>
                    <li>Establishing and maintaining risk based systems and procedures to monitor on-going customer activity;</li>
                    <li>Procedures for reporting suspicious activity internally and to the relevant law enforcement authorities as appropriate;</li>
                    <li>The maintenance of appropriate records for the minimum prescribed periods;</li>
                    <li>Training and awareness for all relevant employees</li>
                </ul>

                <h4 className="question-color">Sanctions Policy:</h4>
                <p className="paragraph-color">BTCWE is prohibited from transacting with individuals, companies and countries that are on prescribed Sanctions lists. Bitstamp will therefore screen against United Nations, European Union, UK Treasury and US Office of Foreign Assets Control (OFAC) sanctions lists in all jurisdictions in which we operate.</p>
                
           </div>
        </div>
        </div>    
      </>)
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.AuthReducer.isAuthenticated,
    accessToken: state.AuthReducer.accessToken,
  };
};

const mapDispatchToProps = (dispatch) => ({
  loginRequest: (data) => dispatch(loginRequest(data)),
  updateRedirectRequest : () => dispatch(updateRedirectRequest())
})

export default connect(mapStateToProps, mapDispatchToProps)(AntiMoney)