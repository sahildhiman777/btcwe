import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
import { get_fav_assets, get_all_fav_assets, add_fav_assets, close_trades_by_username, close_trades, oneTrade } from '../../actions/authAction';
import { Modal, Form, Button } from "react-bootstrap";
import ReactLoading from "react-loading";
import { OverlayTrigger, Popover } from "react-bootstrap";
import "../addFavourite/addFavourite.css";
import * as Scroll from 'react-scroll';
import $ from 'jquery';

let scroll     = Scroll.animateScroll;
class AddFavourite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectState: false,
      show: 'show',
      div_id: '',
      assets: '',
      assetName: 'XRP/BTC',
      dropAssets: [],
      isLoading: true,
      showModal: false,
      selectValue: 1,
      tableContent: [],
      tableCloseContent: [],
      balance:'',
      localbalance:'',
      loader: false,
    };

    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  handleOpenModal() {
    this.setState({ showModal: true });
  }

  handleCloseModal() {
    this.setState({ showModal: false });
  }

  validateForm = () => {
    return this.state.email.length > 0 && this.state.username.length > 0;
  }

  handleChange(e) {
    this.setState({ selectValue: e.target.value });
  }
  modalSave = () => {
    this.setState({ loader: true, showModal: false });
    const { currentUser } = this.props;
    const selectedDrop = this.state.selectValue;
    let data = {
      'email': currentUser,
      'asset_id': selectedDrop
    }
    add_fav_assets(data).then(res =>{
      if(res.status == "success"){
        let data = { 'email': localStorage.getItem('currentUserId') }
      get_fav_assets(data).then(res => {
        if (res.length !== "") {
          let result = res.message[0];
          this.setState({ assets: res.message, div_id: 'div-id-' + result.id, isLoading: false, loader:false });
        }
      });
      }   
    });
  }
  contactus = (e) => {
    e.preventDefault();
    let data = {
      email: (this.state.email !== '') ? this.state.email : "test3@gmail.com",
      username: (this.state.username !== '') ? this.state.username : "test"
    }
  }
  componentDidMount(){
    $(document).ready(function(){
      $(".mobile_menu_icon").removeClass("hide_mobile");
    });
    scroll.scrollToTop({smooth: "linear",duration: 100});
  }
  componentWillMount() {
    $(document).ready(function(){
      $(".mobile_menu_icon").removeClass("hide_mobile");
    });
    let { currentUser } = this.props;
    let data = { 'email': localStorage.getItem('currentUserId') }
    get_fav_assets(data).then(res => {
      if (res.length !== "") {
        let result = res.message[0];
        this.setState({ assets: res.message, div_id: 'div-id-' + result.id, isLoading: false });
      }
    });
    get_all_fav_assets().then(res => {
      if (res.length !== "") {
        this.setState({ dropAssets: res.message, isLoading: false });
      }
    });

    close_trades_by_username(data).then(res => {      
      if (Array.isArray(res) && res.length !== 0) {
        this.setState({ tableCloseContent: res });
      }
    });    
  }

  gotopage(data, e) {
    e.preventDefault();
    e.stopPropagation();
    let asset = this.state.dropAssets.filter(function (da) {
      return da.id === data.id
    })
    if (asset.length) {
      switch (asset[0].type) {
        case 'crypto2crypto':
          this.props.history.push('/dashboard')
          localStorage.setItem('crypto-asset-page', JSON.stringify({ id: data.id, asset_name: data.asset, buy_rate: data.buy_rate, sell_rate: data.sell_rate, asseticon: data.base_icon }));
          break;
        case 'crypto2commodity':
          this.props.history.push('/commodities')
          localStorage.setItem('commodities-asset-page', JSON.stringify({ id: data.id, asset_name: data.asset, buy_rate: data.buy_rate, sell_rate: data.sell_rate, asseticon: data.base_icon }));
          break;
        case 'crypto2fiat':
          this.props.history.push('/fiat')
          localStorage.setItem('fiat-asset-page', JSON.stringify({ id: data.id, asset_name: data.asset, buy_rate: data.buy_rate, sell_rate: data.sell_rate, asseticon: data.base_icon }));
          break;
        case 'home_etf':
          this.props.history.push('/etf')
          localStorage.setItem('etf-asset-page', JSON.stringify({ id: data.id, asset_name: data.asset, buy_rate: data.buy_rate, sell_rate: data.sell_rate, asseticon: data.base_icon }));
          break;
        default:
          this.props.history.push('/dashboard')
          break;
      }
    }
  }


  render() {

    let new_item = this.state.assets;
    let drop_item = this.state.dropAssets;
    var result = drop_item.filter(function(o1){
      return !new_item.some(function(o2){
          return o1.id === o2.id; 
      });
  })
  let options = result.map((data) =>
  <option 
      key={data.id}
      value={data.id}
      label={data.asset}
  >
      {data.asset}
  </option>
);
  
  
    let table_close_item = this.state.tableCloseContent;
    let no_record, close_no_record;
    if (table_close_item === undefined || table_close_item.length == 0) {
      close_no_record = 'Record Not Found';
    } else {
      close_no_record = table_close_item;
      
    }
    return (
      <div>
        {(this.state.loader) ?
              <div className="loader">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px" , "top": "50%"}}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
            :
      <div className="kyc-section-cointainer margin-bottom-100 margin-top-60 addfav">
        <div className="container">
          <Helmet>
            <style>
              {" .nav-item .btn-signup{background-color:#3445CF !important;color:#fff !important; padding: 9px 43px !important;} .kyc-section-cointainer .container{ transform: skew(0deg, 5deg);}.kyc-section-cointainer { transform: skew(0deg, -5deg); background:#f9fafe; background-repeat: no-repeat; border-radius: 0px 104px 0px 104px;}"}</style>
          </Helmet>
          
          <div className="row margin-top-35 dashboard-section">
            <div className="col-md-12 padding-top-20 padding-bottom-20">
              <h2><i className="fa fa-plus" onClick={this.handleOpenModal}><span className="add-fav"> Add Favourite Assets </span></i></h2>

              <Modal show={this.state.showModal} onHide={this.handleCloseModal}>
                <Modal.Header closeButton>
                  <h3>Add Favourite Assets</h3>
                </Modal.Header>
                <Modal.Body>
                  <Form>
                    <Form.Group controlId="exampleForm.ControlSelect1">
                      {/* <Form.Label>Select Assets from List </Form.Label> */}
                      <select defaultValue={this.state.selectValue} className="form-control" onChange={(e) => this.handleChange(e)}>
                      <option>Select Assets</option>
                      {options}
                        {/* {(() => {
                          const dropoptions = [];
                          for (let i = 0; i < result.length; i++) {
                            dropoptions.push(
                              <option key={result[i].id} label={result[i].asset} value={result[i].id} >{result[i].asset}</option>
                            );
                          }

                          return dropoptions;
                        })()} */}
                      </select>
                    </Form.Group>
                    <Button variant="primary" type="button" onClick={this.modalSave} >
                      Save
                      </Button>
                  </Form>
                </Modal.Body>
              </Modal>
            </div>

          </div>
          {(this.state.isLoading) &&
            <>
              <div className="row margin-top-50 dashboard-section padding-top-10 text-center">
                <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px" }}>
                  <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                </div>
              </div>
            </>}
          {(() => {
            const options = [];

            for (let i = 0; i < new_item.length; i++) {
              let change = '';
              let asset = this.state.dropAssets.filter(function(da){
                return da.id === new_item[i].id;
              });
              if(asset[0] != undefined){
                change = asset[0]['24h_change']
              }
              options.push(
                <div className={"row margin-top-50 dashboard-section padding-top-10 text-center "} id={'div-id-' + new_item[i].id} key={new_item[i].id}>
                  <div className="width-215 text-left">
                    <div className="assets-heading" onClick={(e) => this.gotopage(new_item[i], e)}>
                      <div className="curncy_image">
                        <img alt="#" src={require(`../../images/icons/${new_item[i].base_icon}`)} />
                      </div>
                      <div className="asset-name-class"> {new_item[i].asset} </div>
                    </div>
                  </div>
                  <div className="width-1">
                    <p className="lg-change-text">Fair Value</p>
                  </div>
                  <div className="width-10 text-center">
                    <div className="point-value">
                      {new_item[i].spread < 0 ?
                        <b className="low">{new_item[i].spread.toFixed(7)} <img alt="#" src={require("../../images/Path.svg")} /></b>
                        :
                        <b className="heigh">{new_item[i].spread.toFixed(7)} <img alt="#" src={require("../../images/Path-1.svg")} /></b>
                      }
                    </div>
                  </div>
                  <div className="width-1">
                    <p className="lg-change-text">24h Change</p>
                  </div>
                  <div className="width-9 text-center">
                    <div className="status-balance">
                      {change < 0 ?
                        <b className="low">{change}%</b>
                        :
                        <b className="heigh">{change}%</b>
                      }
                    </div>
                  </div>
                  <div className="width-1">
                    <p className="lg-change-text">24h High</p>
                  </div>
                  <div className="width-11 text-center">
                    <div className="status">
                      <b>{new_item[i].max}</b>
                    </div>
                  </div>
                  <div className="width-1">
                    <p className="lg-change-text">24h Low</p>
                  </div>
                  <div className="width-9 text-center">
                    <div className="status">
                      <b>{new_item[i].min} </b>
                    </div>
                  </div>
                  <div className="width-12 text-center">
                    <div className="asset-info">
                      <OverlayTrigger trigger="click" placement="bottom" rootClose={true} overlay={
                        <Popover id={"popover-basic" + new_item[i].id}>
                          <Popover.Title as="h3">Assets Info</Popover.Title>
                          <Popover.Content>
                            {new_item[i].base_info}
                          </Popover.Content>
                        </Popover>
                      }>
                        <b><img alt="#" src={require("../../images/Vector.svg")} /> <span>Asset Info</span></b>
                      </OverlayTrigger>
                    </div>
                  </div>
                </div>
              );
            }

            return options;
          })()}
          <div className="clearfix"></div>

          <div className="clearfix"></div>
        </div>
      </div>
  }
      </div>
      )
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.AuthReducer.isAuthenticated,
    accessToken: state.AuthReducer.accessToken,
    isAuthenticated: state.AuthReducer.isAuthenticated,
    currentUser: state.AuthReducer.currentUser,
  };
};

const mapDispatchToProps = (dispatch) => ({
  get_fav_assets: (data) => dispatch(get_fav_assets(data)),
  add_fav_assets: (data) => dispatch(add_fav_assets(data)),
  get_all_fav_assets: () => dispatch(get_all_fav_assets()),
  close_trades: (data) => dispatch(close_trades(data)),
  close_trades_by_username: (data) => dispatch(close_trades_by_username(data)),
  oneTrade: (trade) => dispatch(oneTrade(trade))
})

export default connect(mapStateToProps, mapDispatchToProps)(AddFavourite)