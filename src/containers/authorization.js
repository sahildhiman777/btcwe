
const authorizationView = (currentUser, access_table, access_right) => {
    return true;
}

const authorizationUpdate = (currentUser, access_table, access_right) => {
    let isAllowed = false;
    let hasAccess = false;
    let objTables = JSON.parse(currentUser[0].Role.rights);
    isAllowed = Object.keys(objTables).some(role => access_table.includes(role));
    if (isAllowed) {
        hasAccess = objTables[access_table].some(role => access_right.includes(role));
    }
    if (isAllowed && hasAccess) {
        return true;
    }
    return false;
}

const authorizationAdd = (currentUser, access_table, access_right) => {
    let isAllowed = false;
    let hasAccess = false;
    let objTables = JSON.parse(currentUser[0].Role.rights);
    isAllowed = Object.keys(objTables).some(role => access_table.includes(role));
    if (isAllowed) {
        hasAccess = objTables[access_table].some(role => access_right.includes(role));
    }
    if (isAllowed && hasAccess) {
        return true;
    }
    return false;
}

const authorizationDelete = (currentUser, access_table, access_right) => {
    let isAllowed = false;
    let hasAccess = false;
    let objTables = JSON.parse(currentUser[0].Role.rights);
    isAllowed = Object.keys(objTables).some(role => access_table.includes(role));
    if (isAllowed) {
        hasAccess = objTables[access_table].some(role => access_right.includes(role));
    }
    if (isAllowed && hasAccess) {
        return true;
    }
    return false;
}

const auth = (currentUser) => {
    return true;
}

module.exports = {
    authorizationView,
    authorizationDelete,
    authorizationUpdate,
    authorizationAdd,
    auth
};