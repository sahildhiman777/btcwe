import React, { Component } from 'react'
import { connect } from 'react-redux'
import '../account/account.css';
import { authorizationView } from '../authorization';
import {
} from 'react-bootstrap';
import * as Scroll from 'react-scroll';
let scroll = Scroll.animateScroll;
class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false
    };

    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }
  componentDidMount() {
    scroll.scrollToTop({ smooth: "linear", duration: 100 });
  }

  handleOpenModal() {
    this.setState({ showModal: true });
  }

  handleCloseModal() {
    this.setState({ showModal: false });
  }

  authorizationCheck = (pathname, viewRight) => {
    let { currentUser } = this.props;
    let listCheck = authorizationView(currentUser, pathname, viewRight);
    return listCheck;
  }
  render() {
    return (
      <div>
        <div className="container">
          <div className="row margin-top-100 account-section">
            <div className="col-md-8 offset-md-2">
              <div className="row padding-top-50">
                <div className="col-md-offset-1 col-md-6 col-md-offset-1" id="account_first_col">
                  <a href="/compliance">
                    <div className="caption">
                      Compliance
                                </div>
                  </a>
                </div>
                <div className="col-md-offset-1 col-md-6 col-md-offset-1" id="account_first_col">
                  <a href="/finance">
                    <div className="caption">
                      Finance
                                </div>
                  </a>
                </div>
                <div className="col-md-offset-1 col-md-6 col-md-offset-1" id="account_first_col">
                  <a href="/compliance">
                    <div className="caption">
                      Security
                                </div>
                  </a>
                </div>
                <div className="col-md-offset-1 col-md-6 col-md-offset-1" id="account_first_col">
                  <a href="/compliance">
                    <div className="caption">
                      Deposit
                                </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.AuthReducer.isLoading,
    accessToken: state.AuthReducer.accessToken,
    isAuthenticated: state.AuthReducer.isAuthenticated,
    currentUser: state.AuthReducer.currentUser,
    isbtcRate: state.AuthReducer.isbtcRate,
    isTab: state.AuthReducer.isTab,
  }
}

export default connect(mapStateToProps, {}
)(Account)
