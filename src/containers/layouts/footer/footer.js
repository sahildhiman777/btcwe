import React, { Component } from 'react'
import "./footer.css";
import {
	Link
} from "react-router-dom";
import { connect } from 'react-redux'
class Footer extends Component {

	render() {
		let { isAuthenticated } = this.props;
		return (
			<div>
				<div className="footer margin-top-30">
					<div className="container">
						<div className="row">
							{(!isAuthenticated) &&
								<>
									<div className="col-lg-2 col-md-12">
										<a className="navbar-brand" href="/"><img alt="#" src={require("../../../images/Logo.png")} /></a>
									</div>
								</>}
							{(isAuthenticated) &&
								<>
									<div className="col-lg-2 col-md-12">
										<a className="navbar-brand" href="/dashboard"><img alt="#" src={require("../../../images/Logo.png")} /></a>
									</div>
								</>}
							<div className="col-lg-8 col-md-12">
								<ul className="footer_menu" style={{ textAlign: "center" }}>
									<li className="nav-item">
										<Link className="nav-link" to="/withdrawal">Withdrawal</Link>
									</li>
									<li className="nav-item">
										<Link className="nav-link" to="/kyc-policy">KYC Policy</Link>
									</li>
									<li className="nav-item">
										<Link className="nav-link" to="/anti-money">Anti-Money Laundering</Link>
									</li>
									<li className="nav-item">
										<Link className="nav-link" to="/privacy-policy">Privacy Policy</Link>
									</li>
								</ul>
							</div>
							<div className="col-lg-2 col-md-12">
								<ul className="social-link" style={{ textAlign: "center" }}>
									<li className="nav-item">
										<a className="nav-link" href="https://twitter.com/" target="_blank"><i className="fa fa-twitter" aria-hidden="true"></i></a>
									</li>
									<li className="nav-item">
										<a className="nav-link" href="https://www.linkedin.com/" target="_blank"><i className="fa fa-linkedin" aria-hidden="true"></i></a>
									</li>
									<li className="nav-item">
										<a className="nav-link" href="https://www.facebook.com/" target="_blank"><i className="fa fa-facebook" aria-hidden="true"></i></a>
									</li>
								</ul>
							</div>
						</div>
						<hr />
						<div className="address-footer col-md-10 offset-md-1 text-center">
							<p>SMART TRADE AND INVESTMENT LTD</p>
							<p>Company number 1201947454 Downage, London, England, NW4 1AH</p>
						</div>
						<div className="row risk-warning-section">
							<div className="col-md-12">
								<p><strong>Risk Warning:</strong> Trading involve high level of risk and may lead to loss of all invested capital. Do not invest capital that you cannot afford to lose. The Company provides general information that does not take into account your objectives, financial situation or needs. The content of this website must not be interpreted as personal investment advice. Please ensure that you understand the risks involved and seek independent advice if necessary.
								</p>
							</div>
						</div>
						<div className="row card-logo-section">
							<div className="col-md-12 text-center">
								<img alt="vload" src={require("../../../images/vload.png")} />
								<img alt="secure-mastercard" src={require("../../../images/secure-mastercard.png")} />
								<img alt="maestro" src={require("../../../images/maestro.png")} />
								<img alt="v-pay" src={require("../../../images/v-pay.png")} />
								<img alt="visa" src={require("../../../images/visa.png")} />
								<img alt="mastercard" src={require("../../../images/mastercard.png")} />
								<img alt="visa-logo" src={require("../../../images/visa-logo.png")} />
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}

}
const mapStateToProps = (state) => {
	return {
		isLoading: state.AuthReducer.isLoading,
		isAuthenticated: state.AuthReducer.isAuthenticated,
		currentUser: state.AuthReducer.currentUser

	}
}
export default connect(mapStateToProps, {})(Footer)