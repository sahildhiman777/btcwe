import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import "./index.css";
export class OurPrincipals extends Component {
    constructor() {
        super();
        this.state = { showText: false };
    }
    render() {
        return (
            <div className="our-principals">
                <div className="our-principals-section" id="our_principals">
                    <div className="container padding-top-15 padding-bottom-100">
                        <div className="row principals-text padding-top-50">
                            <div className="col-md-10 text-center offset-md-1">
                                <h2><img alt="#" src={require("../../../../images/line.png")} /> Our principals</h2>
                            </div>
                        </div>
                        <div className="row hide_on_mobile">
                            <div className="col-md-6 text-center padding-top-105 order-class">
                                <img alt="#" className="img-fluid asset-img" src={require("../../../../images/Asset1.png")} />
                            </div>
                            <div className="col-md-6 padding-top-105 order-class">
                                <div className="principal-sections">
                                    <h4>Education</h4>
                                    <p>Before studying several "universal" technical indicators as soon as possible and choosing a popular crypto-exchange
(it is possible with a trollbox, but it is better without it), you should learn some key trading principles once and for all.
 It is better to store the bulk of cryptocurrency savings not on a centralized exchange, but in a reliable wallet.
These funds can be considered as long-term investments.</p>
                                </div>
                            </div>
                            <div className="col-md-6 padding-top-105 order-class">
                                <div className="principal-sections">
                                    <h4>Benefits</h4>
                                    <p>At the same time," frozen " assets on the crypto - wallet can mean for a trader a missed short-or medium-term benefit,
                                        as they are not involved in the turnover. Thus,
                                        some of these funds can still be brought to several crypto-currency exchanges.</p>
                                </div>
                            </div>
                            <div className="col-md-6 padding-top-105 text-center order-class">
                                <img alt="#" className="img-fluid asset-img" src={require("../../../../images/Asset2.png")} />
                            </div>
                            <div className="col-md-6 padding-top-105 text-center order-class">
                                <img alt="#" className="img-fluid asset-img" src={require("../../../../images/Asset3.png")} />
                            </div>
                            <div className="col-md-6 padding-top-105 order-class">
                                <div className="principal-sections">
                                    <h4>Smart Investment</h4>
                                    <p>It is much better and safer from time to time to Deposit some of the available funds.
 For example, calculate your personal budget so that,
 say, 5-10% of your monthly income would be spent on the purchase of cryptocurrency topping up the Deposit on the crypto exchange.
 Over time, it will also be possible to increase the Deposit through the gradual recapitalization of profits. </p>
                                </div>
                            </div>
                        </div>

                        {/* Mobile View */}
                        <div className="row hide_on_desktop">
                            <div className="col-md-12 col-sm-12 text-center tab-float_l">
                                <img alt="#" className="img-fluid principal-img" src={require("../../../../images/Asset1.png")} />
                            </div>
                            <div className="col-md-12 col-sm-12 tab-float_l">
                                <div className="principal-sections">
                                    <h4>Education</h4>
                                    <p>Before studying several "universal" technical indicators as soon as possible and choosing a popular crypto-exchange
(it is possible with a trollbox, but it is better without it), you should learn some key trading principles once and for all.
 It is better to store the bulk of cryptocurrency savings not on a centralized exchange, but in a reliable wallet.
These funds can be considered as long-term investments.</p>
                                </div>
                            </div>
                        </div>
                        <div className="row hide_on_desktop">
                            <div className="col-md-12 col-sm-12 text-center tab-float_l">
                                <img alt="#" className="img-fluid principal-img" src={require("../../../../images/Asset2.png")} />
                            </div>
                            <div className="col-md-12 col-sm-12 tab-float_l">
                                <div className="principal-sections">
                                    <h4>Benefits</h4>
                                    <p>At the same time," frozen " assets on the crypto - wallet can mean for a trader a missed short-or medium-term benefit,
                                        as they are not involved in the turnover. Thus,
                                        some of these funds can still be brought to several crypto-currency exchanges.</p>
                                </div>
                            </div>

                        </div>
                        <div className="row hide_on_desktop">
                            <div className="col-md-12 col-sm-12 text-center tab-float_l">
                                <img alt="#" className="img-fluid principal-img" src={require("../../../../images/Asset3.png")} />
                            </div>
                            <div className="col-md-12 col-sm-12 tab-float_l">
                                <div className="principal-sections">
                                    <h4>Smart Investment</h4>
                                    <p>It is much better and safer from time to time to Deposit some of the available funds.
 For example, calculate your personal budget so that,
 say, 5-10% of your monthly income would be spent on the purchase of cryptocurrency topping up the Deposit on the crypto exchange.
 Over time, it will also be possible to increase the Deposit through the gradual recapitalization of profits. </p>
                                </div>
                            </div>
                        </div>
                        {/* Mobile view end  */}
                    </div>
                </div>
                <div className="container">
                    <div className="row asset-section">
                        <div className=" col-lg-6 col-md-12 text-center asset-order">
                            <img alt="#" className="img-fluid margin-left-n65" src={require("../../../../images/who-we-are.png")} />
                        </div>
                        <div id="who_we_are" className="col-lg-6 col-md-12 asset-order">
                            <div className="asset-sections">
                                <span ><img alt="#" className="margin-right-15" src={require("../../../../images/line.png")} /> Who we are</span>
                                <h4 className="padding-top-25">BTC.World Exchange is a global cryptocurrency exchange platform</h4>
                                <p>BTC.World Exchange is a global cryptocurrency exchange that provides a platform for trading more than 100 cryptocurrencies.
 Since early 2018, BTC.World Exchange is considered as the biggest cryptocurrency exchange in the world in terms of trading volume.</p>
                                <div className="asset-comment">
                                    <div className="img-clear">
                                        <img alt="#" src={require("../../../../images/Ellipse.svg")} />
                                    </div>
                                    <h5>William Doe</h5>
                                    <p>Co-Founder & CEO</p>
                                </div>
                            </div>
                        </div>
                        <div id="how_it_work" className="col-lg-6 col-md-12 asset-order how_do_work">
                            <div className="asset-sections">
                                <span><img alt="#" className="margin-right-15" src={require("../../../../images/line.png")} /> How do we work</span>
                                <h4 className="padding-top-25">BTC.World Exchange  earned the number Most Influential list – BTC.</h4>
                                <p>If you are interested in buying or selling a cryptocurrency,
 there are few better places to look than one of the world's largest cryptocurrency exchange by trade volume,
 BTC.World Exchange.</p>
                                <p> In fact, BTC.World Exchange became so popular, so quickly it became worth more than $1 billion in less than one year,
officially making it one of the first companies to achieve "unicorn" status in the cryptocurrency industry.</p>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-12 text-center asset-order how_do_work">
                            <img alt="#" className="img-fluid how_do_img" src={require("../../../../images/img.png")} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    buyBTC: state.buyBTC,
    sellBTC: state.AuthReducer.sellBTC,
    userInfo: state.AuthReducer.userInfo,
    currentUser: state.AuthReducer.currentUser,
})

export default connect(mapStateToProps)(OurPrincipals);