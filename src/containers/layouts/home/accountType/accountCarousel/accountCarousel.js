import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "./accountcarousel.css";
import "slick-carousel/slick/slick-theme.css";
class AccountCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
    }
  }
  componentDidMount() {
    const search = window.location.search;
    this.setState({ search: `/register${search}` })
  }

  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500, slidesToShow: 4,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 4,
            slidesToScroll: 4,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 812,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 736,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 667,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 640,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }]
    };
    return (
      <Slider {...settings}>
        <div>
          <div className="tab-body text-center">
            <h6>Basic education</h6>
            <p className="tab-text">€ <span className="tab-heading">49</span><span className="tab-sm">/mo</span></p>
            <ul className="text-left">
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Up to 20% bonus</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >1 risk free trade</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Basic education</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Minimal trading volume 0.01</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Leverage up to 1:100</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >24/7 customer service</span></li>
            </ul>
            <Link to={this.state.search} className="tab-learn-more">Join The Exchange</Link>
          </div>
        </div>
        <div>
          <div className="tab-body text-center">
            <h6>Silver account</h6>
            <p className="tab-text">€ <span className="tab-heading">99</span><span className="tab-sm">/mo</span></p>
            <ul className="text-left">
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Up to 30% bonus</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >5 risk free trade</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Full education</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Minimal trading volume 0.01</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Leverage up to 1:100</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Personal Expert</span></li>
            </ul>
            <Link to={this.state.search} className="tab-learn-more">Join The Exchange</Link>
          </div>
        </div>
        <div>
          <div className="tab-body text-center">
            <h6>Gold account</h6>
            <p className="tab-text">€ <span className="tab-heading">249</span><span className="tab-sm">/mo</span></p>
            <ul className="text-left">
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Up to 50% bonus</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >3 day risk free trading</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Full education</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Minimal trading volume 0.01</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Leverage up to 1:100</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Personal Expert</span></li>
            </ul>
            <Link to={this.state.search} className="tab-learn-more">Join The Exchange</Link>
          </div>
        </div>
        <div>
          <div className="tab-body text-center">
            <h6>Platinum account</h6>
            <p className="tab-text">€ <span className="tab-heading">549</span><span className="tab-sm">/mo</span></p>
            <ul className="text-left">
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Special bonuses</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >15 days risk free trading</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Full education</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Live Training Session</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >Leverage up to 1:100</span></li>
              <li><i className="fa fa-check" aria-hidden="true"></i><span className="check-text" >VIP Expert</span></li>
            </ul>
            <Link to={this.state.search} className="tab-learn-more">Join The Exchange</Link>
          </div>
        </div>
      </Slider>
    );
  }
}
export default AccountCarousel;