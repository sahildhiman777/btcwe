import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Tab, Col, Nav } from "react-bootstrap";
import AccountCarousel from "./accountCarousel/accountCarousel";
export class AccountType extends Component {

    render() {
        return (
            <div id="account_types" className="account-type padding-top-50">
                <div className="container">
                    <div className="row account-text">
                        <div className="col-md-8 text-center offset-md-2">
                            <h2><img alt="#" className="" src={require("../../../../images/line.png")} /> Account types</h2>
                            <p>Sign up in less than 30 seconds.
     Try out our 7 day risk free trial,
 upgrade at anytime</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="container padding-top-10">
                            <Tab.Container id="tabs" defaultActiveKey="monthly">
                                <Col md={12} Col sm={12} className="text-center">
                                    <Nav variant="tabs" className="nav-fill">
                                        <Nav.Link eventKey="monthly">Monthly</Nav.Link>
                                        <Nav.Link eventKey="annually">Annually</Nav.Link>
                                    </Nav>
                                </Col>
                                <Col md={12} Col sm={12} className="padding-0" id="nav-tabContent">
                                    <Tab.Content className="padding-top-50">
                                        <Tab.Pane eventKey="monthly" >
                                            <div>
                                                <AccountCarousel />
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane eventKey="annually">
                                            <div>
                                                Annually...
                                            </div>
                                        </Tab.Pane>
                                    </Tab.Content>
                                </Col>
                            </Tab.Container>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    buyBTC: state.buyBTC,
    sellBTC: state.AuthReducer.sellBTC,
    userInfo: state.AuthReducer.userInfo,
    currentUser: state.AuthReducer.currentUser,
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(AccountType)
// export default Home;
