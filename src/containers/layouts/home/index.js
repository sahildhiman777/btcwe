import React, { Component } from 'react';
import './index.css';
import {
    Link
} from "react-router-dom";
import AccountType from "./accountType/accounttype";
import OurPrincipals from "./ourPrincipals/ourPrincipals";
import { getFiatAssets, createMarket, getRemoteBtcRate, add_fav_assets } from '../../../actions/authAction';
import {
    Redirect
} from "react-router-dom";
import { connect } from 'react-redux';
import $ from 'jquery';
import history from '../../../history';
import ReactLoading from "react-loading";
export class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            assets: '',
            blockloader: true,
        }
    }
    componentWillMount() {
        $(document).ready(function () {
            $(".mobile_menu_icon").removeClass("hide_mobile");
            $(".header2").removeClass("hide-blocked-page");
            $(".header2_mobile").removeClass("hide-blocked-page");
            $(".footer").removeClass("hide-blocked-page");
        });
        // getRemoteBtcRate().then(res => {

        // });
        getFiatAssets().then(res => {
            if (res.props != undefined) {
                if (res.props.to) {
                    this.props.history.push("/countryblocked");
                    this.setState({ blockloader: false });
                }
            } else {
                this.setState({ blockloader: false });
            }
            this.setState({ assets: res.rate });
        });
    }

    render() {
        let new_item = [];
        if (this.state.assets) {
            new_item = this.state.assets;
        }
        let { currentUser } = this.props;
        if (currentUser !== "") {
            return (<Redirect to='/dashboard' />);
        }
        return (
            <div className="home">
                {(this.state.blockloader) &&
                    <>
                        <div className="blockloader">
                            <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                            </div>
                        </div>
                    </>}

                <div className="latest-trends padding-top-100">
                    <div className="container">
                        <div className="row latest-text">
                            <div className="col-md-12 text-center">
                                <h2><img alt="#" className="" src={require("../../../images/line.png")} /> Latest Market Movements</h2>
                            </div>
                        </div>
                        <div className="row latest-trends-img">
                            <div className="bg-table-img">
                                <table className="table table-striped table-responsive">
                                    <tbody>
                                        <tr>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Change (24h)</th>
                                        </tr>
                                        {(() => {
                                            const options = [];
                                            for (let i = 0; i < new_item.length; i++) {
                                                let eur = new_item[i].asset.substring(4, 7);
                                                if (eur === 'EUR') {
                                                    options.push(
                                                        <tr>
                                                            <td><img alt="#" src={require(`../../../images/icons/${new_item[i].base_icon}`)} /> {new_item[i].asset.substring(0, 3)}</td>
                                                            <td>€{new_item[i].market_rate}</td>
                                                            {new_item[i]['24h_change'] < 0 ?
                                                                <td className="latest-danger low">{new_item[i]['24h_change']}%</td>
                                                                :
                                                                <td className="latest-danger heigh">{new_item[i]['24h_change']}%</td>
                                                            }
                                                        </tr>);
                                                }

                                            }

                                            return options;
                                        })()}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div className="text-center padding-top-50">
                            <Link to="/login" className="btn btn-latest-trends" role="button"> Go to Dashboard</Link>
                        </div>
                    </div>
                </div>
                <OurPrincipals />
                <AccountType />
                <div id="contact_us" className="question margin-top-100">
                    <div className="contact_us_bg">
                        <div className="container">
                            <div className="row question-text padding-top-100">
                                <div className="col-md-12 text-center">
                                    <h2><img alt="#" src={require("../../../images/white_line.png")} /> Have a question?</h2>
                                    <p style={{ height: '50px' }}></p>
                                    <div>
                                        <Link to="/contact-us" className="contact_us">Contact US</Link></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    buyBTC: state.buyBTC,
    sellBTC: state.AuthReducer.sellBTC,
    userInfo: state.AuthReducer.userInfo,
    currentUser: state.AuthReducer.currentUser,
})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(Home)
// export default Home;
