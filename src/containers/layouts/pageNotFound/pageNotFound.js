import React, { Component } from 'react';
import { getCountry } from '../../../actions/authAction';
import ReactLoading from "react-loading";

export default class PageNotFound extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blockloader:true,
    }
  }

  componentWillMount(){
    getCountry().then(res => {
      if(res.props != undefined){
      if(res.props.to){
          this.props.history.push("/countryblocked");
          this.setState({blockloader:false});
      }
  }else{
    this.setState({blockloader:false});
  }
  })
  }
  render() {
    return (
      <div className="container">
        {(this.state.blockloader) &&
          <>
            <div className="blockloader">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
          </>}
      <div className="page-container">
        <div className="page-sub-container">
         <span className="page-text">Page Not Found</span>
         </div>
         </div>
      </div>
    )
  }
}