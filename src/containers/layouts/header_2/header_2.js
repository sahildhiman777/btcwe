import React, { Component } from 'react'
import {	Link } from "react-router-dom";
import { Navbar, NavDropdown } from 'react-bootstrap';
import history from '../../../history';
import "../header_2/header_2.css";
import { connect } from 'react-redux'
import { logoutFun } from '../../../actions/authAction';
import { authorizationView } from '../../authorization';
import cancelicon from '../../../images/cancel.svg';
import cancelicon2 from '../../../images/cancel2.svg';
import menuicon from '../../../images/menu.svg';
import menuicon2 from '../../../images/menu2.svg';
import callApi from '../../../utils/apiCaller';
class Header2 extends Component {
	constructor(props) {
		super(props);
		this.state = {
			active: 'crypto',
			visible: '',
			mobile_container: false,
			loginurl: '',
			balance: '0',
			all_balance: []
		}
		this.getAllBalance = this.getAllBalance.bind(this);
	}
	getAllBalance() {
		callApi('user/get_all_balance?email=' + localStorage.getItem('currentUserId'), 'GET')
			.then(res => {
				if(res.props != undefined){

					if(res.props.to){
						return <redirect to ='/countryblocked' />
				}
				}
				localStorage.setItem('all_balance', JSON.stringify(res.message));
				this.setState({ all_balance: res.message })
			});
	}
	stateActive(name) {
		this.setState({ active: name, mobile_container: !this.state.mobile_container });
		if (name == 'crypto') {
			localStorage.setItem('asset_type', 'crypto2crypto')
		}
		if (name == 'fiat') {
			localStorage.setItem('asset_type', 'crypto2fiat')
		}
		if (name == 'commodities') {
			localStorage.setItem('asset_type', 'crypto2commodity')
		}
		if (name == 'etf') {
			localStorage.setItem('asset_type', 'home_etf')
		}
	}
	logout = () => {
		this.props.logoutFun();
		this.setState({ mobile_container: !this.state.mobile_container });
	}

	authorizationCheck = (pathname, viewRight) => {
		let { currentUser } = this.props;
		let listCheck = authorizationView(currentUser, pathname, viewRight);
		return listCheck;
	}
	componentDidUpdate() {
		if(this.state.all_balance){
			if (this.state.all_balance.length == 0 || this.state.all_balance.length == 30) {
				let currentUser = '';
				currentUser = localStorage.getItem('currentUserId')
				if (currentUser) {
					this.getAllBalance();
				}
			}
		}
	}
	componentWillMount() {
		let currentUser = localStorage.getItem('currentUserId');
		console.log(currentUser,'user');
		
		if (currentUser) {
			this.getAllBalance();
		this.interval = setInterval(() => {
			this.getAllBalance();
		}, 60000);

		setTimeout(() => {
			this.getAllBalance()
		}, 300);
	}
	}
	componentDidMount() {

		let urlPaths = history.location.pathname;
		if (urlPaths === '/login') {
			this.setState({ loginurl: urlPaths });
		} else {
			this.setState({ loginurl: urlPaths });
		}

		if (urlPaths === '/dashboard') {
			this.setState({ active: 'crypto' });
		} else if (urlPaths === '/etf') {
			this.setState({ active: 'etf' });
		} else if (urlPaths === '/fiat') {
			this.setState({ active: 'fiat' });
		} else if (urlPaths === '/commodities') {
			this.setState({ active: 'commodities' });
		} else if (urlPaths === '/compliance') {
			this.setState({ active: 'compliance' });
		} else if (urlPaths === '/profile') {
			this.setState({ active: 'profile' });
		} else if (urlPaths === '/portfolio') {
			this.setState({ active: 'portfolio' });
		} else if (urlPaths === '/deposit') {
			this.setState({ active: 'deposit' });
		} else if (urlPaths === '/withdraw-request') {
			this.setState({ active: 'withdraw-request' });
		} else if (urlPaths === '/my-cashier') {
			this.setState({ active: 'my-cashier' });
		} else if (urlPaths === '/') {
			this.setState({ visible: '' });
		}
	}
	componentWillUnmount() {
		clearInterval(this.interval);
	}

	register = () => {
		this.setState({ visible: 'hidden' });
	}

	home = () => {
		this.setState({ visible: 'hidden' });
	}

	onMenuClick = () => {
		this.setState({ mobile_container: !this.state.mobile_container });
	}

	onItemClick = () => {
		this.setState({ mobile_container: !this.state.mobile_container });
	}

	render() {
		let balance = this.state.balance;
		let all_balance = this.state.all_balance;
		let { isAuthenticated, currentUser, name } = this.props;
		return (
			<div>
				<div className="header2">

					<div className="container">
						<Navbar expand="lg">
							{(isAuthenticated) ?
								<Navbar.Brand>
									<a className="navbar-brand" href="/dashboard">
										<img alt="#" src={require("../../../images/Logo.png")} />
									</a>
								</Navbar.Brand>
								:
								<Navbar.Brand>
									<a className="navbar-brand" href="/" >
										<img alt="#" src={require("../../../images/Logo.png")} />
									</a>
								</Navbar.Brand>

							}
							<Navbar.Toggle aria-controls="basic-navbar-nav" className="order-1" />
							<Navbar.Collapse id="basic-navbar-nav">
								<ul className="navbar-nav ml-auto" style={{ textAlign: "right" }}>
									{(!isAuthenticated) &&
										<>
											<li className="nav-item">
												<a className="nav-link" href="/#who_we_are">Who we are</a>
											</li>
											<li className="nav-item">
												<a className="nav-link" href="/#how_it_work">How it works</a>
											</li>
											<li className="nav-item">
												<a className="nav-link" href="/#our_principals">Our principles</a>
											</li>
											<li className="nav-item">
												<a className="nav-link" href="/#account_types">Account types</a>
											</li>
											<li className="nav-item">
												<a className="nav-link" href="/#contact_us">Contact us</a>
											</li>
											<li className="nav-item">
												<a className="btn btn-signup" role="button" href="/login">Login</a>
											</li>
											<li className="nav-item">
												<a className="btn btn-signup" role="button" href="/register">Register</a>
											</li>
										</>
									}
									{(isAuthenticated) &&
										<>
											<li className="nav-item">
												<Link className={"nav-link " + ((this.state.active === 'crypto') ? 'active' : '')} onClick={() => this.stateActive('crypto')} to="/dashboard">Crypto Assets</Link>
											</li>
											<li className="nav-item">
												<Link className={"nav-link " + ((this.state.active === 'fiat') ? 'active' : '')} onClick={() => this.stateActive('fiat')} to="/fiat">Fiat Assets </Link>
											</li>
											<li className="nav-item">
												<Link className={"nav-link " + ((this.state.active === 'commodities') ? 'active' : '')} onClick={() => this.stateActive('commodities')} to="/commodities">Commodities Assets </Link>
											</li>
											<li className="nav-item">
												<Link className={"nav-link " + ((this.state.active === 'etf') ? 'active' : '')} onClick={() => this.stateActive('etf')} to="/etf">ETF Assets </Link>
											</li>
										</>}
								</ul>
							</Navbar.Collapse>
							{(isAuthenticated) &&
								<>
									<Navbar.Collapse id="basic-navbar-nav ">
										<ul className="navbar-nav ml_15px" style={{ textAlign: "right" }}>
											<li className="nav-item">
												<Link className="btn btn-deposit" to="/deposit"> Deposit  </Link>
											</li>
											<li className="nav-item">
												{(isAuthenticated) &&
													<>
														<NavDropdown alignRight title={<div className="pull-left">
															<img src={require("../../../images/user.svg")} width="31px" alt="#" /> {name}
														</div>}
															id="dropdown-menu-align-right" >
															<NavDropdown.Item as={Link} to="/profile" className={((this.state.active === 'profile') ? 'active' : '')} onClick={() => this.stateActive('profile')}>Profile</NavDropdown.Item>

															<NavDropdown.Item as={Link} to="/portfolio" className={((this.state.active === 'portfolio') ? 'active' : '')} onClick={() => this.stateActive('portfolio')}>Portfolio</NavDropdown.Item>

															<NavDropdown.Item as={Link} to="/tradinghistory" className={((this.state.active === 'tradinghistory') ? 'active' : '')} onClick={() => this.stateActive('tradinghistory')}>Trading History</NavDropdown.Item>

															<NavDropdown.Item as={Link} to="/my-cashier" className={((this.state.active === 'my-cashier') ? 'active' : '')} onClick={() => this.stateActive('my-cashier')}>My Cashier</NavDropdown.Item>

															<NavDropdown.Item as={Link} to="/compliance" className={((this.state.active === 'compliance') ? 'active' : '')} onClick={() => this.stateActive('compliance')}>Compliance</NavDropdown.Item>

															<NavDropdown.Item onClick={(e) => this.logout(e)}>Logout</NavDropdown.Item>
														</NavDropdown>
													</>
												}
											</li>
										</ul>
									</Navbar.Collapse></>}
						</Navbar>
					</div>

				</div>
				{/* MOBILE VIEW START  */}
				<div className="header2_mobile">
					<div className="container">
						<Navbar expand="lg">

							<Navbar.Brand>
								{(!isAuthenticated) &&
									<>
										<a className="navbar-brand" href="/" onClick={() => this.home()}><img alt="#" src={require("../../../images/Logo.png")} /></a>
									</>}
								{(isAuthenticated) &&
									<>
										<a className="navbar-brand" href="/dashboard"><img alt="#" src={require("../../../images/Logo.png")} /></a>
									</>}
							</Navbar.Brand>
							{this.state.mobile_container ?
								this.state.loginurl === '/login' ?
									<div className="cross" onClick={this.onItemClick} style={{ backgroundImage: `url(${cancelicon2})` }}></div>
									:
									<div className="cross" onClick={this.onItemClick} style={{ backgroundImage: `url(${cancelicon})` }}>
									</div>
								:
								this.state.loginurl === '/login' ?
									<div className="mobile_menu_icon" aria-controls="basic-navbar-nav" onClick={this.onItemClick} style={{ backgroundImage: `url(${menuicon2})` }}>
									</div>
									:
									<div className="mobile_menu_icon" aria-controls="basic-navbar-nav" onClick={this.onItemClick} style={{ backgroundImage: `url(${menuicon})` }}>
									</div>
							}
							{(this.state.mobile_container) &&
								<>
									<ul className="navbar-nav menu">
										{(!isAuthenticated) &&
											<>
												<li className="nav-item">
													<a className="nav-link" href="/#who_we_are">Who we are</a>
												</li>
												<li className="nav-item">
													<a className="nav-link" href="/#how_it_work">How it works</a>
												</li>
												<li className="nav-item">
													<a className="nav-link" href="/#our_principals">Our principles</a>
												</li>
												<li className="nav-item">
													<a className="nav-link" href="/#account_types">Account types</a>
												</li>
												<li className="nav-item">
													<a className="nav-link" href="/#contact_us">Contact us</a>
												</li>
												<li className="nav-item">
													<a className="btn btn-signup" role="button" href="/register" onClick={this.onMenuClick}>Register</a>
												</li>
												<li className="nav-item">
													<a className="btn btn-signup" role="button" href="/login">Login</a>
												</li>
											</>
										}
										{(isAuthenticated) &&
											<>
												<li className="nav-item">
													<Link className={"nav-link " + ((this.state.active === 'crypto') ? 'active' : '')} onClick={() => this.stateActive('crypto')} to="/dashboard">Crypto Assets</Link>
												</li>
												<li className="nav-item">
													<Link className={"nav-link " + ((this.state.active === 'fiat') ? 'active' : '')} onClick={() => this.stateActive('fiat')} to="/fiat">Fiat Assets </Link>
												</li>
												<li className="nav-item">
													<Link className={"nav-link " + ((this.state.active === 'commodities') ? 'active' : '')} onClick={() => this.stateActive('commodities')} to="/commodities">Commodities Assets </Link>
												</li>
												<li className="nav-item">
													<Link className={"nav-link " + ((this.state.active === 'etf') ? 'active' : '')} onClick={() => this.stateActive('etf')} to="/etf">ETF Assets </Link>
												</li>
												<li className="nav-item">
													<Link className={"nav-link " + ((this.state.active === 'profile') ? 'active' : '')} to="/profile" onClick={() => this.stateActive('profile')}>Profile </Link>
												</li>
												<li className="nav-item">
													<Link className={"nav-link " + ((this.state.active === 'portfolio') ? 'active' : '')} to="/portfolio" onClick={() => this.stateActive('portfolio')}>Portfolio </Link>
												</li>
												<li className="nav-item">
													<Link className={"nav-link " + ((this.state.active === 'tradinghistory') ? 'active' : '')} to="/tradinghistory" onClick={() => this.stateActive('tradinghistory')}>Trading History </Link>
												</li>
												<li className="nav-item">
													<Link className={"nav-link " + ((this.state.active === 'my-cashier') ? 'active' : '')} to="/my-cashier" onClick={() => this.stateActive('my-cashier')}>My Cashier</Link>
												</li>
												<li className="nav-item">
													<Link className={"nav-link " + ((this.state.active === 'compliance') ? 'active' : '')} to="/compliance" onClick={() => this.stateActive('compliance')}>Compliance </Link>
												</li>
												<li className="nav-item">
													<Link className={"nav-link " + ((this.state.active === 'logout') ? 'active' : '')} onClick={(e) => this.logout(e)}>Logout </Link>
												</li>
											</>}
									</ul>
								</>}
						</Navbar>
					</div>
				</div>
				{/* MOBILE VIEW END  */}
				{isAuthenticated &&
					<div className="all-balances-header">
						<div className="container">
							<div className="row text-center">
								<div className="col-xs-12 col-sm-6 col-md-3">
									<p className="header_text">Free Margin: {all_balance.free_margin}</p>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-3">
									<p className="header_text">Equity: {all_balance.equity_eur}</p>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-3">
									<p className="header_text">Balance: {all_balance.free_balance}</p>
								</div>
								<div className="col-xs-12 col-sm-6 col-md-3">
									<p className="header_text">PNL: {all_balance.gross_pl}</p>
								</div>
							</div>
						</div>
					</div>
				}
			</div>
		)
	}
}
const mapStateToProps = (state) => {
	return {
		isLoading: state.AuthReducer.isLoading,
		isAuthenticated: state.AuthReducer.isAuthenticated,
		currentUser: state.AuthReducer.currentUser,
		name: state.AuthReducer.name,
		balance: state.AuthReducer.balance

	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		logoutFun: () => dispatch(logoutFun())
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Header2)