import React, { Component } from 'react';
import { Link } from "react-router-dom";
import logo from "../../../images/logo/blue.svg";
import fbicon from "../../../images/icons/fb.png";
import twicon from "../../../images/icons/tw.png";
import lnicon from "../../../images/icons/in.png";
export default class Footer extends Component {
        render() {
                return (
                        <div className="footer">
                                <div className="container">
                                        <div className="col-md-12 padding-0">
                                                <nav className="navbar navbar-expand-md ">
                                                        <div className="mx-auto">
                                                                <Link className="navbar-brand" to="#"><img alt="#" src={logo} /></Link>
                                                        </div>
                                                        <ul className="navbar-nav content-nav text-center ml-auto" style={{ paddingTop: "5px" }}>
                                                                <li className="nav-item">
                                                                        <Link className="nav-link" to="#">Home</Link>
                                                                </li>
                                                                <li className="nav-item">
                                                                        <Link className="nav-link" to="#">Contact us</Link>
                                                                </li>
                                                                <li className="nav-item">
                                                                        <Link className="nav-link" to="#">Terms of service</Link>
                                                                </li>
                                                                <li className="nav-item">
                                                                        <Link className="nav-link" to="#">Privacy Policy</Link>
                                                                </li>
                                                                <li className="nav-item">
                                                                        <Link className="nav-link" to="#">AML Policy</Link>
                                                                </li>
                                                                <li className="nav-item">
                                                                        <Link className="nav-link" to="#">Affiliate program</Link>
                                                                </li>
                                                                <li className="nav-item">
                                                                        <Link className="nav-link" to="#">Risk</Link>
                                                                </li>


                                                        </ul>
                                                        <ul className="navbar-nav social-nav mx-auto">
                                                                <li className="nav-item">
                                                                        <Link className="nav-link" to="#"> <img alt="#" src={lnicon} className="social-icon" /> </Link>
                                                                </li>
                                                                <li className="nav-item">
                                                                        <Link className="nav-link" to="#"> <img alt="#" src={twicon} className="social-icon" /> </Link>
                                                                </li>
                                                                <li className="nav-item">
                                                                        <Link className="nav-link" to="#"> <img alt="#" src={fbicon} className="social-icon" /> </Link>
                                                                </li>
                                                        </ul>
                                                </nav>
                                        </div>
                                </div>
                        </div>
                )
        }
}