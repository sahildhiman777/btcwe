import React, { Component } from 'react'
import {
  Link,
} from "react-router-dom";
import {
  Navbar,
} from 'react-bootstrap';
import ScrollMenu from "./scrollMenu/scrollMenu";
import history from '../../../history';
import "../header/header.css";
import { connect } from 'react-redux'
import { logoutFun } from '../../../actions/authAction';
import { authorizationView } from '../../authorization';

class Header extends Component {
  logout = () => {
    this.props.logoutFun();
  }

  constructor(props) {
    super(props);
    this.state = {
      visible: '',
      mobile_container: false,
      search: ''
    }
  }

  authorizationCheck = (pathname, viewRight) => {
    let { currentUser } = this.props;
    let listCheck = authorizationView(currentUser, pathname, viewRight);
    return listCheck;
  }
  componentWillMount() {
  }
  componentDidMount() {
    let urlPaths = history.location.pathname;
    const search = window.location.search;
    this.setState({ search: `/register${search}` })
  }

  onItemClick = () => {
    this.setState({ mobile_container: !this.state.mobile_container });
  }

  login = () => {
  }
  register = () => {
    this.setState({ mobile_container: !this.state.mobile_container });
  }
  home = () => {
    this.setState({ visible: '' });
  }
  profile = () => {
  }

  paymentmethod = () => {
  }

  dashboard = () => {
  }


  render() {
    return (
      <div className="header">
        <div className="container">
          <div className="desktop_view_menu">
            <Navbar expand="lg">

              <Navbar.Brand>
                <Link className="navbar-brand" to="/" onClick={() => this.home()}><img alt="#" src={require("../../../images/Logo.png")} /></Link>
              </Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
                <ul className="navbar-nav ml-auto" style={{ textAlign: "right" }}>
                  <ScrollMenu />
                  <li className="nav-item">
                    <Link className="btn btn-signup" role="button" to='/login'>Login</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="btn btn-signup" role="button" onClick={() => this.register()} to={this.state.search}>Register</Link>
                  </li>
                </ul>
              </Navbar.Collapse>
            </Navbar>
          </div>

          {/* Mobile Navbar Start  */}
          <div className="mobile_view_header">
            <Navbar expand="lg">

              <Navbar.Brand>
                <Link className="navbar-brand" to="/" onClick={() => this.home()}><img alt="#" src={require("../../../images/Logo.png")} /></Link>
              </Navbar.Brand>
              {this.state.mobile_container ?
                <div className="cross" onClick={this.onItemClick}>
                  <i class="fas fa-times"></i>
                </div>
                :
                <div className="mobile_menu_icon" aria-controls="basic-navbar-nav" onClick={this.onItemClick}>
                  <i class="fas fa-bars"></i>
                </div>
              }
              {(this.state.mobile_container) &&
                <>
                  <ul className="navbar-nav menu">
                    <ScrollMenu />
                    <li className="nav-item">
                      <Link className="btn btn-signup" role="button" onClick={() => this.register()} to={this.state.search}>Register</Link>
                    </li>
                    <li className="nav-item">
                      <Link className="btn btn-signup" role="button" to="/login">Login</Link>
                    </li>
                  </ul>
                </>}
            </Navbar>
          </div>
          {/* Mobile Navbar End  */}

          <div className={"banner-section " + this.state.visible}>

            <div className="row">
              <div className="col-md-12 col-lg-6">
                <div className="banner-style">
                  <div className="banner-text">
                    <h1>Buy. Sell. Trade.</h1>
                    <p>BTCWE are at the forefront of the crypto and Blockchain revolution.
                        We bring the stability of a market-leading financial services provider
                        and the security and transparency of a regulated crypto powerhouse.</p>
                  </div>
                </div>
              </div>
              <div className="col-md-12 col-lg-6 hide_mobile">
                <img alt="#" src={require("../../../images/img-btc.png")} className="img-fluid hide_mobile" width="627px" />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    isLoading: state.AuthReducer.isLoading,
    isAuthenticated: state.AuthReducer.isAuthenticated,
    currentUser: state.AuthReducer.currentUser

  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logoutFun: () => dispatch(logoutFun())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header)