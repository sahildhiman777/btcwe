import React, { Component } from 'react';

import { Link } from 'react-scroll';
class ScrollMenu extends Component {
    render() {
        return (
            <>
                <li className="nav-item">
                    <Link className="nav-link" to="who_we_are" spy={true} smooth={true} hashSpy={true} duration={500} delay={550} isDynamic={true} ignoreCancelEvents={false}>Who we are</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="how_it_work" spy={true} smooth={true} hashSpy={true} duration={500} delay={550} isDynamic={true} ignoreCancelEvents={false}>How it works</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="our_principals" spy={true} smooth={true} hashSpy={true} duration={500} delay={550} isDynamic={true} ignoreCancelEvents={false}>Our principles</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="account_types" spy={true} smooth={true} hashSpy={true} duration={500} delay={550} isDynamic={true} ignoreCancelEvents={false}>Account types</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="contact_us" spy={true} smooth={true} hashSpy={true} offset={-150} duration={500} delay={550} isDynamic={true} ignoreCancelEvents={false}>Contact us</Link>
                </li>
            </>
        );
    }
}

export default ScrollMenu;