import React, { Component } from 'react'
import Login from './login/login';
import ContactUs from './contactUs/contactUs';
import Compliance from './compliance/compliance';
import Withdrawal from './withdrawal/withdrawal';
import Withdraw from './withdraw/withdraw';
import AccountSnapshot from './accountSnapshot/accountSnapshot';
import KycPolicy from './kycPolicy/kycPolicy';
import PrivacyPolicy from './privacyPolicy/privacyPolicy';
import AntiMoney from './antiMoney/antiMoney';
import ForgotPassword from './forgotPassword/forgotPassword';
import AddFavourite from './addFavourite/addFavourite';
import ResetLink from '../shared/resetLink/resetLink';
import ResetPassword from './ResetPassword/resetPassword';
import ResetPass from '../shared/resetPass/resetPass';
import Account from './account/account';
import Register from './register/register';
import Footer from './layouts/footer/footer';
import Header from './layouts/header/header';
import Header2 from './layouts/header_2/header_2';
import Profile from '../containers/profile/profile';
import Dashboard from './dashboard/dashboard';
import Deposit from './deposit/deposit';
import Etf from './dashboard/etf/etf';
import Commodities from './dashboard/commodities/commodities';
import Fiat from './dashboard/fiat/fiat';
import Portfolio from './addFavourite/addFavourite';
import TradingHistory from './dashboard/tradinghistory/tradinghistory';
import TermsAndConditions from './TermsAndConditions/terms-and-conditions';
import Allassets from './allassets/allassest';
import Chart from '../shared/chart/chart';
import PageNotFound from './layouts/pageNotFound/pageNotFound';

import routes from '../routes';
import Home from './layouts/home';
import PrivateRoute from './PrivateRoute';
import {
  Route,
  withRouter,
  Redirect
} from "react-router-dom";
import { checkIfUserIsLoggedIn } from '../actions/authAction'
import { connect } from 'react-redux';
import { logoutFun } from '../actions/authAction';
import IdleTimer from 'react-idle-timer';
import CountryBlocked from './countryblocked/countryblocked'
class ContainerLayout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      timeout: 1000 * 60 * 15,
      showModal: false,
      userLoggedIn: false,
      isTimedOut: false
    }
    this.idleTimer = null
    this.onAction = this._onAction.bind(this)
    this.onActive = this._onActive.bind(this)
    this.onIdle = this._onIdle.bind(this)
  }
  _onAction(e) {
    let currentUser = '';
    currentUser = localStorage.getItem('currentUserId')
    if (currentUser) {
      this.setState({ isTimedOut: false })
    }
  }
  _onActive(e) {
    let currentUser = '';
    currentUser = localStorage.getItem('currentUserId')
    if (currentUser) {
      this.setState({ isTimedOut: false })
    }
  }
  _onIdle(e) {
    let currentUser = '';
    currentUser = localStorage.getItem('currentUserId')
    if (currentUser) {
      this.setState({ isTimedOut: true }, () => {
        this.props.logoutFun();
        this.props.history.push('/')
      })
    }
  }

  componentWillMount() {
    if (this.props.isAuthenticated) {
    }
  }

  authorizationCheck = (pathname, viewRight) => {
    let { currentUser } = this.props;
    let listCheck = (currentUser, pathname, viewRight);
    return listCheck;
  }
  render() {
    const { match } = this.props
    let urlPaths = window.location.pathname;
    let validRoute = routes.some(data => (urlPaths === data));
    let header_height;
    let header;
    if (urlPaths === '/') {
      header_height = 'header-height';
      header = <Header className={header_height} />
    }
    else if (urlPaths === '/dashboard') {
      header = <Header2 />
    }
    else {
      header = <Header2 />
    }
    let currentUser = '';
    currentUser = localStorage.getItem('currentUserId')
    if (currentUser === '') {
      if (urlPaths === '/dashboard') {
        this.props.history.push('/')
      }
      if (urlPaths === '/deposit') {
        this.props.history.push('/login')
      }
      if (urlPaths === '/fiat') {
        this.props.history.push('/login')
      }
      if (urlPaths === '/commodities') {
        this.props.history.push('/login')
      }
      if (urlPaths === '/etf') {
        this.props.history.push('/login')
      }
      if (urlPaths === '/profile') {
        this.props.history.push('/login')
      }
      if (urlPaths === '/portfolio') {
        this.props.history.push('/login')
      }
      if (urlPaths === '/tradinghistory') {
        this.props.history.push('/login')
      }
      if (urlPaths === '/my-cashier') {
        this.props.history.push('/login')
      }
      if (urlPaths === '/compliance') {
        this.props.history.push('/login')
      }
    }
    if (currentUser != null) {
      if (urlPaths === '/register') {
        this.props.history.push('/')
      }
    }
    
    return (
      <>  <IdleTimer
        ref={ref => { this.idleTimer = ref }}
        element={document}
        onActive={this.onActive}
        onIdle={this.onIdle}
        onAction={this.onAction}
        debounce={250}
        timeout={this.state.timeout} />

        {header}
        <div>
          {currentUser !== '' &&
            <>
              {(this.authorizationCheck('dashboard', 'VIEW') && validRoute) && <PrivateRoute exact path="/(dashboard|fiat|etf|commodities)/" component={Dashboard} />}
              {(this.authorizationCheck('profile', 'VIEW') && validRoute) && <PrivateRoute exact path="/profile" component={Profile} />}
              {(this.authorizationCheck('portfolio', 'VIEW') && validRoute) && <PrivateRoute exact path="/portfolio" component={Portfolio} />}
              {(this.authorizationCheck('deposit', 'VIEW') && validRoute) && <PrivateRoute exact path="/deposit" component={Deposit} />}
              {(this.authorizationCheck('withdraw-request', 'VIEW') && validRoute) && <PrivateRoute exact path="/withdraw-request" component={Withdraw} />}
              {(this.authorizationCheck('my-cashier', 'VIEW') && validRoute) && <PrivateRoute exact path="/my-cashier" component={AccountSnapshot} />}
              {(this.authorizationCheck('compliance', 'VIEW') && validRoute) && <PrivateRoute exact path="/compliance" component={Compliance} />}
              {(this.authorizationCheck('tradinghistory', 'VIEW') && validRoute) && <PrivateRoute exact path="/tradinghistory" component={TradingHistory} />}
              {(this.authorizationCheck('allassets', 'VIEW') && validRoute) && <PrivateRoute exact path="/allsessts" component={Allassets} />}
            </>
          }
          {(currentUser === '') && <Route exact path="/dashboard" component={Login} />}
          {(currentUser === '') && <Route exact path="/profile" component={Login} />}
          {(currentUser === '') && <Route exact path="/commodities" component={Login} />}
          {(currentUser === '') && <Route exact path="/fiat" component={Login} />}
          {(currentUser === '') && <Route exact path="/etf" component={Login} />}
          {(currentUser === '') && <Route exact path="/portfolio" component={Login} />}
          {(currentUser === '') && <Route exact path="/withdraw-request" component={Login} />}
          {(currentUser === '') && <Route exact path="/my-cashier" component={Login} />}
          {(currentUser === '') && <Route exact path="/compliance" component={Login} />}
          {(currentUser === '') && <Route exact path="/tradinghistory" component={Login} />}
          {(currentUser === '') && <Route exact path="/allsessts" component={Login} />}

          <Route exact path="/account" component={Account} />
          <Route exact path="/contact-us" component={ContactUs} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/login/:token" component={Login} />
          <Route exact path="/register" component={Register} />
          <Route exact path="/withdrawal" component={Withdrawal} />
          <Route exact path="/kyc-policy" component={KycPolicy} />
          <Route exact path="/privacy-policy" component={PrivacyPolicy} />
          <Route exact path="/anti-money" component={AntiMoney} />
          <Route exact path="/forgot-password" component={ForgotPassword} />
          <Route exact path="/reset-password" component={ResetPassword} />
          <Route exact path="/reset-link" component={ResetLink} />
          <Route exact path="/reset-pass" component={ResetPass} />
          <Route exact path="/" component={Home} />
          <Route exact path="/terms-and-conditions" component={TermsAndConditions} />
          <Route exact path="/allassets" component={Allassets} />
          <Route exact path="/chart" component={Chart} />
          <Route exact path="/countryblocked" component={CountryBlocked} />
          {
            (!validRoute) &&
          <Route path="*" component={PageNotFound} />
          }
        </div>
        <Footer />
      </>
    )
  }

}

const mapStateToProps = (state) => {
  return ({
    isAuthenticated: state.AuthReducer.isAuthenticated,
    accessToken: state.AuthReducer.accessToken,
    isLoading: state.AuthReducer.isLoading,
    currentUser: state.AuthReducer.currentUser
  })
}

const mapDispatchToProps = (dispatch) => {
  return {
    checkIfUserIsLoggedIn: () => dispatch(checkIfUserIsLoggedIn()),
    logoutFun: () => dispatch(logoutFun())
  }
}

export default withRouter(connect(mapStateToProps
  , mapDispatchToProps
)(ContainerLayout));