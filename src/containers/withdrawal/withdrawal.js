import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { loginRequest, updateRedirectRequest, getCountry } from '../../actions/authAction';
import { Helmet } from "react-helmet";
import * as Scroll from 'react-scroll';
import "../withdrawal/withdrawal.css";
import ReactLoading from "react-loading";

let scroll = Scroll.animateScroll;
class Withdrawal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectState: false,
      email: "test3@gmail.com",
      username: "test",
      blockloader:true,
    }
  }
  validateForm = () => {
    return this.state.email.length > 0 && this.state.username.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  contactus = (e) => {
    e.preventDefault();
    let data = {
      email: (this.state.email !== '') ? this.state.email : "test3@gmail.com",
      username: (this.state.username !== '') ? this.state.username : "test"
    }
  }

  componentDidMount() {
    scroll.scrollToTop({ smooth: "linear", duration: 100 });
    this.props.updateRedirectRequest();
  }

  componentWillReceiveProps(nxtProps) {
  }
  componentWillMount(){
    getCountry().then(res => {
      if(res.props != undefined){
      if(res.props.to){
          this.props.history.push("/countryblocked");
          this.setState({blockloader:false});
      }
  }else{
    this.setState({blockloader:false});
  }
  })
  }

  render() {
    return (
      <>
      {(this.state.blockloader) &&
          <>
            <div className="blockloader">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
          </>}
        <div className="withrawal-over" >
          <div className="container withrawal-body">
            <Helmet>
              <style>{'.navbar{padding-right: 0px; padding-left:0px;} .nav-item .btn-signup{background-color:#3445CF !important;color:#fff !important; padding: 9px 43px !important;}'}</style>
            </Helmet>
            <div className="row margin-top body-color">
              <div className="col-md-12">
                <div className="theme-title-withdrawal margin-bottom-100">
                  <h2 className="text-center"><img alt="#" src={require("../../images/line.png")} />HOW TO WITHDRAW FROM YOUR BTCWE ACCOUNT - FAQ </h2>
                </div>

                <h4 className="question-color"> How do I withdraw money?</h4>
                <p className="paragraph-color">Want to withdraw funds from your account? No problem! Customers may withdraw funds from their Btcwe account at any time. Funds can be withdrawn up to the value of the balance of your Btcwe account.</p>
                <div className="row">
                  <div className="col-md-8">
                    <p className="paragraph-color">Funds are withdrawn using the same method, and to the same account as previously used to deposit, in the following order of priority:</p>
                    <p className="paragraph-small-color">* Please note you will be asked to provide an alternative payment method to be used in the event that you are withdrawing more than you originally deposited or we are unable to return your funds to your original deposit method.</p>
                  </div>
                  <div className="col-md-4">
                    <ul type="none" className="methods-ul">
                      <li><div><img src={require('../../images/icons/BTC.svg')} alt="#" /></div><span> Bitcoin</span></li>
                      <li><div className="credit-bg"> <img src={require('../../images/icons/credit-card 1.svg')} alt="#" /></div><span> Credit card</span></li>
                      <li><div className="payment-bg"><img src={require('../../images/icons/Payment.svg')} alt="#" /></div><span> Alt Payment Method</span></li>
                      <li><div className="bank-bg"><img src={require('../../images/icons/bank-bg.svg')} alt="#" /></div><span> Bank transfer</span></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="long-take  margin-top-100">
            <div className="container withrawal-body">
              <div className="row margin-bottom-100">
                <div className="col-md-12" style={{ marginTop: "-55px" }}>
                  <h4 className="question-color"> How long will it take to process my withdrawal request and when will I receive my funds?</h4>
                  <p className="paragraph-color">Withdrawal requests take up to 1 business day to process, considering we have all necessary information. The time taken to receive your payment may vary depending on the payment provider involved. The typical time frame per payment method can be seen in the table below:</p>
                  <div className="col-md-8 offset-md-2 margin-bottom-100">
                    <div className="withdrawal-table-responsive">
                      <table className="table text-left table-striped table-hover table-color">

                        <tbody>
                          <tr>
                            <td><span className="th-txt">Payment Methods</span></td>
                            <td><span className="th-txt">Time Taken To Receive Funds</span></td>
                          </tr>
                          <tr className="td-txt">
                            <td> <div> <img src={require('../../images/icons/BTC.svg')} alt="#" /></div><span> Bitcoin</span></td>
                            <td>1 working days</td>
                          </tr>
                          <tr className="td-txt">
                            <td><div className="credit-td-bg"> <img src={require('../../images/icons/credit-card 1.svg')} alt="#" /></div><span> Credit / Debit card</span></td>
                            <td>Up to 5 working days</td>
                          </tr>
                          <tr className="td-txt">
                            <td><div className="bank-td-bg"><img src={require('../../images/icons/bank-bg.svg')} alt="#" /></div><span> Wire Transfer</span></td>
                            <td>Up to 5 working days</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="cancel-request">
            <div className="container withrawal-body padding-bottom-50">
              <h4 className="question-color"> How can I cancel my withdrawal?</h4>
              <p className="paragraph-color">In order to cancel your withdrawal request please follow one of the steps below:</p>
              <div className="bol-p">
                <p>1. Contact your account manager</p>
                <p>2. Contact support department at <a href="mailto:info@btcwe.io" className="anchor-color"><b>info@btcwe.io</b></a></p>
              </div>

              <h4 className="question-color">What are the conditions to open a withdrawal request?</h4>
              <div className="bol-p">
                <p>1. The minimum amount for withdrawal is 50$</p>
                <p>2. You have to have available withdrawable funds</p>
                <p>3. Your account needs to be verified which means you have the green tick in your profile (if you haven’t yet verified your account, please contact us and upload the required documents)</p>
              </div>
              <p className="paragraph-color">All your personal information, payment details and documents are managed according to a very firm information security policy, as expected from an organization that provides Financial Services to end users and is in charge of the Safekeeping of Clients Accounts. You can find more information here:
            <Link to='/privacy-policy' className="anchor-color"><b> Privacy Policy.</b></Link></p>
            </div>
          </div>
          <div className="processing-request margin-top-100 margin-bottom-100">
            <div className="container withrawal-body">
              <h4 className="question-color">Are there any withdrawal fees?</h4>
              <p className="paragraph-color">Yes, withdrawal requests are subjected to a withdrawal processing fee. You will see the actual withdrawal fee in the Cashier after you have entered the withdrawal amount; at this point you still can abort the request.</p>
              <div className="col-md-8 offset-md-2">
                <div className="fees-table-responsive">
                  <table className="table text-left table-striped table-hover table-color">
                    <tbody>
                      <tr>
                        <td><span className="th-txt">Withdrawal Amount (USD)</span></td>
                        <td><span className="th-txt">Fee (USD)</span></td>
                      </tr>
                      <tr>
                        <td><div> <img src={require('../../images/icons/BTC.svg')} alt="#" /></div> <span>50.00 +</span></td>
                        <td>25.00</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

              <p className="paragraph-small-color margin-bottom-80">* Please note, you may be charged by the funding provider involved in the transfer (Intermediary bank, receiving bank, or your credit card provider).</p>

              <h4 className="question-color">How will I know if my withdrawal request has been processed?</h4>
              <p className="paragraph-color">You will be notified via email once the processing of your withdrawal request has been completed and informed to which payment provider your funds were sent. In addition you can click on ‘Portfolio’ and then on the history tab to see the withdrawal status.</p>

              <h4 className="question-color">What shall I do if I cannot see my funds?</h4>
              <p>1. Click on portfolio tab then on the history tab in order to see to which payment method the funds were sent to. Alternatively you can check the email you received in relation to the processing of your withdrawal, which will be specify the payment method to which the funds were sent and the date processed.</p>
              <p>2. Take into consideration that from the time we process your withdrawal, it can take up to 8 business days for the funds to appear in your account statement.</p>
              <p>3. If all the above was checked and you still cannot find the funds, please contact us and provide us with a payment method statement showing the incoming and outgoing transactions from the time the withdrawal was processed until the time you contacted us. Please make sure we can see your name, the transaction dates and the payment method details.</p>
            </div>
          </div>
          <div className="container">
            <p className="material-color margin-top-100 margin-bottom-100">The materials on this website are for illustration and discussion purposes only and do not constitue an offering. An offering may be made only by delivering of a confidential offering memorandum to appropriate investors. Past performance is no guarantee of future results.</p>
          </div>
        </div>
      </>)
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.AuthReducer.isAuthenticated,
    accessToken: state.AuthReducer.accessToken,
  };
};

const mapDispatchToProps = (dispatch) => ({
  loginRequest: (data) => dispatch(loginRequest(data)),
  updateRedirectRequest: () => dispatch(updateRedirectRequest())
})

export default connect(mapStateToProps, mapDispatchToProps)(Withdrawal)