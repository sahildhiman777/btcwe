import React
  from 'react';

import {
  Route,
  Redirect,
  withRouter
} from "react-router-dom";
import { connect } from 'react-redux';


const PrivateRoute = ({ component: Component, isAuthenticated, currentUser, accessToken, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        (isAuthenticated) ? (
          <Component {...props} />
        ) : (
            <Redirect
              to={{
                pathname: "/login",
                state: { from: props.location }
              }}
            />
          )
      }
    />
  );
}
const mapStateToProps = state => ({
  isAuthenticated: state.AuthReducer.isAuthenticated,
  currentUser: state.AuthReducer.currentUser,
  accessToken: state.AuthReducer.accessToken
});

export default withRouter(
  connect(
    mapStateToProps,
    {}
  )(PrivateRoute)
);