
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import { contactUsRequest, getCountry } from '../../actions/authAction';
import { Button, FormGroup, FormControl, FormLabel, Card } from "react-bootstrap";
import "../contactUs/contactUs.css";
import * as Scroll from 'react-scroll';
import $ from 'jquery';
import ReactLoading from "react-loading";

let scroll = Scroll.animateScroll;
class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectState: false,
      email: "",
      username: "",
      subject: "",
      message: "",
      blockloader:true,
    }
  }
  validateForm = () => {
    return this.state.email.length > 0 && this.state.username.length > 0 && this.state.message.length > 0 && this.state.subject.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }
  componentWillMount(){
    getCountry().then(res => {
      if(res.props != undefined){
      if(res.props.to){
          this.props.history.push("/countryblocked");
          this.setState({blockloader:false});
      }
  }else{
    this.setState({blockloader:false});
  }
  })
  }


  contactus = (e) => {
    e.preventDefault();
    let data1 = {
      email: (this.state.email !== '') && this.state.email,
      name: (this.state.username !== '') && this.state.username,
      message: (this.state.message !== '') && this.state.message,
      subject: (this.state.subject !== '') && this.state.subject
    }
    this.props.contactUsRequest(data1);
  }

  componentDidMount() {
    $(document).ready(function () {
      $(".mobile_menu_icon").removeClass("hide_mobile");
    });
    scroll.scrollToTop({ smooth: "linear", duration: 100 });
    this.nameInput.focus();
  }

  componentWillReceiveProps(nxtProps) {
    if (nxtProps.isAuthenticated) {
      nxtProps.history.push('/dashboard');
    }
  }

  render() {
    if (this.props.isRedirect !== '') {
      this.props.history.push(this.props.isRedirect)
    }
    let contactUs = this.props.CONTACT_US;
    let contactmes = ''; let sub = '';
    if (contactUs !== '') {
      contactmes = this.props.CONTACT_US['message'];
      sub = this.props.CONTACT_US['sub-message'];
    }
    return (
      <div>
        {(this.state.blockloader) &&
          <>
            <div className="blockloader">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
          </>}
        <div className="contact_div margin-bottom-100 ">
          <Helmet>
            <style>{'.navbar{padding-right: 0px; padding-left:0px;} .nav-item .btn-signup{background-color:#3445CF !important;color:#fff !important; padding: 9px 43px !important;}'}</style>
          </Helmet>
          <div className="row margin-top-50">
            <div className="col-md-12 col-lg-6 contact-section">
              <div className="contactus_section">
                {(contactUs === '') &&
                  <>

                    <h1 className="contact-header-text">Submit query</h1>
                    <p>For questions, queries and information, please fill the form. You will get a response in up to 24h. </p>
                    <Card >
                      <Card.Body>
                        <form onSubmit={(e) => { this.contactus(e) }}>
                          <FormGroup controlId="username">
                            <FormLabel>Name</FormLabel>
                            <FormControl
                              name="username"
                              value={this.state.username}
                              onChange={this.handleChange}
                              type="text"
                              ref={(input) => { this.nameInput = input; }}
                            />

                          </FormGroup>
                          <FormGroup controlId="email">
                            <FormLabel>Email</FormLabel>
                            <FormControl
                              autoFocus
                              name="email"
                              type="email"
                              value={this.state.email}
                              onChange={this.handleChange}
                            />
                          </FormGroup>
                          <FormGroup controlId="subject">
                            <FormLabel>Subject</FormLabel>
                            <FormControl
                              autoFocus
                              name="subject"
                              type="text"
                              value={this.state.subject}
                              onChange={this.handleChange}
                            />
                          </FormGroup>

                          <FormGroup controlId="message" className="margin-bottom-50">
                            <FormLabel>I would like to discuss</FormLabel>
                            <FormControl
                              name="message"
                              value={this.state.message}
                              onChange={this.handleChange}
                              as="textarea" rows="3" />
                          </FormGroup>
                          <div className="contact_button">
                            <Button className="btn-signup-custom"
                              disabled={!this.validateForm()}
                              type="submit"
                            >
                              Get free consultation
                    </Button>
                          </div>
                        </form>

                      </Card.Body>
                    </Card>
                  </>
                }
                {(contactUs !== '') &&
                  <>
                    <div className="message">
                      <h1 className="contact-header-text text-left" style={{ marginTop: "20px" }}>{contactmes}</h1>
                      <p>{sub}</p>
                    </div>
                  </>
                }
              </div>
            </div>
            <div className="col-md-12 col-lg-6">
              <div className="map-body text-center">
                <h4 className="map_heading hide_on_desktop"><img alt="#" className="margin-right-15" src={require("../../images/line.png")} /> How to find us</h4>
                <div className="map">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.661997591653!2d-0.11064214827410958!3d51.519416779537146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761b4dcd189fd7%3A0xd7a32525c194683e!2s88-90%20Hatton%20Garden%2C%20Holborn%2C%20London%20EC1N%208PN%2C%20UK!5e0!3m2!1sen!2sin!4v1586333859346!5m2!1sen!2sin"></iframe>
                </div>
              </div>
            </div>
          </div>
          <div className="row margin-top-100">
            <div className="col-md-4 col-lg-4 ">
              <div className="contact-comment">
                <div className="float-left padding-3">
                  <img alt="#" src={require("../../images/icons/comment.svg")} />
                </div>
                <div className="float-left margin-left-10">
                  <p>Email</p>
                  <p className="par-blue">contact@btcwe.io</p>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-lg-4">
              <div className="contact-comment margin-left-10">
                <div className="float-left padding-3">
                  <img alt="#" src={require("../../images/icons/phone.svg")} />
                </div>
                <div className="float-left margin-left-10">
                  <p>Phone</p>
                  <p className="par-blue">30029 3993 33</p>
                </div>
              </div>
            </div>
            <div className="col-md-4 col-lg-4">
              <div className="contact-comment">
                <div className="float-left padding-3">
                  <img alt="#" src={require("../../images/icons/location.svg")} />
                </div>
                <div className="float-left margin-left-10">
                  <p>Location</p>
                  <p className="par-blue">88-90 Hatton Garden</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.AuthReducer.isAuthenticated,
    accessToken: state.AuthReducer.accessToken,
    isRedirect: state.AuthReducer.isRedirect,
    CONTACT_US: state.AuthReducer.CONTACT_US
  };
};

const mapDispatchToProps = (dispatch) => ({
  contactUsRequest: (data) => dispatch(contactUsRequest(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ContactUs)