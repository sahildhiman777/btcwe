import { Helmet } from "react-helmet";
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { forgotRequest, getCountry } from '../../actions/authAction';
import { Button, FormGroup, FormControl, FormLabel, Card } from "react-bootstrap";
import "../forgotPassword/forgotPassword.css";
import * as Scroll from 'react-scroll';
import ReactLoading from "react-loading";

let scroll = Scroll.animateScroll;
export class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectState: false,
      email: "",
      blockloader:true,
    }
  }
  validateForm = () => {
    return this.state.email.length > 0;
  }
  componentWillMount(){
    getCountry().then(res => {
      if(res.props != undefined){
      if(res.props.to){
          this.props.history.push("/countryblocked");
          this.setState({blockloader:false});
      }
  }else{
    this.setState({blockloader:false});
  }
  })
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  forgot = (e) => {
    e.preventDefault();
    let data = {
      email: (this.state.email !== '') ? this.state.email : "test3@gmail.com",
    }
    this.props.forgotRequest(data);
  }

  componentDidMount() {
    scroll.scrollToTop({ smooth: "linear", duration: 100 });
  }

  componentWillReceiveProps(nxtProps) {
    if (nxtProps.isAuthenticated) {
      nxtProps.history.push('/dashboard');
    }
  }

  render() {
    if (this.props.isRedirect !== '') {
      this.props.history.push(this.props.isRedirect)
    }
    return (
      <div className=" forgot-section-cointainer">
        {(this.state.blockloader) &&
          <>
            <div className="blockloader">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
          </>}
        <div className="container padding-top-50">
          <Helmet>
            <style>
              {".header2 .container{ max-width:unset; margin:unset;padding-left:6%;} #root{overflow:hidden;} .forgot-section-cointainer .container { transform: skew(0deg, 2deg);}.forgot-section-cointainer { transform: skew(0deg, -2deg); background-repeat: no-repeat; border-radius: 0px 104px 0px 104px;} ul.ml-auto{display:none !important;}  .footer{ display:none !important;}"}</style>
          </Helmet>
          <div className="row margin-top-50">
            <div className=" col-md-12 col-lg-6 forgot-section">
              <div className="col-md-12">
                <h1 className="forgot-header-text text-left">Forgot password?</h1>
                <p className=" margin-bottom-20">Enter the email address associated with your account.</p>
                <Card >
                  <Card.Body>
                    <form onSubmit={(e) => { this.forgot(e) }}>
                      <FormGroup className="margin-bottom-50" controlId="email">
                        <FormLabel>Email</FormLabel>
                        <FormControl
                          autoFocus
                          name="email"
                          type="email"
                          value={this.state.email}
                          onChange={this.handleChange}
                        />
                      </FormGroup>

                      <div className="forget_button">
                        <Button className="btn-signup-custom"
                          disabled={!this.validateForm()}
                          type="submit"
                        >
                          Reset
                    </Button>
                      </div>
                      <FormGroup className="margin-top-50 return_to_login">
                        <Link to="/login" className="anchor-signup margin-top-30">Return to login </Link></FormGroup>
                    </form>

                  </Card.Body>
                </Card>
              </div>
            </div>
            <div className="col-md-12 col-lg-6 message-section">
              <div className="col-md-12 col-lg-12 message-section-2">
                <div className="text-rotate">
                  <div className="margin-bottom-20">
                    <img src={require('../../images/bckground/undraw_forgot_password_gi2d 1.svg')} className="img-fluid" alt="#" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-3 col-lg-4"></div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.AuthReducer.isAuthenticated,
    accessToken: state.AuthReducer.accessToken,
    isRedirect: state.AuthReducer.isRedirect
  };
};

const mapDispatchToProps = (dispatch) => ({
  forgotRequest: (data) => dispatch(forgotRequest(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)