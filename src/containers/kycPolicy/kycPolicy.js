import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
import { loginRequest, updateRedirectRequest, getCountry } from '../../actions/authAction';
import { Modal } from "react-bootstrap";
import "../kycPolicy/kycPolicy.css";
import * as Scroll from 'react-scroll';
import $ from 'jquery';
import ReactLoading from "react-loading";

let scroll = Scroll.animateScroll;
class KycPolicy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectState: false,
      email: "test3@gmail.com",
      username: "test",
      showModal: false,
      blockloader:true,
    };

    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }
  componentWillMount(){
    getCountry().then(res => {
      if(res.props != undefined){
      if(res.props.to){
          this.props.history.push("/countryblocked");
          this.setState({blockloader:false});
      }
  }else{
    this.setState({blockloader:false});
  }
  })
  }

  handleOpenModal() {
    this.setState({ showModal: true });
  }

  handleCloseModal() {
    this.setState({ showModal: false });
  }

  validateForm = () => {
    return this.state.email.length > 0 && this.state.username.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  contactus = (e) => {
    e.preventDefault();
    let data = {
      email: (this.state.email !== '') ? this.state.email : "test3@gmail.com",
      username: (this.state.username !== '') ? this.state.username : "test"
    }
  }

  componentDidMount() {
    $(document).ready(function () {
      $(".mobile_menu_icon").removeClass("hide_mobile");
    });
    scroll.scrollToTop({ smooth: "linear", duration: 100 });
    this.props.updateRedirectRequest();
  }

  componentWillReceiveProps(nxtProps) {
  }

  render() {
    return (
      <div>
        {(this.state.blockloader) &&
          <>
            <div className="blockloader">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
          </>}
      <div className="kyc-section-cointainer">
        <div className="container">
          <Helmet>
            <style>
              {" .nav-item .btn-signup{background-color:#3445CF !important;color:#fff !important; padding: 9px 43px !important;} .kyc-section-cointainer .container{ transform: skew(0deg, 5deg);}.kyc-section-cointainer { transform: skew(0deg, -5deg); background:#f9fafe; background-repeat: no-repeat; border-radius: 0px 104px 0px 104px;}"}</style>
          </Helmet>
          <div className="row margin-top-100 body-color">
            <div className="col-md-12">
              <div className="theme-title-kyc margin-bottom-100">
                <h2 className="text-center"><img alt="#" src={require("../../images/line.png")} /> Kyc Policy</h2>
              </div>

              <h4 className="question-color">1. Basic BTCWE Services.</h4>
              <p className="paragraph-color"><span>1.1. Eligibility.</span> To be eligible to use the BTCWE Services, you must be at least 18 years old.
            Your eligibility to access certain BTCWE Services also depends on the country in which you reside. BTCWE Services that are not accessible by countries can be found <Link to="#" className="anchor-color" onClick={this.handleOpenModal} data-toggle="modal" data-target="#myModal-list"><b>here</b></Link>
              </p>
              <p className="paragraph-color"><span>1.2. BTCWE Services.</span> Your BTCWE account encompasses the following basic BTCWE services: One or more hosted Digital Currency wallets that allow users to store certain supported digital currencies, like Bitcoin or Ethereum ("Digital Currency"), and to track, transfer, and manage their supported Digital Currencies (the "Hosted Digital Currency Wallet"); Digital Currency conversion services through which users can buy and sell supported Digital Currencies in transactions with  BTCWE (the "Conversion Services"); and a Fiat Currency (e.g. GBP, EUR, CAD) account for use in connection with other  BTCWE Services (a "Currency Wallet") and for eligible users, a Digital Currency exchange platform (collectively the " BTCWE Services").<br /> <b>The risk of loss in trading or holding Digital Currency can be substantial. You should therefore carefully consider whether trading or holding Digital Currency is suitable for you in light of your financial condition.</b></p>

              <h4 className="question-color">2. Creating an BTCWE Account.</h4>
              <p className="paragraph-color"><span>2.1. Registration of BTCWE Account.</span> In order to use any of the BTCWE Services, you must first register by providing your name, an e-mail address, password, and affirming your acceptance of this Agreement. BTCWE may, in our sole discretion, refuse to allow you to establish a BTCWE Account, or limit the number of BTCWE Accounts that a single user may establish and maintain at any time.</p>
              <p className="paragraph-color"><span>2.2. Identity Verification.</span> In order to use certain features of the BTCWE Services, including certain transfers of Digital Currency and/or government-issued currency ("Fiat Currency"), you may be required to provide BTCWE with certain personal information, including, but not limited to, your name, address, telephone number, e-mail address, date of birth, taxpayer identification number, government identification number, and information regarding your bank account (e.g., financial institution, account type, routing number, and account number). In submitting this or any other personal information as may be required, you verify that the information is accurate and authentic, and you agree to update BTCWE if any information changes.
            <br />
                <br />
                <b className="paragraph-color">You hereby authorize BTCWE to, directly or through third parties make any inquiries we consider necessary to verify your identity and/or protect against fraud, including to query identity information contained in public reports (e.g., your name, address, past addresses, or date of birth), to query account information associated with your linked bank account (e.g., name or account balance), and to take action we reasonably deem necessary based on the results of such inquiries and reports. You further authorize any and all third parties to which such inquiries or requests may be directed to fully respond to such inquiries or requests.</b>
              </p>
              <p className="material-color margin-top-100 margin-bottom-100">The materials on this website are for illustration and discussion purposes only and do not constitue an offering. An offering may be made only by delivering of a confidential offering memorandum to appropriate investors. Past performance is no guarantee of future results.</p>

            </div>
          </div>
        </div>
        <Modal show={this.state.showModal} onHide={this.handleCloseModal}>
          <Modal.Header closeButton><h4>Blocked Countries</h4>
          </Modal.Header>
          <Modal.Body>
            <div className="row">
              <div className="col-md-3 col-sm-3">
                Afghanistan<br />
                Albenia<br />
                Algeria<br />
                Andorra<br />
                Angola<br />
                Anguilla<br />
                Argentina<br />
                Armenia<br />
                Azarbaijan<br />
                Bangaledash<br />
                Belarus<br />
                Benin<br />
                Bhutan<br />
                Bosnia and Hertzegovina<br />
                Bolivia<br />
                Brazil<br />
                Bulgaria<br />
                Burkina Paso<br />
                Burundi<br />
                Cambodia<br />
                Cameroon<br />
                Cape Verde<br />
                Central Africans Republic<br />
                Chad<br />
                Chile<br />
                China<br />
                Comoros<br />
              </div>
              <div className="col-md-3 col-sm-3">
                Congo<br />
                Congo, the bema Republic<br />
                Colombia<br />
                Costa Rica<br />
                Cominica<br />
                Cote B'loire<br />
                Cuba<br />
                Dominican Republic<br />
                El Salvador<br />
                Egypt<br />
                Erithrea<br />
                Equatorial Guinea<br />
                Equador<br />
                Gambia<br />
                Ghana<br />
                Georgia<br />
                Guadeloupe<br />
                Guam<br />
                Guatemala<br />
                Gubon<br />
                Guinea<br />
                Guine - Bissau<br />
                Haiti<br />
                India<br />
                Indonesia<br />
                Iraq<br />
                Israel<br />

              </div>
              <div className="col-md-3 col-sm-3">
                Japan<br />
                Kazakhstan<br />
                Kenya<br />
                Kosovo<br />
                Kyrgustan<br />
                Lao People Democracy<br />
                Latvia<br />
                Liberia<br />
                Libya<br />
                Malawi<br />
                Mali<br />
                Macedonia<br />
                Mexico<br />
                Moldova<br />
                Panama<br />
                Paraguay<br />
                Peru<br />
                Phillipines<br />
                Pitcairn<br />
                Senegal<br />
                Sierra Leone<br />
                Sri Lanka<br />
                Suriname<br />
                Swaziland<br />
                Syrian Arab Republic<br />
                Taiwan<br />
                Tajikistan<br />
              </div>
              <div className="col-md-3 col-sm-3">
                Tanzania<br />
                Thailand<br />
                Timor Leste<br />
                Tokelau<br />
                Tonga<br />
                Tunisia<br />
                Turkmenistan<br />
                Turkey<br />
                Tuvalu<br />
                Uganda<br />
                Ukraine<br />
                United States<br />
                Urugway<br />
                Uzbekistan<br />
                Vanuatu<br />
                Vietnam<br />
                Venezuela<br />
                Western Sahara<br />
                Zambia<br />
                Zimbabwe<br />
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </div>
      </div>)
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.AuthReducer.isAuthenticated,
    accessToken: state.AuthReducer.accessToken,
  };
};

const mapDispatchToProps = (dispatch) => ({
  loginRequest: (data) => dispatch(loginRequest(data)),
  updateRedirectRequest: () => dispatch(updateRedirectRequest())
})

export default connect(mapStateToProps, mapDispatchToProps)(KycPolicy)