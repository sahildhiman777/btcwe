import React, { Fragment } from 'react';
import '../profile/profile.css';
import { getuserdetails, changePassword, uploadProfilePhoto } from '../../actions/authAction';
import { connect } from 'react-redux';
import {
    Redirect
} from "react-router-dom";
import UploadProfile from "../../shared/UploadProfile/UploadProfile";
import emailimg from "../../images/icons/verify_email.svg";
import { authorizationView } from '../authorization';
import ReactLoading from "react-loading";
import $ from 'jquery';
import { Button, FormLabel, FormGroup, InputGroup, FormControl } from 'react-bootstrap';
import * as Scroll from 'react-scroll';
let scroll = Scroll.animateScroll;
class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            redirectState: false,
            email: "",
            fullName: "",
            phoneNumber: "",
            DOB: "",
            UserInfo: '',
            street: '',
            city: '',
            state: '',
            country: '',
            postalCode: '',
            password: "",
            repeat_password: "",
            old_password: "",
            message: '',
            type: 'password',
            type2: 'password',
            type3: 'password',
            profile_image: '',
            formData: '',
            isLoading: true,
        }
    }

    componentDidMount() {
        $(document).ready(function () {
            $(".mobile_menu_icon").removeClass("hide_mobile");
        });
        scroll.scrollToTop({ smooth: "linear", duration: 100 });
        let data = {
            email: localStorage.getItem('currentUserId')
        }
        getuserdetails(data).then(res => {
            if (res.length !== "") {
                if (res.full_name.length > 0) {
                    this.setState({
                        email: res.email, phoneNumber: res.mobile, fullName: res.full_name,
                        isLoading: false
                    });
                }
            }
        }).catch(err => { console.log(err, 'err') })
    }
    authorizationCheck = (pathname, viewRight) => {
        let { currentUser } = this.props;
        let listCheck = authorizationView(currentUser, pathname, viewRight);
        return listCheck;
    }
    validateForm = () => {
        return this.state.email.length > 0 && this.state.fullName.length > 0;
    }

    onChange(event) {
        event.stopPropagation();
        this.setState({
            [event.target.name]: event.target.value
        });
    }
    handleClick = () => this.setState(({ type }) => ({
        type: type === 'text' ? 'password' : 'text'
    }));
    handleClick2 = () => this.setState(({ type2 }) => ({
        type2: type2 === 'text' ? 'password' : 'text'
    }));
    handleClick3 = () => this.setState(({ type3 }) => ({
        type3: type3 === 'text' ? 'password' : 'text'
    }));
    reset = (e) => {
        e.preventDefault();
        if (this.state.password !== this.state.repeat_password) {
            return this.setState({ message: 'Password not matched!!' });
        } else {
            let data = {
                email: (this.state.email !== '') ? this.state.email : "",
                password: (this.state.password !== '') ? this.state.password : "",
                old_password: (this.state.old_password !== '') ? this.state.old_password : ""
            }
            this.setState({ message: '' });
            this.props.changePassword(data);
        }
    }
    validatePasswordForm = () => {
        if (this.state.email.length > 0 && this.state.password.length > 0 && this.state.repeat_password.length > 0 && this.state.old_password.length > 0) {
            // if(this.state.password === this.state.repeat_password){
            return true;
            // }else{
            //     return false; 
            // }
        }
        // return this.state.email.length > 0 && (this.state.password === this.state.repeat_password) && this.state.password.length > 0 && this.state.repeat_password.length > 0 && this.state.lastName.length > 0 && this.state.firstName.length > 0 && this.state.phone.length > 0 && this.state.country.length > 0 && this.state.agree.length > 0;
    }
    profileFun(e) {
        e.preventDefault();
        e.stopPropagation();
        const { fullName, phoneNumber } = this.state;
        if (fullName !== '' && phoneNumber !== '') {
        } else {
            console.log('error');
        }
    }


    render() {
        let { currentUser } = this.props;
        let user = currentUser;
        if (currentUser === "") {
            return (<Redirect to='/login' />);
        }
        return (
            <div className="container main-container">
                {this.state.isLoading ?
                    <div className="loader_profile">
                        <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                    </div>
                    :
                    <Fragment>
                        <form encType="multipart/form-data">
                            <div className="row ">
                                <div className="col-md-4">
                                    <div className="row row-margin">
                                        <div className="profile padding-bottom-20">
                                            <label className="form-label">PROFILE</label>
                                            <hr />
                                            <br />
                                            <div className="profile-circle">
                                                <div class="profile_photo_div">
                                                    <label for="back_credit_card" class="profile_photo_label">
                                                        <i className="fa fa-camera" aria-hidden="true"></i>
                                                    </label>

                                                </div>
                                            </div>
                                            <div className="label-container padding-top-10">
                                                <label className="sub-label">{this.state.fullName}</label><br />
                                                <label className="form-labels profile-email">{localStorage.getItem('currentUserId')}</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-8">
                                    <div className="row row-margin">
                                        <div className="profile-details">
                                            <form className="profile-form" onClick={(e) => this.profileFun(e)}>

                                                <div className="form-row">
                                                    <div className="form-group col-md-12">
                                                        <label className="form-label">PERSONAL DETAILS</label>
                                                        <hr />
                                                    </div>
                                                    <div className="form-group col-md-6">
                                                        <label className="form-labels" >Full name</label>
                                                        <input type="text" className="form-input" placeholder="Enter full name" name="fullName" disabled value={(this.state.fullName) || ''} />
                                                        <input type="hidden" className="form-input" name="street" value={this.state.street || ''} />
                                                        <input type="hidden" className="form-input" name="city" value={this.state.city || ''} />
                                                        <input type="hidden" className="form-input" name="state" value={this.state.state || ''} />
                                                        <input type="hidden" className="form-input" name="country" value={this.state.country || ''} />
                                                        <input type="hidden" className="form-input" name="postalCode" value={this.state.country || ''} />
                                                    </div>
                                                    <div className="form-group col-md-6">
                                                        <label className="form-labels">Email address</label>
                                                        <input type="text" className="form-input" placeholder="Enter email address" name="email" value={this.state.email || ''} disabled />
                                                    </div>
                                                    <div className="form-group col-md-6">
                                                        <label className="form-labels">Phone no.</label>
                                                        <input type="text" className="form-input" required placeholder="Enter phone number" name="phoneNumber" value={this.state.phoneNumber || ''} disabled />
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div className="row row-margin">
                                        <div className="profile-details">
                                            <form className="profile-form" onSubmit={(e) => { this.reset(e) }}>
                                                <div className="form-row">
                                                    <div className="form-group col-md-12">
                                                        <label className="form-label">CHANGE PASSWORD</label>
                                                        <hr />
                                                    </div>
                                                    <FormGroup controlId="password" className="col-md-6">
                                                        <FormLabel>Password</FormLabel>
                                                        <InputGroup className="mb-3">
                                                            <FormControl
                                                                className="border-right-unset"
                                                                name="old_password"
                                                                value={this.state.old_password}
                                                                onChange={(e) => this.onChange(e)}
                                                                type={this.state.type}
                                                                placeholder="Enter current password"
                                                            />
                                                            <InputGroup.Append>
                                                                <InputGroup.Text onClick={this.handleClick}> <img src={require('../../images/icons/visibility.svg')} alt="#" /> </InputGroup.Text>
                                                            </InputGroup.Append>
                                                        </InputGroup>
                                                    </FormGroup>
                                                    <FormGroup controlId="password" className="col-md-6">
                                                        <FormLabel>New Password</FormLabel>

                                                        <InputGroup className="mb-3">
                                                            <FormControl
                                                                className="border-right-unset"
                                                                name="password"
                                                                value={this.state.password}
                                                                onChange={(e) => this.onChange(e)}
                                                                type={this.state.type2}
                                                                placeholder="Enter new password"
                                                            />
                                                            <InputGroup.Append>
                                                                <InputGroup.Text onClick={this.handleClick2}> <img src={require('../../images/icons/visibility.svg')} alt="#" /> </InputGroup.Text>
                                                            </InputGroup.Append>
                                                        </InputGroup>
                                                    </FormGroup>
                                                    <FormGroup controlId="password" className="col-md-6">
                                                        <FormLabel>Confirm Password</FormLabel>
                                                        <InputGroup className="mb-3">
                                                            <FormControl
                                                                className="border-right-unset"
                                                                name="repeat_password"
                                                                value={this.state.repeat_password}
                                                                onChange={(e) => this.onChange(e)}
                                                                type={this.state.type3}
                                                                placeholder="Enter confirm password"
                                                            />
                                                            <InputGroup.Append>
                                                                <InputGroup.Text onClick={this.handleClick3}> <img src={require('../../images/icons/visibility.svg')} alt="#" /> </InputGroup.Text>
                                                            </InputGroup.Append>
                                                        </InputGroup>
                                                    </FormGroup>
                                                    {(this.state.message !== '') &&
                                                        <>
                                                            <FormLabel className="text-left text-danger">{this.state.message}</FormLabel>
                                                        </>
                                                    }
                                                </div>
                                                <Button className="btn-signup-custom margin-bottom-20" disabled={!this.validatePasswordForm()} type="submit" > Reset </Button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </Fragment>
                }
            </div>
        )
    }
}
const mapStateToProps = (state) => {

    return {
        isLoading: state.AuthReducer.isLoading,
        accessToken: state.AuthReducer.accessToken,
        isAuthenticated: state.AuthReducer.isAuthenticated,
        currentUser: state.AuthReducer.currentUser,
        profilePic: state.AuthReducer.update_picture
    }
}
const mapDispatchToProps = (dispatch) => ({
    changePassword: (data) => dispatch(changePassword(data))
});
export default connect(mapStateToProps, mapDispatchToProps)(Profile)