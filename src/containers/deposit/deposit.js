import React, { Component } from 'react'
import { connect } from 'react-redux'
import '../deposit/deposit.css';
import { getRemoteBtcRate, getWireDetails, uploadFile } from '../../actions/authAction';
import { authorizationView } from '../authorization';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import $ from 'jquery';
import {
    Tab, Col, Nav, Form, Modal, Button, Alert
} from 'react-bootstrap';
import {
    Link
} from "react-router-dom";
import * as Scroll from 'react-scroll';
let scroll = Scroll.animateScroll;
class Deposit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bitcoin: '',
            euro: '',
            btcData: '',
            amerror: 'hide_error',
            error: 'hide_error',
            showModal: false,
            copy_address: '',
            img_link: '',
            bankDetails: '',
            depositAmount: '',
            depositCurrency: '',
            formData: '',
            photo_name: '',
        };
        this.handleArrowRate = this.handleArrowRate.bind(this);
        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }
    componentDidMount() {
        $(document).ready(function () {
            $(".mobile_menu_icon").removeClass("hide_mobile");
        });
        scroll.scrollToTop({ smooth: "linear", duration: 100 });
    }

    componentWillMount() {
        $(document).ready(function () {
            $(".mobile_menu_icon").removeClass("hide_mobile");
        });
        getRemoteBtcRate().then(res => {
            var str = res.message.qr_code;
            var img_res = str.replace("centralcoinsbank", "allure-trade");
            this.setState({ btcData: res.message.rate, copy_address: res.message.address, img_link: img_res });
        });
        getWireDetails().then(result => {
            this.setState({ bankDetails: result.message });
        });
    }

    handleBuyAmountInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value });
    }

    handleWireAmountInput(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value });
    }
    handleArrowRate() {

        var btcAmount = this.state.bitcoin;
        var usdAmount = this.state.euro;
        var data = this.state.btcData;
        if (btcAmount !== "" && (usdAmount === "" || usdAmount === undefined)) {
            var usd = (btcAmount * data).toFixed(8);
            this.setState({ euro: usd });
        }
        else if (btcAmount === "" && usdAmount !== "") {
            var btc = (usdAmount / data).toFixed(8);
            this.setState({ bitcoin: btc });
        }
        else if (btcAmount === "" && usdAmount === "") {
            this.setState({ amerror: 'show_error' });
            setTimeout(function () {
                this.setState({ amerror: 'hide_error' });

            }.bind(this), 4000);
        }
        else {
            this.setState({ error: 'show_error' });
            setTimeout(function () {
                this.setState({ error: 'hide_error' });
            }.bind(this), 4000);
        }
    }
    handleOpenModal() {
        this.setState({ showModal: true, euro: '', bitcoin: '' });
    }

    fileChangedHandler = (event) => {
        event.preventDefault();

        for (let i = 0; i < event.target.files.length; i++) {
            let file = event.target.files[i];
            let name = file.name;
            var formData = new FormData();
            formData.append('file', file, file.name);
            formData.append('t', 'proofSlip');
            formData.append('u', '0');
            formData.append('id', this.props.currentUser);
            formData.append('amount', this.state.depositAmount);

            for (var value of formData.values()) {
            }
            this.setState({ formData: formData, photo_name: name });
        }
    }
    saveWireDetail(e) {
        e.preventDefault();
        this.props.uploadFile(this.state.formData).then(res => {
        });
    }
    saveCreditCardData() {

    }

    handleCloseModal() {
        this.setState({ showModal: false });
    }

    authorizationCheck = (pathname, viewRight) => {
        let { currentUser } = this.props;
        let listCheck = authorizationView(currentUser, pathname, viewRight);
        return listCheck;
    }
    render() {
        return (
            <div>
                <div className="container">
                    <div className="row margin-top-50 margin-bottom-100 account-deposit-section">
                        <div className="col-md-8 offset-md-2">
                            <div className="row padding-top-20 padding-bottom-50">
                                <Tab.Container id="tabs" defaultActiveKey="bitcoin">
                                    <Col sm={12} className="text-center">
                                        <Nav variant="tabs" className="nav-fill row">
                                            <Nav.Link eventKey="bitcoin" className="col-md-4"> Via Bitcoin </Nav.Link>
                                            <Nav.Link eventKey="credit_card" className="col-md-4">Via Credit Card</Nav.Link>
                                            <Nav.Link eventKey="wire_transfer" className="col-md-4">Via Wire Transfer</Nav.Link>
                                        </Nav>
                                    </Col>
                                    <Col sm={12} className="padding-0" id="nav-tabContent">
                                        <Tab.Content className="padding-top-50">
                                            <Tab.Pane eventKey="bitcoin" >
                                                <div>
                                                    <Form className="row">
                                                        <Alert variant="danger" className={'col-md-12 ' + this.state.amerror}>
                                                            <strong>Error!</strong> One Input Must Be Not Empty For Convertion.
                                                </Alert>
                                                        <Alert variant="danger" className={'col-md-12 ' + this.state.error}>
                                                            <strong>Error!</strong> One Input Must Be Empty For Convertion.
                                                </Alert>
                                                        <div className=" col-md-5">
                                                            <label>BITCOIN AMOUNT</label>
                                                            <div className="form-group">
                                                                <input className="bitcoin-input form-control" type="number" name="bitcoin" value={this.state.bitcoin ? this.state.bitcoin : ''} onChange={(e) => this.handleBuyAmountInput(e)} />
                                                            </div></div>
                                                        <div className=" col-md-2 padding-top-35 text-center">
                                                            <div className="form-group">
                                                                <Link className="arrow-button" to="#" role="button" onClick={() => this.handleArrowRate()} > <i className="fa fa-exchange"></i> </Link>
                                                            </div></div>
                                                        <div className=" col-md-5">
                                                            <label>EUR AMOUNT</label>
                                                            <div className="form-group ">
                                                                <input className="euro-input form-control" type="number" name="euro" value={(this.state.euro !== '') ? this.state.euro : ''} onChange={(e) => this.handleBuyAmountInput(e)} />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12 padding-top-20">
                                                            <Link to="#" className="btn btn-deposit-signup-custom" role="button" onClick={this.handleOpenModal} data-toggle="modal" data-target="#myModal-list"><b>SHOW ADDRESS</b></Link>
                                                        </div>
                                                    </Form>
                                                </div>
                                            </Tab.Pane>
                                            <Tab.Pane eventKey="credit_card">
                                                <div>
                                                    <Form className="row">
                                                        <div className=" col-md-6">
                                                            <label>AMOUNT</label>
                                                            <div className="form-group">
                                                                <input className="credit-input form-control" type="number" name="bitcoin" value={this.state.bitcoin ? this.state.bitcoin : ''} onChange={(e) => this.handleBuyAmountInput(e)} />
                                                            </div>
                                                        </div>
                                                        <div className="col-md-12 padding-top-20">
                                                            <a href="https://safecurrency.com/en#home" target="_blank" className="btn btn-deposit-signup-custom" role="button" ><b>MAKE A DEPOSIT</b></a>
                                                        </div>
                                                    </Form>
                                                </div>
                                            </Tab.Pane>
                                            <Tab.Pane eventKey="wire_transfer">
                                                <div>
                                                    <Form onSubmit={(e) => this.saveWireDetail(e)} encType="multipart/form-data">

                                                        <div className="row wire-details-section">
                                                            <div className="col-md-6 padding-b-15">
                                                                <label>CURRENCY</label>
                                                                <select defaultValue={(this.state.selectValue) ? this.state.selectValue : 'USD'} className="form-control" onChange={(e) => this.handleWireAmountInput(e)}>
                                                                    <option value="EUR">EUR</option>
                                                                </select>
                                                            </div>
                                                            <div className="col-md-6">
                                                                <label>ENTER AMOUNT</label>
                                                                <input type="number" name="depositAmount" placeholder="" onChange={(e) => this.handleWireAmountInput(e)} value={(this.state.depositAmount != '') ? this.state.bankDetails.depositAmount : ''} className="form-control" required />
                                                            </div>

                                                            <div className="col-md-12 tab-head-sm padding-t-15 padding-top-20"><h2>Bank Details </h2></div>
                                                            <hr />
                                                            <div className="col-md-6">
                                                                <label>BENEFICIARY NAME</label>
                                                                <input type="text" name="beneficiary_name" placeholder="" disabled value={(this.state.bankDetails != '') ? this.state.bankDetails.beneficiary_name : ''} required className="form-control" />
                                                            </div>
                                                            <div className="col-md-6">
                                                                <label>BENEFICIARY ADDRESS</label>
                                                                <input type="text" name="beneficiary_address" placeholder="" disabled value={(this.state.bankDetails != '') ? this.state.bankDetails.beneficiary_address : ''} required className="form-control" />
                                                            </div>
                                                            <div className="col-md-6">
                                                                <label>BANK NAME</label>
                                                                <input type="text" name="bank_name" placeholder="" disabled value={(this.state.bankDetails != '') ? this.state.bankDetails.bank_name : ''} required className="form-control" />
                                                            </div>
                                                            <div className="col-md-6">
                                                                <label>BANK ADDRESS</label>
                                                                <input type="text" name="bank_address" placeholder="" disabled required className="form-control" />
                                                            </div>
                                                            <div className="col-md-6">
                                                                <label>SWIFT (BIC)</label>
                                                                <input type="text" name="swift" placeholder="" disabled value={(this.state.bankDetails != '') ? this.state.bankDetails.SWIFT : ''} required className="form-control" />
                                                            </div>
                                                            <div className="col-md-6">
                                                                <label>IBAN</label>
                                                                <input type="text" name="iban" placeholder="" disabled value={(this.state.bankDetails != '') ? this.state.bankDetails.IBAN : ''} required className="form-control" />
                                                            </div>

                                                            <div className="col-md-12">
                                                                <p className="upload_file_p">Please upload a picture of the receipt from your bank once you transfer funds</p>
                                                                <div class="deposit-file">
                                                                    <input id="deposit_photo_id" name="deposit_photo_id" type="file" class="deposit-file-input" onChange={this.fileChangedHandler} required />
                                                                    {this.state.photo_name ?
                                                                        <div class="deposit-file-div"><img src={require('../../images/check.svg')} alt="#" />{this.state.photo_name}</div>
                                                                        :
                                                                        <label for="deposit_photo_id" class="deposit-file-label">No file chosen</label>
                                                                    }
                                                                </div>
                                                            </div>

                                                            <div className="col-md-12 padding-top-20">
                                                                <button type="submit" className="btn  btn-deposit-signup-custom" >MAKE A DEPOSIT</button>
                                                            </div>
                                                        </div>
                                                    </Form>
                                                </div>

                                            </Tab.Pane>
                                        </Tab.Content>
                                    </Col>
                                </Tab.Container>
                                <Modal show={this.state.showModal} onHide={this.handleCloseModal}>
                                    <Modal.Header closeButton><h4>COPY ADDRESS</h4>
                                    </Modal.Header>
                                    <Modal.Body>
                                        <div className="row">
                                            <div className="col-md-8 offset-md-2" >
                                                <Form>
                                                    <div className="text-center">
                                                        <img src={this.state.img_link ? this.state.img_link : '/'} className="img-fluid" />
                                                    </div>
                                                    <div className="form-group">
                                                        <label>COPY ADDRESS</label>

                                                        <input className="bitcoin-input form-control" type="text" name="copy_address" value={this.state.copy_address ? this.state.copy_address : ''} />
                                                        <CopyToClipboard className="margin-top-20 btn btn-signup-custom" text={this.state.copy_address ? this.state.copy_address : ''}>
                                                            <button>COPY</button>
                                                        </CopyToClipboard>
                                                    </div>
                                                </Form>
                                            </div>
                                        </div>
                                    </Modal.Body>
                                </Modal>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isLoading: state.AuthReducer.isLoading,
        accessToken: state.AuthReducer.accessToken,
        isAuthenticated: state.AuthReducer.isAuthenticated,
        currentUser: state.AuthReducer.currentUser,
        isbtcRate: state.AuthReducer.isbtcRate,
        isTab: state.AuthReducer.isTab,
    }
}


export default connect(mapStateToProps, { getRemoteBtcRate, getWireDetails, uploadFile }
)(Deposit)
