import React, { Component } from 'react';
import { getCountry } from '../../actions/authAction';
import $ from 'jquery';
import ReactLoading from "react-loading";

 class CountryBlocked extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blockloader:true,
    }
  }

  componentWillMount(){
    $(document).ready(function () {
      $(".header2").addClass("hide-blocked-page");
      $(".header2_mobile").addClass("hide-blocked-page");
      $(".footer").addClass("hide-blocked-page");
    });

    getCountry().then(res => {
      if(res.props == undefined){
          this.props.history.push("/");
          this.setState({blockloader:false});
  }else{
    this.setState({blockloader:false});
  }
  })
  }

  render() {
    return (
    <div className="blocked">
     {(this.state.blockloader) &&
          <>
            <div className="blockloader">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
          </>}
       <div className="container">
        <div className="row">
          <div className="col-md-6 block-border">
          <div className="block-logo">
            <img src={require("../../images/Logo.png")} />
          </div>
          </div>
          <div className="col-md-6">
          <div className="block-content">
            <h1>Your country has been blocked.</h1>
            <p>If you believe this is a mistake, please contact us at</p>
            <p><a href="mailto:support@btcwe.io">support@btcwe.io</a></p>
            <p>We apologize for the inconvenience.</p>
            <p className="block-footer1">Best Regards,</p>
            <p className="block-footer">BTCWE Team</p>
          </div>
          </div>
        </div>
      </div>
    </div>
    )
}
}
export default CountryBlocked