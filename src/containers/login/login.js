
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from "react-router-dom";
import { loginRequest, updateRedirectRequest, tokenVerify } from '../../actions/authAction';
import { Helmet } from "react-helmet";
import { Button, FormGroup, FormControl, FormLabel, Card, Form } from "react-bootstrap";
import "../login/login.css";
import * as Scroll from 'react-scroll';
import $ from 'jquery';
import cookie from 'react-cookies';
import { Base64 } from 'js-base64';
import {  getCountry } from '../../actions/authAction';
import ReactLoading from "react-loading";


let scroll = Scroll.animateScroll;
export class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectState: false,
      email: "",
      password: "",
      type: 'password',
      score: 'null',
      urlparams: [],
      checked: '',
      blockloader:true,
    }
    this.showHide = this.showHide.bind(this);
  }
  componentWillMount() {
    $(document).ready(function () {
      $(".mobile_menu_icon").addClass("hide_mobile");
    });
    getCountry().then(res => {
      // this.setState({blockloader:true});
      if(res.props != undefined){
      if(res.props.to){
          this.props.history.push("/countryblocked");
          this.setState({blockloader:false});
      }
  }else{
    this.setState({blockloader:false});
  }
  })
    if (this.props.match.params.token) {
      this.props.tokenVerify({ token: this.props.match.params.token });
    }
    if (cookie.load('userId') != undefined && cookie.load('password') != undefined) {
      let newpass = Base64.decode(cookie.load('password'));
      this.setState({ email: cookie.load('userId'), password: newpass, checked: true })
    }
  }
  showHide(e) {
    this.setState({
      type: this.state.type === 'password' ? 'text' : 'password'
    })
  }
  validateForm = () => {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }
  a
  login = (e) => {
    e.preventDefault();
    let data = {
      email: (this.state.email !== '') ? this.state.email : "test3@gmail.com",
      password: (this.state.password !== '') ? this.state.password : "test"
    }
    this.props.loginRequest(data);
    let pass = Base64.encode(this.state.password);
    if (this.state.checked == true) {
      cookie.save('userId', this.state.email, { path: '/' })
      cookie.save('password', pass, { path: '/' })
    }
    if (this.state.checked == false && cookie.load('userId') == this.state.email && cookie.load('password') == pass){
      cookie.remove('userId');
      cookie.remove('password')
    }
  }

  componentDidMount() {
    $(document).ready(function () {
      $(".mobile_menu_icon").addClass("hide_mobile");
    });
    scroll.scrollToTop({ smooth: "linear", duration: 100 });
    this.props.updateRedirectRequest();
  }

  componentWillReceiveProps(nxtProps) {
    if (nxtProps.isAuthenticated) {
      nxtProps.history.push('/dashboard');
    }
  }
  handleCheck(e) {
    this.setState({ checked: e.target.checked });
  }

  render() {
    return (
      <div className=" login-section-cointainer">
        {(this.state.blockloader) &&
          <>
            <div className="blockloader">
              <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px", "top": "50%" }}>
                <ReactLoading color={'#3445CF'} type="spinningBubbles" />
              </div>
            </div>
          </>}
        <div className="container padding-top-bottom-50">
          <Helmet>
            <style>
              {".header2 .container{ max-width:unset; margin:unset;padding-left:6%;} #root{overflow:hidden;}.login-section-cointainer .container { transform: skew(0deg, 2deg);}.login-section-cointainer { transform: skew(0deg, -2deg); background:#f9fafe; background-repeat: no-repeat; border-radius: 0px 104px 0px 104px;} ul.ml-auto{display:none !important;}  .footer{ display:none !important;}"}</style>
          </Helmet>
          <div className="row margin-top-50">
            <div className=" col-md-12 col-lg-6 login-section">
              <div className="col-md-9 offset-md-1">
                <h1 className="login-header-text text-left margin-bottom-50">Log in to your account</h1>
                <div className="first_signup haveaccount">
                  <span>Don’t have an account yet? <Link to="/register" className="btn bck-white-text" role="button"> Sign Up</Link></span>
                </div>

                <Card >
                  <Card.Body>
                    <form onSubmit={(e) => { this.login(e) }}>
                      <FormGroup controlId="email">
                        <FormLabel>Email</FormLabel>
                        <FormControl
                          autoFocus
                          name="email"
                          type="email"
                          placeholder="Enter your email"
                          value={this.state.email}
                          onChange={this.handleChange}
                        />
                      </FormGroup>
                      <div className="password_input">
                        <FormGroup controlId="password">
                          <FormLabel>Password <span className="text-right forget_text"><Link className="forgot-link float-right" to="/forgot-password">Forgot Password? </Link></span></FormLabel>

                          <FormControl
                            name="password"
                            value={this.state.password}
                            onChange={this.handleChange}
                            type={this.state.type}
                            placeholder="Enter your password"
                          />
                          <span className="password__show" onClick={this.showHide}> <img src='/static/media/visibility.dc8a7f67.svg' />  </span>
                        </FormGroup>
                      </div>

                      <FormGroup controlId="formBasicCheckbox" className="remember_text margin-top-30 margin-bottom-30">
                        <Form.Check
                          type="checkbox"
                          label=" Remember Me"
                          onChange={(e) => this.handleCheck(e)}
                          defaultChecked={this.state.checked}
                        />
                      </FormGroup>
                      <div className="login_button">
                        <Button className="btn-signup-custom login-button"
                          disabled={!this.validateForm()}
                          type="submit"
                        >
                          Log In
                    </Button>
                      </div>
                    </form>

                  </Card.Body>
                </Card>
              </div>
            </div>
            <div className="col-md-12 col-lg-6 message-section">
              <div className="col-md-12 col-lg-12 message-section-2">
                <div className="text-rotate">
                  <p className="signup-text">Don’t have an account yet?</p>
                  <Link to="/register" className="btn bck-white-text" role="button">Sign Up</Link>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-3 col-lg-4"></div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.AuthReducer.isAuthenticated,
    accessToken: state.AuthReducer.accessToken,
  };
};

const mapDispatchToProps = (dispatch) => ({
  loginRequest: (data) => dispatch(loginRequest(data)),
  updateRedirectRequest: () => dispatch(updateRedirectRequest()),
  tokenVerify: (data) => dispatch(tokenVerify(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Login)