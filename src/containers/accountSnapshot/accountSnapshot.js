import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Helmet } from "react-helmet";
import {
  Link
} from "react-router-dom";
import { Button, FormGroup, FormControl, FormLabel, Card } from "react-bootstrap";
import './accountSnapshot.css';
import { accountSnapshot } from '../../actions/authAction';
import ReactLoading from "react-loading";
import * as Scroll from 'react-scroll';
import DataTable from 'react-data-table-component'



import {
  Redirect
} from "react-router-dom";
var moment = require('moment');

var value_price = parseInt(0);
let scroll = Scroll.animateScroll;
class AccountSnapshot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      account: '',
      loader: true,
    }
  }
  componentDidMount() {
    scroll.scrollToTop({ smooth: "linear", duration: 100 });
  }
  componentWillMount() {
    const { currentUser } = this.props;
    let data = {
      'email': currentUser
    }
    accountSnapshot(data).then(res => {
      this.setState({ account: res }, () => {
        this.setState({ loader: false })
      })
    });
  }
  render() {
    let no_record = [];
    if (this.state.account === undefined || this.state.account.length == 0) {
      no_record = 'No record found !!';
    } else {
      for (let i = 0; i < this.state.account.length; i++) {
        no_record.push(this.state.account[i].object);
      }
    }
    const columns = [
      {
        name: 'DATE',
        selector: 'created_at',
        cell: row => moment(row.created_at).format('DD-MM-YYYY'),
      },
      {
        name: 'TYPE',
        selector: 'type'
      },
      {
        name: 'CURRENCY',
        selector: 'currency'
      },
      {
        name: 'AMOUNT',
        selector: 'amount',
        cell: row => <>{<div>{row.amount} €</div>}</>
      },
      {
        name: 'STATUS',
        selector: 'status'
      }
    ];

    let { isAuthenticated } = this.props;
    if (!isAuthenticated) {
      return (<Redirect to='/login' />);
    }
    return (
      <div>
        <Helmet>
          <style>{'body { background-color: #eceff54d; } .container{ padding:0px;} .navbar{padding-right: 0px; padding-left:0px;}'}</style>
        </Helmet>

        <div className="container">
          <div className="deposit_withdraw_btn">
            <Link className="btn btn-deposit deposit_margin" to="/withdraw-request"> Withdraw  </Link>

            <Link className="btn btn-deposit deposit_margin" to="/deposit"> Deposit  </Link>
          </div>
        </div>
        <div className="container">
          <div className="row  dashboard-section padding-top-50 text-center">
            <div className="col-md-12">
              <h2 className="text-center"><img alt="#" src={require("../../images/line.png")} />My Cashier</h2>
              <div className="table-responsive">
                {(this.state.loader) ?
                  <div className="row margin-top-20 dashboard-section padding-top-10 text-center">
                    <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px" }}>
                      <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                    </div>
                  </div>
                  :
                  <DataTable className="scrollbar_all_trades" columns={columns} data={no_record} />
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.AuthReducer.isLoading,
    accessToken: state.AuthReducer.accessToken,
    isAuthenticated: state.AuthReducer.isAuthenticated,
    currentUser: state.AuthReducer.currentUser,
    isbtcRate: state.AuthReducer.isbtcRate,
    isTab: state.AuthReducer.isTab,
    balance: state.AuthReducer.balance
  }
}


export default connect(mapStateToProps, { accountSnapshot }
)(AccountSnapshot)
