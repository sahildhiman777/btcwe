import callApi from '../utils/apiCaller';
import history from '../history';
import { ToastTopEndSuccessFire, ToastTopEndWarningFire, ToastTopEndErrorFire } from '../toast/toast';

// Export Constants
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOADING = 'LOADING';
export const USER_LOGGEDOUT = 'USER_LOGGEDOUT';
export const SET_USER = 'SET_USER';
export const IS_REDIRECT = 'IS_REDIRECT';
export const PROFILE = 'PROFILE';
export const IS_TAB = 'IS_TAB';
export const BTC_RATE = 'BTC_RATE';
export const IS_SIGN = 'IS_SIGN';
export const IS_DATA = 'IS_DATA';
export const CONTACT_US = 'CONTACT_US';
export const BUYBTC_MIN_AMOUNT = 200;
export const hydratePaymentBank = "hydratePaymentBank";
export const BUYBTC_UPDATE_BTC = 'BUYBTC_UPDATE_BTC';
export const BUYBTC_UPDATE_EUR = 'BUYBTC_UPDATE_EUR';
export const BUYBTC_UPDATE_TRADE_REF = 'BUYBTC_UPDATE_TRADE_REF';
export const WITHDRAW_REQUEST = 'WITHDRAW_REQUEST';


// Buy/Sell BTC Flow Common Actions
export const updateCurrency = (flow, type, payload) => ({ type: flow + '_UPDATE_' + type.toUpperCase(), payload });
// export const updateCurrency = (flow, type, payload) => ({ type: `${flow}_UPDATE_${type.toUpperCase()}`, payload });
export const resetCurrencies = (flow) => ({ type: `${flow}_RESET_CURRENCIES` });
export const updateTradeRef = (flow, payload) => ({ type: `${flow}_UPDATE_TRADE_REF`, payload });

// Export Actions
export function loggedIn(accessToken, userData) {
    return {
        type: LOGIN_SUCCESS,
        isAuthenticated: true,
        accessToken: accessToken,
        payload: userData
    };
}

export function loaderFun(check) {
    return {
        type: LOADING,
        isLoading: check,
    };
}

export function profileFun(userData) {
    return {
        type: PROFILE,
        payload: userData
    };
}

export function logoutFunction() {
    return {
        type: USER_LOGGEDOUT
    };
}

export function setUser(userData) {
    return {
        type: SET_USER,
        payload: userData
    };
}


export function isRedirect(data) {
    return {
        type: IS_REDIRECT,
        payload: data
    };
}

export function isdata(data) {
    return {
        type: IS_DATA,
        payload: data
    };
}

export function isSign() {
    return {
        type: IS_SIGN
    };
}

export function hydratebank(data) {
    let banks = [];
    data.map(bank => {
        const {
            IBAN,
            bank_name,
            account_number
        } = bank.object;

        banks.push({
            iban: IBAN,
            name: bank_name,
            number: account_number,
            ...bank.object
        });
        return banks;
    }
    );
    return new Promise((resolve, reject) => {
        return resolve(banks);
    });
}
export function logoutFun() {
    return (dispatch) => {
        localStorage.removeItem('accessToken');
        localStorage.removeItem('currentUserId');
        localStorage.removeItem('email');
        localStorage.removeItem('user');
        localStorage.clear();
        ToastTopEndSuccessFire('SignOut successfully');
        dispatch(logoutFunction());
        history.push('/login');
        dispatch(isRedirect('/login'));
    }
}

export function isTab(data) {
    return {
        type: IS_TAB,
        payload: data
    };
}

export function contactUs(data) {
    return {
        type: CONTACT_US,
        payload: data
    };
}

export function withdrawData(data) {
    return {
        type: WITHDRAW_REQUEST,
        payload: data
    };
}

export function handleOnclickInput(data) {
    return (dispatch) => {
        dispatch(isTab(data))
    }
}
export function rate(response) {
    return {
        type: BTC_RATE,
        payload: response
    };
}
export function getbtcrate() {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('public/get-btc-rate', 'GET').then(res => {
            dispatch(rate(res));
        }).catch(err => {
            dispatch(loaderFun(false));
            ToastTopEndWarningFire(err.message);
        });
    }
}

export function getuserdetails(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/details?email=' + creds.email, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res.message);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}
export function getComplianceImages(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/compliance_image?email=' + creds.email, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res.message);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}

export function getrate(creds) {
    return new Promise((resolve, reject) => {
        return callApi('public/get-currency-rate?email=' + creds.email, 'GET').then(res => {
            if (res.status === "error") {
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })

}

export function getRemoteBtcRate(creds) {
    return new Promise((resolve, reject) => {
        return callApi('public/get_btc_remote_value', 'GET').then(res => {
            if (res.status === "error") {
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })

}

export function getlogs(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/dashboard_logs?email=' + creds.email, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })

}

export function getpending(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/dashboard_pending?email=' + creds.email, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res.results);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })

}

export function getWireDetails(creds) {
    return new Promise((resolve, reject) => {
        return callApi('public/get_wire_details', 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })

}

export function getcompleted(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/dashboard_completed?email=' + creds.email, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })

}

export function getfailed(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/dashboard_failed?email=' + creds.email, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })

}

export function buyinitiatetrade(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/buy_initiate_trade', 'POST', creds).then(res => {
            resolve(res);
        }).catch(err => {
            reject(err);
        });
    })

}

export function accountSnapshot(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/account_snapshot?email=' + creds.email, 'GET').then(res => {
            resolve(res.message);
        }).catch(err => {
            reject(err);
        });
    })

}

export function withdrawRequest(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/make_withdrawal_request', 'POST', creds).then(res => {
            if (res.status === 'error') {
                resolve(res);
                ToastTopEndErrorFire(res.massage);
            } else {
                resolve(res);
                ToastTopEndSuccessFire(res.massage);
            }
        }).catch(err => {
            reject(err);
            ToastTopEndErrorFire(err.massage);
        });
    })

}

export function createMarket(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('user/create_market_order', 'POST', creds).then(res => {

            if (res.status === 'error') {
                ToastTopEndErrorFire(res.message);
            } else {
                ToastTopEndSuccessFire(res.message);
            }
        }).catch(err => {
            ToastTopEndErrorFire(err.message);
        });
    };
}

export function updateRedirectRequest() {
    return (dispatch) => {
        dispatch(isRedirect(''))
    }
};

export function loginRequest(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('login', 'POST', {
            email: creds.email,
            password: creds.password
        }).then(res => {
            if (res.status === 'error') {
                ToastTopEndErrorFire(res.message);
            } else {
                localStorage.setItem('accessToken', res.token);
                localStorage.setItem('currentUserId', res.userInfo.email);
                localStorage.setItem('name', res.userInfo.full_name);
                localStorage.setItem('spread', res.userInfo.spread);
                dispatch(loggedIn(res.token, res.userInfo));
                ToastTopEndSuccessFire('Signed in successfully');
            }
        }).catch(err => {
            ToastTopEndErrorFire(err.message);
        });
    };
}

export function contactUsRequest(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('user/contact_us', 'POST', creds).then(res => {
            let data;
            if (res.status === 'error') {
                data = {
                    'message': 'Email not Sent.',
                    'sub-message': 'Please try again later!'
                }
            } else {
                data = {
                    'message': 'We’ve got your message!',
                    'sub-message': 'You will get a response in up to 24h.'
                }
            }
            dispatch(contactUs(data));
        }).catch(err => {
            dispatch(loaderFun(false));
            ToastTopEndErrorFire(err.message);
        });
    };
}

export function forgotRequest(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('user/forgot_password', 'POST', creds).then(res => {
            if (res.status === 'error') {
                ToastTopEndErrorFire('Email Not Sent. Please try again later!');
            } else {
                dispatch(loaderFun(false));
                history.push('/reset-link');
                dispatch(isRedirect('/reset-link'));
            }

        }).catch(err => {
            dispatch(loaderFun(false));
            ToastTopEndErrorFire(err.message);
        });
    };
}

export function resetRequest(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('user/reset_password', 'POST', creds).then(res => {
            if (res.status === 'error') {
                ToastTopEndErrorFire('Email Not Sent. Please try again later!');
            } else {
                ToastTopEndSuccessFire('Email Sent Successfully!');
            }
            dispatch(isRedirect('/login'));
        }).catch(err => {
            dispatch(loaderFun(false));
            ToastTopEndErrorFire(err.message);
        });
    };
}

export function registerRequest(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('register', 'POST', creds).then((res) => {
            dispatch(loaderFun(false));
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                dispatch(isRedirect('/dashboard'))
            } else {
                ToastTopEndSuccessFire(res.message);
                localStorage.setItem('accessToken', res.token);
                localStorage.setItem('currentUserId', res.userInfo.email);
                localStorage.setItem('name', res.userInfo.full_name);
                dispatch(loggedIn(res.token, res.userInfo));
                dispatch(isRedirect('/dashboard'))
            }

        }).catch(err => {
            dispatch(loaderFun(false));
            ToastTopEndWarningFire(err.message);
            dispatch(isRedirect('/dashboard'))
        });
    };
}
export function tokenVerify(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('public/tokenverify', 'POST', creds).then(res => {
            if (res.status === 'error') {
                ToastTopEndErrorFire(res.message);
            } else {
                localStorage.setItem('accessToken', res.token);
                localStorage.setItem('currentUserId', res.userInfo.email);
                localStorage.setItem('name', res.userInfo.full_name);
                localStorage.setItem('spread', res.userInfo.spread);
                dispatch(loggedIn(res.token, res.userInfo));
                ToastTopEndSuccessFire('Signed in successfully');
            }
        }).catch(err => {
            ToastTopEndErrorFire(err.message);
        });
    };
}

export function verifyEmail(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('verifyemail', 'POST', {
            token: creds.token
        }).then((res) => {
            dispatch(loaderFun(false));
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
            } else {
                ToastTopEndSuccessFire(res);
            }
            dispatch(isRedirect('/login'))
        }).catch(err => {
            dispatch(loaderFun(false));
            ToastTopEndWarningFire(err.message);
            dispatch(isRedirect('/login'))
        });
    };
}

export function EditProfileRequest(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('user/profile', 'POST', {
            firstName: creds.firstName,
            lastName: creds.lastName,
            email: creds.email,
            phoneCode: creds.phoneCode,
            phoneNumber: creds.phoneNumber,
            street: creds.street,
            city: creds.city,
            state: creds.state,
            country: creds.country,
            postalCode: creds.postalCode
        }).then(() => {
            dispatch(loaderFun(false));
            ToastTopEndSuccessFire('Successfully Edit Personal Details!');
            dispatch(isRedirect('/profile'))
        }).catch(err => {
            dispatch(loaderFun(false));
            ToastTopEndWarningFire(err.message);
            dispatch(isRedirect('/profile'))
        });
    };
}

export function uploadFile(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('user/upload_file', 'POST', creds, true).then(res => {
            if (res.status === 'error') {
                ToastTopEndErrorFire(res.message);
            } else {
                ToastTopEndSuccessFire(res.message);
            }
        }).catch(err => {
            ToastTopEndErrorFire('Something Went Wrong!');
        });
    };
}
export function verifyToken(creds) {
    return new Promise((resolve, reject) => {
        return callApi('public/verify_token?token=' + creds.token, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}

export function getPaymentbanks(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/get_bank_account?email=' + creds.email, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}

export function get_buy_sell(creds) {
    return new Promise((resolve, reject) => {
        return callApi('public/get_buy_sell?email=' + creds.email + '&id=' + creds.id, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res.message);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}

export function getAssets(creds) {
    return new Promise((resolve, reject) => {
        return callApi('public/get_assets?email=' + creds.email + '&assets_type=' + creds.asset_type, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}

export function get_fav_assets(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/get_fav_assets?email=' + creds.email, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}

export function get_all_fav_assets() {
    return new Promise((resolve, reject) => {
        return callApi('public/get_all_fav_assets', 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}
export function add_fav_assets(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/add_fav_assets?email=' + creds.email + '&asset_id=' + creds.asset_id, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
                ToastTopEndSuccessFire(res.message);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}
export function open_trades_by_username(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/open_trades_by_username?email=' + creds.email, 'GET').then(res => {

            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res.message);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    });
}

export function close_trades(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/close_trades', 'POST', { email: creds.email }).then(res => {

            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                ToastTopEndSuccessFire(res.message);
                resolve(res.message);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    });
}
export function oneTrade(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/close_one_trade', 'POST', {
            username: creds.username,
            trade_id: creds.trade_id
        }).then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                ToastTopEndSuccessFire(res.message);
                resolve(res.message);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    });
}

export function close_trades_by_username(creds) {
    return new Promise((resolve, reject) => {
        return callApi('user/closed_trades_by_username?email=' + creds.email, 'GET').then(res => {

            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res.message);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    });
}

export function getEtfAssets() {
    return new Promise((resolve, reject) => {
        return callApi('public/get_etfassets', 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}
export function getComAssets() {
    return new Promise((resolve, reject) => {
        return callApi('public/get_comassets', 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}
export function getGraphData(creds) {
    return new Promise((resolve, reject) => {
        return callApi('public/graph_data?asset_id=' + creds.asset_id + '&interval=' + creds.interval + '&days=' + creds.days, 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}
export function getFiatAssets() {
    return new Promise((resolve, reject) => {
        return callApi('public/get_fiatassets', 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}

export function bankAccount(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('user/bank_account', 'POST', {
            email: creds.email,
            iban: creds.iban,
            bank_name: creds.bank_name,
            bank_address: creds.bank_address,
            swift: creds.swift,
            account_number: creds.account_number
        }).then(res => {
            if (res.status === 'error') {
                ToastTopEndErrorFire(res.message);

            } else {
                ToastTopEndSuccessFire(res.message);
            }
            dispatch(isRedirect('/paymentmethod'))
        }).catch(err => {
            ToastTopEndErrorFire(err.message);
        });
    };
}

export function updateNewPassword(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('public/changeNewPassword', 'POST', creds).then(res => {
            if (res.status === 'error') {
                ToastTopEndErrorFire(res.message);

            } else {
                history.push('/reset-pass');
                dispatch(isRedirect('/reset-pass'))
            }
        }).catch(err => {
            ToastTopEndErrorFire(err.message);
        });
    };
}

export function changePassword(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('public/update_new_password', 'POST', creds).then(res => {
            if (res.status === 'error') {
                ToastTopEndErrorFire(res.message);
            } else {
                ToastTopEndSuccessFire(res.message);
            }
            dispatch(isRedirect('/profile'))
        }).catch(err => {
            ToastTopEndErrorFire(err.message);
        });
    };
}

export function checkIfUserIsLoggedIn() {
    let token = localStorage.getItem('accessToken');
    return (dispatch) => {
        if (token && token !== '') {
            dispatch(loggedIn(token));
        }
        return;
    }
}


export function complianceRequest(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('user/compliance', 'POST', {
            email: creds.email,
            firstName: creds.first_name,
            lastName: creds.last_name,
            phoneCode: creds.phoneCode,
            mobile: creds.mobile,
            address: creds.address,
            city: creds.city,
            state: creds.state,
            postal: creds.postal,
            country: creds.country,
        }).then(() => {
            dispatch(loaderFun(false));
            ToastTopEndSuccessFire('Successfully Edit Personal Details!');
        }).catch(err => {
            dispatch(loaderFun(false));
            ToastTopEndWarningFire(err.message);
        });
    };
}


export function complianceUploadFile(creds) {
    return (dispatch) => {
        dispatch(loaderFun(true));
        return callApi('user/compliance_upload_file', 'POST', creds, true).then(res => {

            if (res.status === 'error') {
                ToastTopEndErrorFire(res.message);
            } else {
                ToastTopEndSuccessFire(res.message);
            }
        }).catch(err => {
            ToastTopEndErrorFire('Something Went Wrong!');
        });
    };
}

export function getAssetsHistory() {
    return new Promise((resolve, reject) => {
        return callApi('public/get_assets_history', 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}

export function getCountry() {
    return new Promise((resolve, reject) => {
        return callApi('public/get_country', 'GET').then(res => {
            if (res.status === "error") {
                ToastTopEndErrorFire(res.message);
                reject(res.message);
            } else {
                resolve(res);
            }
        }).catch(err => {
            ToastTopEndWarningFire(err.message);
            reject(err);
        });
    })
}