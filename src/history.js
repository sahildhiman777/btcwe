// import { createBrowserHistory } from 'history';
// let history = createBrowserHistory();
// export default history;
import store from './store';

import { syncHistoryWithStore } from 'react-router-redux'
import { createBrowserHistory } from 'history';
var browserHistory = createBrowserHistory();

var history = syncHistoryWithStore(browserHistory, store);
export default history;