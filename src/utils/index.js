import React from 'react';
import ButterToast from 'butter-toast';
// import countryList from 'countries-list';
// import { PASS_MIN_CHAR } from 'config';

export const MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

export const EMAIL_REGEX = RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
export const PASS_REGEX = RegExp(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{2,}$/);

export const EMAIL_ERROR_MSG = 'Please enter a valid email address';
// export const PASS_ERROR_MSG = `Password must be at least ${PASS_MIN_CHAR} characters long`;

export const Toast = (text) => {
    ButterToast.raise({
        timeout: 10000,
        content: ({ dismiss }) => {
            return (
                <div>
                    <span>{text}</span>
                    <button onClick={dismiss}>×</button>
                </div>
            );
        }
    });
}

// export const countries = Object.values(countryList.countries);
// export const countryNames = countries.map(v => v.name);
// export const countryFlagIcons = countries.map(v => v.emoji);

// const codes = countries.map(v => {
//     const phoneCode = `+${v.phone}`;

//     if (phoneCode.indexOf(',') !== -1) {
//         return phoneCode.split(',')[0];
//     }

//     return phoneCode;
// });

// export const countryPhoneCodes = [...new Set(codes.sort((a, b) => a-b))];

export const getNumberedMonths = (month) => {
    const months = [],
        LAST_MONTH = 12;

    for (let day = 1; day <= LAST_MONTH; day++) {
        months.push(day);
    }

    return months;
};

export const getFutureYears = () => {
    const years = [],
        currentYear = new Date().getFullYear(),
        endYear = currentYear + 21;

    for (let year = currentYear; year <= endYear; year++) {
        years.push(year);
    }

    return years;
};

export const getYears = () => {
    const years = [];

    for (let year = 1988; year <= (new Date().getFullYear()); year++) {
        years.push(year);
    }

    return years;
};

export const getDays = () => {
    const days = [],
        LAST_DAY = 31;

    for (let day = 1; day <= LAST_DAY; day++) {
        days.push(day);
    }

    return days;
};

export const getUrlParams = (param) => {
    const urlParams = new URLSearchParams(window.location.search);

	return urlParams.get(param);
}
