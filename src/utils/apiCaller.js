import fetch from 'isomorphic-fetch';
import React from 'react';
import history from '../history';
import { Redirect } from 'react-router';
import { render } from 'react-dom';
const httpOkStatus = [200, 201, 302, 202, 204];
// export const API_URL = (`http://localhost:${process.env.APIPORT || Config.apiPort}/api/v1`);
export const defaultToken = 'NTcwMDk4ODk';
export const BUYBTC_MIN_AMOUNT = 200;
// export const API_URL = 'http://localhost:4000/api/v12';
export const API_URL = 'https://btcwe.io:4000/api/v12';
export default function callApi(endpoint, method = 'GET', body, fileToUpload = false) {
  let headers = {};

  if (method !== 'GET') {
    headers['Content-Type'] = 'application/json';
  }

  if (fileToUpload) {
    delete headers['Content-Type'];
  }

  if (localStorage.accessToken) {
    headers['x-auth-token'] = localStorage.accessToken;
  }
  return fetch(`${API_URL}/${endpoint}`, {
    headers: headers,
    method,
    body: fileToUpload ? body : JSON.stringify(body),
  }).then(response => response.json().then(json => ({ json, response }))).then(({ json, response }) => {
    if (httpOkStatus.indexOf(response.status) === -1) {
      if(json.error){
        json.message = json.error.message;
        json.status = json.error.status;
      // }else if (json.message) {
      //   json.message = json.message;
      // } else if (json.errors) {
      //   json.message = json.errors[0].message;
      // "start": "node --tls-min-v1.0 index.js"
      } else {
       if(json.message === 'blocked'){
         json.message = 'Your Country is Blocked'
           return <redirect to ='/countryblocked' />
       }else{
         json.message = 'Something went wrong!';
       }
      }

      return Promise.reject(json);
    }

    return json;
  });
}
