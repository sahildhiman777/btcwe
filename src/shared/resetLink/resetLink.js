import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from "react-router-dom";
import { Helmet } from "react-helmet";
import { Button, FormGroup, FormControl, FormLabel, Card, InputGroup, Form } from "react-bootstrap";
import Select from 'react-select'
import countryList from 'react-select-country-list'
import "../resetLink/resetLink.css";
import $ from 'jquery';

$(document).ready(function () {
    $(".mobile_menu_icon").addClass("hide_mobile");
});
class ResetLink extends Component {
    constructor(props) {
        super(props);
        this.options = countryList().getData()
        this.state = {
            redirectState: false,
            email: "",
            password: "",
            repeat_password: "",
            lastName: "four",
            firstName: "test",
            phone: "",
            options: this.options,
            value: "",
            country: "",
            agree: false,
            passworderror: ""
        }

    }
    componentWillReceiveProps(nextProps) {
    }
    render() {
        return (
            <div className=" res-section-cointainer">
                <div className="container padding-top-bottom-50">
                    <Helmet>
                        <style>
                            {".header2 .container{ max-width:unset; margin:unset;padding-left:6%;} #root{overflow:hidden;}.res-section-cointainer .container { transform: skew(0deg, 2deg);}.res-section-cointainer { transform: skew(0deg, -2deg); background:#f9fafe; background-repeat: no-repeat; border-radius: 0px 104px 0px 104px;} ul.ml-auto{display:none !important;}  .footer{ display:none !important;}"}</style>
                    </Helmet>
                    <div className="row margin-top-50">
                        <div className="col-md-12 col-lg-6 res-message-section order-class">
                            <div className="col-md-12 col-lg-12 res-message-section-2">
                                <div className="text-rotate">
                                    <div className="margin-bottom-20">
                                        <img src={require("../../images/bckground/undraw_my_password_d6kg 1.svg")} alt="#" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className=" col-md-12 col-lg-6 res-section order-class">
                            <div className="col-md-12 col-lg-12">
                                <div className="resetlink_text_div">
                                    <div className="check_image">
                                        <img src={require("../../images/icons/tick-reset.svg")} alt="#" />
                                    </div>
                                    <h1 className="res-header-text text-left">A link to reset password was sent</h1>
                                    <p className="mail_text">Please check your email and follow the instructions.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.AuthReducer.isAuthenticated,
        accessToken: state.AuthReducer.accessToken,
        isRedirect: state.AuthReducer.isRedirect
    };
};

const mapDispatchToProps = (dispatch) => ({
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ResetLink)