import React, { Component } from 'react';
import { createChart } from 'lightweight-charts';
import { connect } from 'react-redux';
import { getGraphData } from '../../actions/authAction';
var moment = require('moment');
class Chart extends Component {
    constructor(props) {
		super(props);
		this.state = {
			active:'',
			no_record: '',
			asset_id:'',
			interval:'',
			intervaltype:'',
			xvalueformat:'',
		};
	}
    static defaultProps = {
		containerId: 'lightweight_chart_container',
	};

    componentWillMount() {
		let asset_id = 1;

		let local = [];
		local.push(JSON.parse(localStorage.getItem('commodities-asset-page')));
		
		if(local[0] === null){
			asset_id = 1;
		}else{
			asset_id = local[0].id;
		}
		let data = {
			'asset_id': asset_id,
			'interval': 1440,
			'days': 30
		}
		
		this.setState({ active:"1month" });
		getGraphData(data).then(res => {
			if (res.length !== "") {
				let no_record = res.message; 
				let state = this.state;
				{
					(() => {
						if (no_record.length == 0) {
							this.setState({ no_record: "Data Not Found!!" });
						} else {
							const options = [];

							for (let i = 0; i < no_record.length; i++) {
								let time = moment(no_record[i].object.time).format("YYYY-MM-DD");
											 
								options.push(
									{ time: time, open: no_record[i].object.opening_val, high: no_record[i].object.max_value, low: no_record[i].object.min_value, close: no_record[i].object.closing_val }
									 );
							}
							this.setState({ dataPoints: options, no_record: '' })
							const chart = createChart(this.props.containerId, { 
								width: 800,
								height: 300,
								layout: {
									backgroundColor: '#ffffff',
									textColor: '#000000',
								},
								timeScale: {
									borderColor: 'rgba(197, 203, 206, 0.8)',
								},

								priceScale: {
									mode: 3,
							},

							});

				const candleSeries = chart.addCandlestickSeries({
						upColor: 'rgba(255, 144, 0, 1)',
						downColor: '#000',
						borderDownColor: 'rgba(255, 144, 0, 1)',
						borderUpColor: 'rgba(255, 144, 0, 1)',
						wickDownColor: 'rgba(255, 144, 0, 1)',
						wickUpColor: 'rgba(255, 144, 0, 1)',
				});
				candleSeries.setData(options);
				chart.timeScale().fitContent();
			}
					})()
				}
			}
		});
		this.graphinterval = setInterval(() => {getGraphData(data) }, 60000);
	}

	render() {
		
		return (
			<div
				id={ this.props.containerId }
				className={ 'TVChartContainer' }
			></div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		isLoading: state.AuthReducer.isLoading,
		accessToken: state.AuthReducer.accessToken,
		isAuthenticated: state.AuthReducer.isAuthenticated,
		currentUser: state.AuthReducer.currentUser,
		isbtcRate: state.AuthReducer.isbtcRate,
		isTab: state.AuthReducer.isTab,
		balance: state.AuthReducer.balance
	}
}


// const mapDispatchToProps = (dispatch) => {
//   return {
//     logoutFun: () => dispatch(logoutFun())
//   }
// }

export default connect(mapStateToProps, { getGraphData }
)(Chart)