import React, { Component } from 'react';
import { connect } from 'react-redux';
import "../CandlestickChart/candlestickchart.css";
import { getGraphData } from '../../actions/authAction';
import { createChart } from 'lightweight-charts';
import ReactLoading from "react-loading";
var moment = require('moment');
let areaSeries = '';
let chart = '';

var convertPrice = function (num) {
	return num ? Number(num).toFixed(9) : 0;
}
class CandlestickChart extends Component {
	constructor(props) {
		super(props);
		this.state = {
			active: '',
			data_loading: true,
			no_record: '',
			dataPoints: [],
			asset_id: '',
			datagraph: [],
		};
		this.graph = this.graph.bind(this);
		this.setData = this.setData.bind(this);
		this.onClickfilter = this.onClickfilter.bind(this);
		this.onUpdateData = this.onUpdateData.bind(this)
	}
	componentDidMount() {
		this.props.onRef(this);
	}
	onUpdateData(data) {
		let datagraph = this.state.datagraph;
		let length = datagraph.length;
		let last_data = datagraph[length - 1];
		var time = moment(data.time).utc().format('YYYY-MM-DD HH:mm:ss');
		let setdata = last_data.rate - data.value;
		if (last_data.rate > data.value) {
			areaSeries.applyOptions({
				priceLineColor: '#FC4263',
				lineColor: '#FC4263',
				topColor: 'rgba(252, 66, 99, 0.25)',
				bottomColor: 'rgba(252, 66, 99, 0.25)',
			})
		}
		if (last_data.rate < data.value) {
			areaSeries.applyOptions({
				priceLineColor: '#21DDB8',
				topColor: 'rgba(33, 221, 184, 0.56)',
				bottomColor: 'rgba(33, 221, 184, 0.04)',
				lineColor: '#21DDB8',
			})
		}
		areaSeries.update(data);
		this.state.datagraph.push({ rate_time: time, asset_id: this.state.asset_id, rate: data.value });
	}

	setData(data) {
		if (data.length === 0) {
			return false;
		}

		const options = [];
		for (let i = 0; i < data.length; i++) {
			let time = moment(data[i].rate_time).format("YYYY-MM-DD");
			var unixTime = Date.parse(data[i].rate_time) / 1000;

			options.push(
				{ time: unixTime, value: data[i].rate }
			);
			this.setState({ data_loading: false })
			if (this.state.datagraph.length == 0 || (this.state.datagraph.length && this.state.datagraph[0].asset_id != data[0].asset_id)) {
				this.setState({ datagraph: data, asset_id: data[0].asset_id, active: "1month" })
			}
		}
		areaSeries.setData(options);
		chart.timeScale().fitContent();
	}

	graph(data) {

		const options = [];
		for (let i = 0; i < data.length; i++) {
			var unixTime = Date.parse(data[i].rate_time) / 1000;
			options.push(
				{ time: unixTime, value: data[i].rate }
			);
			this.setState({ data_loading: false, datagraph: data, asset_id: data[i].asset_id })
		}
		chart = createChart('chart_container', {
			width: 800,
			height: 300,
			priceScale: {
				scaleMargins: {
					top: 0.1,
					bottom: 0.1,
				},
			},
			layout: {
				backgroundColor: '#ffffff00',
				textColor: '#7A878C',
			},
			grid: {
				vertLines: {
					color: '#DFE4E9',
					style: 1,
					visible: false,
				},
				horzLines: {
					color: '#DFE4E9',
					style: 1,
					visible: true,
				},
			},
			timeScale: {
				borderColor: '#DFE4E9',
				borderVisible: false,
				visible: true,
				timeVisible: true,
				secondsVisible: true,
			},
			priceScale: {
				borderColor: '#DFE4E9',
				// mode: 0,
			},

		});

		areaSeries = chart.addAreaSeries({
			topColor: 'rgba(33, 221, 184, 0.56)',
			bottomColor: 'rgba(33, 221, 184, 0.04)',
			lineColor: '#21DDB8',
			lineWidth: 1,
		});
		areaSeries.applyOptions({
			priceFormat: {
				type: 'custom',
				minMove: 0.0000001,
				formatter: function (price) {
					return convertPrice(price)
				},
			}
		});
		areaSeries.setData(options);
		chart.timeScale().fitContent();

	}

	componentWillMount() {
		this.setState({ active: "1month", });
	}
	componentWillUnmount() {
		this.props.onRef(undefined)
	}
	onClickfilter(e, t) {
		let data = this.state.datagraph;

		if (t == 1) {
			let newdate = moment().subtract(1, 'd').format('YYYY-MM-DD');

			data = data.filter(function (d) {
				var unixTime = Date.parse(d.rate_time);
				return moment(unixTime).isAfter(newdate, 'day');
			})
			this.setState({ active: "24h" });

		} else if (t == 3) {
			let newdate = moment().subtract(3, 'd').format('YYYY-MM-DD');

			data = data.filter(function (d) {
				var unixTime = Date.parse(d.rate_time);
				return moment(unixTime).isAfter(newdate, 'day');
			})
			this.setState({ active: "3day" });

		} else if (t == 7) {
			let newdate = moment().subtract(7, 'd').format('YYYY-MM-DD');

			data = data.filter(function (d) {
				var unixTime = Date.parse(d.rate_time);
				return moment(unixTime).isAfter(newdate, 'day');
			})
			this.setState({ active: "1week" });
		} else {
			this.setState({ active: "1month", });
		}
		this.setData(data);
	}
	render() {
		return (<>
			<div className="row">
				<div className="col-lg-3 col-md-6 col-sm-6 col-xs-6"><p>
					<img className="graph_currncy_image" alt="#" src={require(`../../images/icons/${this.props.assetIcon}`)} />
					{this.props.assetCoin}
				</p>
				</div>
				<div className="col-lg-9 col-md-6 col-sm-6 col-xs-6">
					<div className="hide_on_mobile">
						<button id="hrs24" type="button" onClick={(event) => this.onClickfilter(event, 1)} className={"btn btn-gray btn-sm " + ((this.state.active === '24h') ? 'active' : '')}> 24h </button>
						<button id="hrs24" type="button" onClick={(event) => this.onClickfilter(event, 3)} className={"btn btn-gray btn-sm " + ((this.state.active === '3day') ? 'active' : '')}> 3days </button>
						<button id="week" type="button" className={"btn btn-gray btn-sm " + ((this.state.active === '1week') ? 'active' : '')} onClick={(event) => this.onClickfilter(event, 7)}> 1Week </button>
						<button id="month" type="button" className={"btn btn-gray btn-sm " + ((this.state.active === '1month') ? 'active' : '')} onClick={(event) => this.onClickfilter(event, 30)}> 1Month </button>
					</div>
					<div className="mobile_dropdown hide_on_desktop">
						<div className="dropdown">
							<button type="button" className="btn dropdown-toggle graph_button" data-toggle="dropdown">
								{this.state.active}
							</button>
							<div className="dropdown-menu graph-btns">
								<ul>
									<li><button id="hrs24" type="button" onClick={(event) => this.onClickfilter(event, 1)} className={"btn btn-gray btn-sm " + ((this.state.active === '24h') ? 'active' : '')}> 24h </button></li>
									<li><button id="hrs24" type="button" onClick={(event) => this.onClickfilter(event, 3)} className={"btn btn-gray btn-sm " + ((this.state.active === '3day') ? 'active' : '')}> 3days </button></li>
									<li><button id="week" type="button" className={"btn btn-gray btn-sm " + ((this.state.active === '1week') ? 'active' : '')} onClick={(event) => this.onClickfilter(event, 7)}> 1Week </button></li>
									<li><button id="month" type="button" className={"btn btn-gray btn-sm " + ((this.state.active === '1month') ? 'active' : '')} onClick={(event) => this.onClickfilter(event, 30)}> 1Month </button></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div>
				{(this.state.no_record) !== "" &&
					<>
						<div class="row">
							<div class="col-md-12">
								<h2 className="jumbotron text-center margin-top-20">{this.state.no_record}</h2>
							</div>
						</div>
					</>
				}
				{(this.state.no_record) === "" &&
					<>
						{(this.state.data_loading) &&
							<>
								<div className="row margin-top-20 dashboard-section padding-top-10 text-center">
									<div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px" }}>
										<ReactLoading color={'#3445CF'} type="spinningBubbles" />
									</div>
								</div>
							</>
						}
						<div id='chart_container'></div>
					</>
				}
			</div></>

		);
	}
}
const mapStateToProps = (state) => {
	return {
		isLoading: state.AuthReducer.isLoading,
		accessToken: state.AuthReducer.accessToken,
		isAuthenticated: state.AuthReducer.isAuthenticated,
		currentUser: state.AuthReducer.currentUser,
		isbtcRate: state.AuthReducer.isbtcRate,
		isTab: state.AuthReducer.isTab,
		balance: state.AuthReducer.balance
	}
}

export default connect(mapStateToProps, { getGraphData }
)(CandlestickChart)