import React, { Component } from 'react';
import { connect } from 'react-redux';
import CanvasJSReact from '../../assets/canvasjs.react';
import "../CandlestickChart/candlestickchart.css";
import { getGraphData } from '../../actions/authAction';
import { createChart } from 'lightweight-charts';
import ReactLoading from "react-loading";
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var CanvasJS = CanvasJSReact.CanvasJS;
var moment = require('moment');
class CandlestickChart extends Component {
	constructor(props) {
		super(props);
		this.state = {
			active:'',
			valueFormat: "DD MMM",
			data_loading: true,
			no_record: '',
			dataPoints:[],
			asset_id:'',
			interval:'',
			intervaltype:'',
			xvalueformat:'',
			default_chart:false,
			month_chart:false,
			week_chart:false,
			threeday_chart:false,
			oneday_chart:false,
			halfday_chart:false,
			sixhour_chart:false,
			onehour_chart:false,
		};
		this.toggleDataSeries = this.toggleDataSeries.bind(this);
		this.addSymbols = this.addSymbols.bind(this);
	}
	static defaultProps = {
		default_chart: 'chart_container',
		month_chart: 'chart_container_month',
		week_chart: 'chart_container_week',
		threeday_chart:'chart_container_threeday',
		oneday_chart:'chart_container_oneday',
		halfday_chart:'chart_container_halfday',
		sixhour_chart:'chart_container_sixhour',
		onehour_chart:'chart_container_onehour',
	};
	componentWillMount() {
		let asset_id = 1;

		let local = '';
		local = localStorage.getItem('asset_id');
		
		if(local === null){
			asset_id = 1;
		}else{
			asset_id = local;
		}
		let data = {
			'asset_id': asset_id,
			'interval': 1440,
			'days': 30
		}
		this.setState({ active:"1month",default_chart: true, month_chart:false, week_chart:false, threeday_chart:false, oneday_chart:false, halfday_chart:false, sixhour_chart:false, onehour_chart:false, });
		getGraphData(data).then(res => {
			
			if (res.length !== "") {
				let no_record = res.message; 
				let state = this.state;
				{
					(() => {
						if (no_record.length == 0) {
							this.setState({ no_record: "Data Not Found!!" });
						} else {
							const options = [];
							for (let i = 0; i < no_record.length; i++) {
								let time = moment(no_record[i].object.time).format("YYYY-MM-DD");	 
								options.push(
									{ time: time, open: no_record[i].object.opening_val, high: no_record[i].object.max_value, low: no_record[i].object.min_value, close: no_record[i].object.closing_val }
									 );
									 this.setState({ data_loading: false})
							}
							this.setState({ dataPoints: options, no_record: '' })
							const chart = createChart(this.props.default_chart, { 
								width: 800,
								height: 300,
								layout: {
									backgroundColor: '#ffffff00',
									textColor: '#2D3344',
								},
								grid: {
									vertLines: {
											color: '#DFE4E9',
											style: 1,
											visible: false,
									},
									horzLines: {
											color: '#DFE4E9',
											style: 1,
											visible: true,
									},
							},
								timeScale: {
									borderColor: '#000000',
									borderVisible: false,
								},
								priceScale: {
									mode: 3,
								
							},

							});

				const candleSeries = chart.addCandlestickSeries({
						upColor: '#3445CF',
						downColor: '#21DDB8',
						borderDownColor: '#21DDB8',
						borderUpColor: '#3445CF',
						wickDownColor: '#19C8B3	',
						wickUpColor: '#FC7342',
				});
				candleSeries.setData(options);
				chart.timeScale().fitContent();
			}
					})()
				}
			}
		});
		this.graphinterval = setInterval(() => {getGraphData(data) }, 60000);

	
	}
	componentWillUnmount() {
    clearInterval(this.graphinterval);
  }
	addSymbols(e) {
		var suffixes = ["", "K", "M", "B"];
		var order = Math.max(Math.floor(Math.log(e.value) / Math.log(1000)), 0);
		if (order > suffixes.length - 1)
			order = suffixes.length - 1;
		var suffix = suffixes[order];
		return CanvasJS.formatNumber(e.value / Math.pow(1000, order)) + suffix;
	}
	toggleDataSeries(e) {
		if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
			e.dataSeries.visible = false;
		}
		else {
			e.dataSeries.visible = true;
		}
		this.chart.render();
	}
	onClick(e, days) {
		let asset_id = this.props.asset_id;
		let data = {
			'asset_id': asset_id,
			'interval': 5,
			'days': days
		}
		this.setState({active:"1h", data_loading: true, default_chart: false, month_chart:false, week_chart:false, threeday_chart:false, oneday_chart:false, halfday_chart:false, sixhour_chart:false, onehour_chart:true, });

		getGraphData(data).then(res => {
			
			if (res.length !== "") {
				let no_record = res.message;
				let state = this.state;
				{
					(() => {
						if (no_record.length == 0) {
							this.setState({ no_record: "Data Not Found!!" });
						} else {
							const options = [];

							for (let i = 0; i < no_record.length; i++) {
								// let time = moment(no_record[i].object.time).format("YYYY-MM-DD");
								var time = Date.parse(no_record[i].object.time)/1000;
								options.push(
									{ time: time, open: no_record[i].object.opening_val, high: no_record[i].object.max_value, low: no_record[i].object.min_value, close: no_record[i].object.closing_val }
									 );
									 this.setState({ data_loading: false})
							}
							this.setState({ dataPoints: options, no_record: '' })
							const chart = createChart(this.props.onehour_chart, { 
								width: 800,
								height: 300,
								layout: {
									backgroundColor: '#ffffff00',
									textColor: '#2D3344',
								},
								grid: {
									vertLines: {
											color: '#DFE4E9',
											style: 1,
											visible: false,
									},
									horzLines: {
											color: '#DFE4E9',
											style: 1,
											visible: true,
									},
							},
								timeScale: {
									borderColor: '#000000',
									borderVisible: false,
									visible: true,
                	timeVisible: true,
                	secondsVisible: true,
								},
								priceScale: {
									mode: 3,
								
							},
							});
				const candleSeries = chart.addCandlestickSeries({
					upColor: '#3445CF',
					downColor: '#21DDB8',
					borderDownColor: '#21DDB8',
					borderUpColor: '#3445CF',
					wickDownColor: '#21DDB8',
					wickUpColor: '#3445CF',
				});
				candleSeries.setData(options);
				chart.timeScale().fitContent();
			}
					})()
				}
			}
		});
	}

	onClickSHour(e, days) {
		let asset_id = this.props.asset_id;
		let data = {
			'asset_id': asset_id,
			'interval': 5,
			'days': days
		}
		this.setState({active:"6h", data_loading: true, default_chart: false, month_chart:false, week_chart:false, threeday_chart:false, oneday_chart:false, halfday_chart:false, sixhour_chart:true, onehour_chart:false,});

		getGraphData(data).then(res => {
			if (res.length !== "") {
				let no_record = res.message;
				let state = this.state;
				{
					(() => {
						if (no_record.length == 0) {
							this.setState({ no_record: "Data Not Found!!" });
						} else {
							const options = [];

							for (let i = 0; i < no_record.length; i++) {
								// let time = moment(no_record[i].object.time).format("YYYY-MM-DD");
								var time = Date.parse(no_record[i].object.time)/1000;
								options.push(
									{ time: time, open: no_record[i].object.opening_val, high: no_record[i].object.max_value, low: no_record[i].object.min_value, close: no_record[i].object.closing_val }
									 );
									 this.setState({ data_loading: false})
							}
							this.setState({ dataPoints: options, no_record: '' })
							const chart = createChart(this.props.sixhour_chart, { 
								width: 800,
								height: 300,
								layout: {
									backgroundColor: '#ffffff00',
									textColor: '#2D3344',
								},
								grid: {
									vertLines: {
											color: '#DFE4E9',
											style: 1,
											visible: false,
									},
									horzLines: {
											color: '#DFE4E9',
											style: 1,
											visible: true,
									},
							},
								timeScale: {
									borderColor: '#000000',
									borderVisible: false,
									visible: true,
                	timeVisible: true,
                	secondsVisible: true,
								},
								priceScale: {
									mode: 3,
								
							},
							});
				const candleSeries = chart.addCandlestickSeries({
					upColor: '#3445CF',
					downColor: '#21DDB8',
					borderDownColor: '#21DDB8',
					borderUpColor: '#3445CF',
					wickDownColor: '#21DDB8',
					wickUpColor: '#3445CF',
				});
				candleSeries.setData(options);
				chart.timeScale().fitContent();
			}
					})()
				}
			}
		});
	}
	onClickTHour(e, days) {
		let asset_id = this.props.asset_id;
		let data = {
			'asset_id': asset_id,
			'interval': 5,
			'days': days
		}
		this.setState({active:"12h", data_loading: true, default_chart: false, month_chart:false, week_chart:false, threeday_chart:false, oneday_chart:false, halfday_chart:true, sixhour_chart:false, onehour_chart:false, });
		getGraphData(data).then(res => {
			if (res.length !== "") {
				let no_record = res.message;
				let state = this.state;
				{
					(() => {
						if (no_record.length == 0) {
							this.setState({ no_record: "Data Not Found!!" });
						} else {
							const options = [];

							for (let i = 0; i < no_record.length; i++) {
								// let time = moment(no_record[i].object.time).format("YYYY-MM-DD");
								var time = Date.parse(no_record[i].object.time)/1000;
								options.push(
									{ time: time, open: no_record[i].object.opening_val, high: no_record[i].object.max_value, low: no_record[i].object.min_value, close: no_record[i].object.closing_val }
									 );
									 this.setState({ data_loading: false})
							}
							this.setState({ dataPoints: options, no_record: '' })
							const chart = createChart(this.props.halfday_chart, { 
								width: 800,
								height: 300,
								layout: {
									backgroundColor: '#ffffff00',
									textColor: '#2D3344',
								},
								grid: {
									vertLines: {
											color: '#DFE4E9',
											style: 1,
											visible: false,
									},
									horzLines: {
											color: '#DFE4E9',
											style: 1,
											visible: true,
									},
							},
								timeScale: {
									borderColor: '#000000',
									borderVisible: false,
									visible: true,
                	timeVisible: true,
                	secondsVisible: true,
								},
								priceScale: {
									mode: 3,
								
							},
							});
				const candleSeries = chart.addCandlestickSeries({
					upColor: '#3445CF',
					downColor: '#21DDB8',
					borderDownColor: '#21DDB8',
					borderUpColor: '#3445CF',
					wickDownColor: '#21DDB8',
					wickUpColor: '#3445CF',
				});
				candleSeries.setData(options);
				chart.timeScale().fitContent();
			}
					})()
				}
			}
		});
	}

	onClickHour(e, days) {
		if(this.state.active == '24h'){
			return;
		}
		let asset_id = this.props.asset_id;
		let data = {
			'asset_id': asset_id,
			'interval': 60,
			'days': days
		}
		this.setState({active:"24h", data_loading: true, default_chart: false, month_chart:false, week_chart:false, threeday_chart:false, oneday_chart:true, halfday_chart:false, sixhour_chart:false, onehour_chart:false, });

		getGraphData(data).then(res => {
			if (res.length !== "") {
				let no_record = res.message;
				let state = this.state;
				{
					(() => {
						let string = "invalid day interval!";

						if (no_record.length == 0) {
							this.setState({ no_record: "Data Not Found!!" });
						} else {
							const options = [];

							for (let i = 0; i < no_record.length; i++) {
								// let time = moment(no_record[i].object.time).format("YYYY-MM-DD");
								var time = Date.parse(no_record[i].object.time)/1000;
								
								options.push(
									{ time: time, open: no_record[i].object.opening_val, high: no_record[i].object.max_value, low: no_record[i].object.min_value, close: no_record[i].object.closing_val }
									 );
									 this.setState({ data_loading: false})
							}
							
							this.setState({ dataPoints: options, no_record: '' })
							const chart = createChart(this.props.oneday_chart, { 
								width: 800,
								height: 300,
								layout: {
									backgroundColor: '#ffffff00',
									textColor: '#2D3344',
								},
								grid: {
									vertLines: {
											color: '#DFE4E9',
											style: 1,
											visible: false,
									},
									horzLines: {
											color: '#DFE4E9',
											style: 1,
											visible: true,
									},
							},
								timeScale: {
									borderColor: '#000000',
									borderVisible: false,
									visible: true,
                	timeVisible: true,
                	secondsVisible: true,
								},
								priceScale: {
									mode: 3,
								
							},
							});
				const candleSeries = chart.addCandlestickSeries({
					upColor: '#3445CF',
					downColor: '#21DDB8',
					borderDownColor: '#21DDB8',
					borderUpColor: '#3445CF',
					wickDownColor: '#21DDB8',
					wickUpColor: '#3445CF',
				});
				candleSeries.setData(options);
				chart.timeScale().fitContent();
			}
					})()
				}
			}
		});
	}

	onClickThree(e, days) {
		if(this.state.active == '3day'){
			return;
		}
		let asset_id = this.props.asset_id;
		let data = {
			'asset_id': asset_id,
			'interval': 720,
			'days': days
		}
		this.setState({ active:"3day", data_loading: true, default_chart: false, month_chart:false, week_chart:false, threeday_chart:true, oneday_chart:false, halfday_chart:false, sixhour_chart:false, onehour_chart:false,});

		getGraphData(data).then(res => {
			if (res.length !== "") {
				let no_record = res.message;
				let state = this.state;
				{
					(() => {
						if (no_record.length == 0) {
							this.setState({ no_record: "Data Not Found!!" });
						} else {
							const options = [];

							for (let i = 0; i < no_record.length; i++) {
								let time = moment(no_record[i].object.time).format("YYYY-MM-DD");
								options.push(
									{ time: time, open: no_record[i].object.opening_val, high: no_record[i].object.max_value, low: no_record[i].object.min_value, close: no_record[i].object.closing_val }
									 );
									 this.setState({ data_loading: false})
							}
							this.setState({ dataPoints: options, no_record: '' })
							const chart = createChart(this.props.threeday_chart, { 
								width: 800,
								height: 300,
								layout: {
									backgroundColor: '#ffffff00',
									textColor: '#2D3344',
								},
								grid: {
									vertLines: {
											color: '#DFE4E9',
											style: 1,
											visible: false,
									},
									horzLines: {
											color: '#DFE4E9',
											style: 1,
											visible: true,
									},
							},
								timeScale: {
									borderColor: '#000000',
									borderVisible: false,
								},
								priceScale: {
									mode: 3,
								
							},
							});
				const candleSeries = chart.addCandlestickSeries({
					upColor: '#3445CF',
					downColor: '#21DDB8',
					borderDownColor: '#21DDB8',
					borderUpColor: '#3445CF',
					wickDownColor: '#21DDB8',
					wickUpColor: '#3445CF',
				});
				candleSeries.setData(options);
				chart.timeScale().fitContent();
			}
					})()
				}
			}
		});
	}
	onClickWeek(e, days) {
		if(this.state.active == '1week'){
			return;
		}
		let asset_id = this.props.asset_id;
		let data = {
			'asset_id': asset_id,
			'interval': 1440,
			'days': days
		}
		this.setState({active:"1week" , data_loading: true, default_chart: false, month_chart:false, week_chart:true, threeday_chart:false, oneday_chart:false, halfday_chart:false, sixhour_chart:false, onehour_chart:false, });

		getGraphData(data).then(res => {
			if (res.length !== "") {
				let no_record = res.message;
				let state = this.state;
				{
					(() => {
						let string = "invalid day interval!";

						if (no_record.length == 0) {
							this.setState({ no_record: "Data Not Found!!" });
						} else {
							const options = [];

							for (let i = 0; i < no_record.length; i++) {
								let time = moment(no_record[i].object.time).format("YYYY-MM-DD");
								options.push(
									{ time: time, open: no_record[i].object.opening_val, high: no_record[i].object.max_value, low: no_record[i].object.min_value, close: no_record[i].object.closing_val }
									 );
									 this.setState({ data_loading: false})
							}
							this.setState({ dataPoints: options, no_record: '' })
							const chart = createChart(this.props.week_chart, { 
								width: 800,
								height: 300,
								layout: {
									backgroundColor: '#ffffff00',
									textColor: '#2D3344',
								},
								grid: {
									vertLines: {
											color: '#DFE4E9',
											style: 1,
											visible: false,
									},
									horzLines: {
											color: '#DFE4E9',
											style: 1,
											visible: true,
									},
							},
								timeScale: {
									borderColor: '#000000',
									borderVisible: false,
								},
								priceScale: {
									mode: 3,
								
							},
							});
				const candleSeries = chart.addCandlestickSeries({
					upColor: '#3445CF',
					downColor: '#21DDB8',
					borderDownColor: '#21DDB8',
					borderUpColor: '#3445CF',
					wickDownColor: '#21DDB8',
					wickUpColor: '#3445CF',
				});
				candleSeries.setData(options);
				chart.timeScale().fitContent();
			}
					})()
				}
			}
		});
	}
	onClickMonth(e, days) {
		if(this.state.active == '1month'){
			return;
		}
		let asset_id = this.props.asset_id;
		let data = {
			'asset_id': asset_id,
			'interval': 1440,
			'days': days
		}
		this.setState({active:"1month", data_loading: true, default_chart: false, month_chart:true, week_chart:false, threeday_chart:false, oneday_chart:false, halfday_chart:false, sixhour_chart:false, onehour_chart:false, });

		getGraphData(data).then(res => {			
			if (res.length !== "") {
				let no_record = res.message;
				let state = this.state;
				{
					(() => {
						if (no_record.length == 0) {
							this.setState({ no_record: "Data Not Found!!" });
						} else {
							const options = [];

							for (let i = 0; i < no_record.length; i++) {
								let time = moment(no_record[i].object.time).format("YYYY-MM-DD");
								options.push(
									{ time: time, open: no_record[i].object.opening_val, high: no_record[i].object.max_value, low: no_record[i].object.min_value, close: no_record[i].object.closing_val }
									 );
									 this.setState({ data_loading: false})
							}
							this.setState({ dataPoints: options, no_record: '' })
							const chart = createChart(this.props.month_chart, { 
								width: 800,
								height: 300,
								layout: {
									backgroundColor: '#ffffff00',
									textColor: '#2D3344',
								},
								grid: {
									vertLines: {
											color: '#DFE4E9',
											style: 1,
											visible: false,
									},
									horzLines: {
											color: '#DFE4E9',
											style: 1,
											visible: true,
									},
							},
								timeScale: {
									borderColor: '#000000',
									borderVisible: false,
								},
								priceScale: {
									mode: 3,
								
							},
							});
				const candleSeries = chart.addCandlestickSeries({
					upColor: '#3445CF',
					downColor: '#21DDB8',
					borderDownColor: '#21DDB8',
					borderUpColor: '#3445CF',
					wickDownColor: '#21DDB8',
					wickUpColor: '#3445CF',
				});
				candleSeries.setData(options);
				chart.timeScale().fitContent();
			}
					})()
				}
			}
		});
	}
	render() {
		
		// const options = {
		// 	animationEnabled: true,
		// 	title: {
		// 		text: ""
		// 	},
		// 	axisX: {
		// 		valueFormatString: this.state.valueFormat,
		// 		interval: this.state.interval,
		// 		intervalType: this.state.intervaltype,
		// 		lineColor: "#DFE4E9",
		// 		tickLength: 20,
		// 		tickColor: "transparent",
		// 	},
		// 	axisY: {
		// 		prefix: "",
		// 		title: "",
		// 		tickLength: 20,
		// 		tickColor: "transparent",
		// 		lineColor: "transparent",
		// 		gridColor: "#DFE4E9",
		// 		includeZero: false,
				
		// 	},
		// 	toolTip: {
		// 	},
		// 	height: 300,
		// 	responsive: true,
		// 	backgroundColor: "transparent",
		// 	data: [{
		// 		type: "area",
		// 		showInLegend: true,
		// 		dataPointWidth: 50,
		// 		name: "",
		// 		risingColor: "#3445CF",
		// 		yValueFormatString: "#,##0.0000000000",
		// 		xValueFormatString: this.state.xvalueformat,
				
		// 		dataPoints: this.state.dataPoints
		// 	}
		// 	]
		// }
		

		
		
		return (<>
			<div className="row">
				<div className="col-lg-3 col-md-6 col-sm-6 col-xs-6"><p>
					<img className="graph_currncy_image" alt="#" src={require(`../../images/icons/${this.props.assetIcon}`)} />
					{this.props.assetCoin}
				</p>
				</div>
				<div className="col-lg-9 col-md-6 col-sm-6 col-xs-6">
					<div className="hide_on_mobile">
						{/* <button id="hrs24" type="button" onClick={(event) => this.onClick(event, 1)} className={"btn btn-gray btn-sm "+((this.state.active==='1h')?'active':'')}> 1h </button>
						<button id="hrs24" type="button" onClick={(event) => this.onClickSHour(event, 1)} className={"btn btn-gray btn-sm "+((this.state.active==='6h')?'active':'')}> 6h </button>
						<button id="hrs24" type="button" onClick={(event) => this.onClickTHour(event, 1)} className={"btn btn-gray btn-sm "+((this.state.active==='12h')?'active':'')}> 12h </button> */}
						<button id="hrs24" type="button" onClick={(event) => this.onClickHour(event, 1)} className={"btn btn-gray btn-sm "+((this.state.active==='24h')?'active':'')}> 24h </button>
						<button id="hrs24" type="button" onClick={(event) => this.onClickThree(event, 3)} className={"btn btn-gray btn-sm "+((this.state.active==='3day')?'active':'')}> 3days </button>
						<button id="week" type="button" className={"btn btn-gray btn-sm "+((this.state.active==='1week')?'active':'')} onClick={(event) => this.onClickWeek(event, 7)}> 1Week </button>
						<button id="month" type="button" className={"btn btn-gray btn-sm "+((this.state.active==='1month')?'active':'')} onClick={(event) => this.onClickMonth(event, 30)}> 1Month </button>
					</div>
					<div className="mobile_dropdown hide_on_desktop">
					<div className="dropdown">
						<button type="button" className="btn dropdown-toggle graph_button" data-toggle="dropdown">
							Week
  					</button>
						<div className="dropdown-menu">
							<ul>
								{/* <li><button id="hrs24" type="button" onClick={(event) => this.onClick(event, 1)} className={"btn btn-gray btn-sm "+((this.state.active==='1h')?'active':'')}> 1h </button></li>
								<li><button id="hrs24" type="button" onClick={(event) => this.onClickSHour(event, 1)} className={"btn btn-gray btn-sm "+((this.state.active==='6h')?'active':'')}> 6h </button></li>
								<li><button id="hrs24" type="button" onClick={(event) => this.onClickTHour(event, 1)} className={"btn btn-gray btn-sm "+((this.state.active==='12h')?'active':'')}> 12h </button></li> */}
								<li><button id="hrs24" type="button" onClick={(event) => this.onClickHour(event, 1)} className={"btn btn-gray btn-sm "+((this.state.active==='24h')?'active':'')}> 24h </button></li>
								<li><button id="hrs24" type="button" onClick={(event) => this.onClickThree(event, 3)} className={"btn btn-gray btn-sm "+((this.state.active==='3day')?'active':'')}> 3days </button></li>
								<li><button id="week" type="button" className={"btn btn-gray btn-sm "+((this.state.active==='1week')?'active':'')} onClick={(event) => this.onClickWeek(event, 7)}> 1Week </button></li>
								<li><button id="month" type="button" className={"btn btn-gray btn-sm "+((this.state.active==='1month')?'active':'')} onClick={(event) => this.onClickMonth(event, 30)}> 1Month </button></li>
							</ul>
						</div>
					</div>
				</div>
				</div>
			</div>
			<div>
				{(this.state.no_record) !== "" &&
					<>
						<div class="row">
							<div class="col-md-12">
								<h2 className="jumbotron text-center margin-top-20">{this.state.no_record}</h2>
							</div>
						</div>
					</>
				}
				{(this.state.no_record) === "" &&
					<>
					{(this.state.data_loading) &&
					<>
					 <div className="row margin-top-20 dashboard-section padding-top-10 text-center">
                <div className="col-md-2 offset-md-5" style={{ "marginLeft": "45%", "marginBottom": "10px" }}>
                  <ReactLoading color={'#3445CF'} type="spinningBubbles" />
                </div>
              </div>
					</>
					}
						{/* <CanvasJSChart className="chart_over" options={options}
						style={{"height": "300px"}}/> */}
						{(this.state.default_chart) &&
						<>
						<div id={ this.props.default_chart }></div>
						</>
						}
			
						{(this.state.month_chart) &&
						<>
						<div id={ this.props.month_chart }></div>
						</>
						}

						{(this.state.week_chart) &&
						<>
						<div id={ this.props.week_chart }></div>
						</>
						}

						{(this.state.threeday_chart) &&
						<>
						<div id={ this.props.threeday_chart }></div>
						</>
						}

						{(this.state.oneday_chart) &&
						<>
						<div id={ this.props.oneday_chart }></div>
						</>
						}

						{(this.state.halfday_chart) &&
						<>
						<div id={ this.props.halfday_chart }></div>
						</>
						}

						{(this.state.sixhour_chart) &&
						<>
						<div id={ this.props.sixhour_chart }></div>
						</>
						}

						{(this.state.onehour_chart) &&
						<>
						<div id={ this.props.onehour_chart }></div>
						</>
						}

					</>
				}	
				</div></>

		);
	}
}
const mapStateToProps = (state) => {
	return {
		isLoading: state.AuthReducer.isLoading,
		accessToken: state.AuthReducer.accessToken,
		isAuthenticated: state.AuthReducer.isAuthenticated,
		currentUser: state.AuthReducer.currentUser,
		isbtcRate: state.AuthReducer.isbtcRate,
		isTab: state.AuthReducer.isTab,
		balance: state.AuthReducer.balance
	}
}


// const mapDispatchToProps = (dispatch) => {
//   return {
//     logoutFun: () => dispatch(logoutFun())
//   }
// }

export default connect(mapStateToProps, { getGraphData }
)(CandlestickChart)