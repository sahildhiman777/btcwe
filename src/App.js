
import React from 'react'
import PropTypes from 'prop-types'
import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'
import './App.css';
import ContainerLayout from './containers/index.js';
import { connect } from 'react-redux'

const App = ({ store, history }) => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <ContainerLayout />
      </Router>
    </Provider>
  )
}

App.propTypes = {
  store: PropTypes.object.isRequired
}



const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.AuthReducer.isAuthenticated,
    accessToken: state.AuthReducer.accessToken,
  };
};


export default connect(mapStateToProps)(App)
